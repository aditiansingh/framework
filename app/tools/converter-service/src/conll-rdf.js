const { spawn } = require('child_process')
const express = require('express')
const app = express()

app.use(express.json({ limit: '500kb' }))
app.post('/format/rdf', ({ body, query: { base } }, res) => {
    if (body.annotation == null) {
        res.end()
        return
    }

    const conllRdf = spawn('../CoNLL-RDF/run.sh', [
        'CoNLLStreamExtractor',
        base || `https://cdli.ucla.edu/P${body.artifact_id.padLeft(6, '0')}#`,
        'CDLI_ID FORM SEGM XPOSTAG HEAD DEPREL MISC'
    ])

    let stdout = '';
    let stderr = '';
    conllRdf.stdout.on('data', data => { stdout += data.toString() });
    conllRdf.stderr.on('data', data => { stderr += data.toString() });

    conllRdf.on('exit', (code) => {
        if (stdout.length > 0) {
            res.send(stdout)
        } else {
            res.status(500).send('Processing CDLI-CoNLL failed')
        }
    })

    conllRdf.stdin.write(body.annotation + '\n')
    conllRdf.stdin.end()
})

module.exports = {
    app,
    message: `Running CoNLL-RDF...`
}
