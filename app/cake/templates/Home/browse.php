<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */


 $additional=["Abbreviations" => "abbreviations", "Agade mails" => "agade_mails", "Archives" => "archives","Artifacts" => "artifacts",
 "Artifact types" => "artifactTypes", "Authors" => "authors", "CDLI tablet" => "CdliTablet", "Dates" => "dates", "Dynasties" => "dynasties", "External resources" => "externalResources",
 "Journals" => "journals", "Material aspects" => "materialAspects", "Material colors" => "materialColors", "Materials" => "materials",
 "Publications" => "publications", "Regions" => "regions", "Rulers" => "rulers", "Sign readings" => "signReadings"];
?>
<main class="container">
	<h1 class="display-3 header-text text-left">Browse</h1>
	<p class="text-left page-summary-text mt-4"> To browse a subset of artifacts that match your interest, please choose a featured category to explore.</p>
	<div>
		<h2 class="text-left display-4 section-title">Featured</h2>
		<div class="row mt-5">
      <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<!-- <img class="card-img-top" src="https://www.desktopbackground.org/p/2013/09/01/632092_free-download-wallpapers-hd-free-grey-background-images_2480x3508_h.jpg" alt="Card image cap"> -->
				<div class="card-body text-left border">
					<p class="card-title">
						<a href="/heatmap">Heatmap</a>
					</p>
					<p class="card-text">Use this Heatmap to visualise the location and numbers of tablets found in different sites. The map can be tweaked using a search and a filter feature.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<!-- <img class="card-img-top" src="https://www.desktopbackground.org/p/2013/09/01/632092_free-download-wallpapers-hd-free-grey-background-images_2480x3508_h.jpg" alt="Card image cap"> -->
				<div class="card-body text-left border">
					<p class="card-title">
						<a href="/proveniences">Proveniences</a>
					</p>
					<p class="card-text">Cuneiform tablets have been found across the wider Middle East, the vast majority are from Iraq, but substantial amounts of material come from neighboring countries.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<!-- <img class="card-img-top" src="https://www.desktopbackground.org/p/2013/09/01/632092_free-download-wallpapers-hd-free-grey-background-images_2480x3508_h.jpg" alt="Card image cap"> -->
				<div class="card-body text-left border">
					<p class="card-title">
						<a href="/periods">Periods</a>
					</p>
					<p class="card-text">Written for more than 3,000 years cuneiform writing covers a wider range of time periods.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<!-- <img class="card-img-top" src="https://www.desktopbackground.org/p/2013/09/01/632092_free-download-wallpapers-hd-free-grey-background-images_2480x3508_h.jpg" alt="Card image cap"> -->
				<div class="card-body text-left border">
					<p class="card-title">
						<a href="/genres">Genres</a>
					</p>
					<p class="card-text">Used to write everything from accounting documents, to epic poetry and scientific texts, the preserved cuneiform texts over many genres.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<!-- <img class="card-img-top" src="https://www.desktopbackground.org/p/2013/09/01/632092_free-download-wallpapers-hd-free-grey-background-images_2480x3508_h.jpg" alt="Card image cap"> -->
				<div class="card-body text-left border">
					<p class="card-title">
						<a href="/languages">Languages</a>
					</p>
					<p class="card-text">Cuneiform was used to write over 10 different languages and many other dialects.</p>
				</div>
			</div>

			<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-6">
				<!-- <img class="card-img-top" src="https://www.desktopbackground.org/p/2013/09/01/632092_free-download-wallpapers-hd-free-grey-background-images_2480x3508_h.jpg" alt="Card image cap"> -->
				<div class="card-body text-left border">
					<p class="card-title">
						<a href="/collections">Collections</a>
					</p>
					<p class="card-text">Cuneiform tablets can be found in modern collections around the world.</p>
				</div>
			</div>

		</div>

		<h2 class="text-left display-4 section-title">Dispersed categories</h2>
		<?php $this->Grid->Alphabetical($additional)?>
	</div>
	<?php echo $this->element('smoothscroll'); ?>
</main>
