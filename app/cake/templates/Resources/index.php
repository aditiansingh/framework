<h1 class="ml-0 text-left display-4 mb-4 res-header">Resources</h1>
<div class="accordion my-acc">
    <?= $this->Accordion->partOpen('CDLI Resources', 'CDLI Resources', 'h2') ?>
        <div>
          <div class="row mt-3">
             <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body text-left border">
                   <p class="card-title">
                      <a href="/publications">CDLI Bibliography</a>
                   </p>
                   <p  class="resource-des ">Information about cuneiform artifacts have been published in numerous peer-reviewed books and articles. This CDLI feature provides search and browse capabilities into this precious resource that was compiled by a multitude of scholars over a three decades period.</p>
                </div>
             </div>
             <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body text-left border">
                   <p class="card-title">
                      <a href="/CdliTablet">CDLI Tablet</a>
                   </p>
                   <p  class="resource-des ">cdli tablet is an iPad and Android app that combines text and images of cuneiform inscriptions, of related archaeological artifacts, and of the equipment and techniques employed by specialists to capture, digitally preserve and disseminate the cultural heritage of the ancient Near East. </p>
                </div>
             </div>
             <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                <div class="d-flex flex-column card-body text-left border">
                   <p class="card-title">
                      <a href="https://cdli-gh.github.io/guides/guides_intro.html">CDLI Documentation</a>
                   </p>
                   <p  class="resource-des ">List of main categories in documentation (Available in august).</p>
                </div>
             </div>
             <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                <div class="d-flex flex-column card-body text-left border">
                   <p class="card-title">
                      <a href="/agade_mails">Cooked Agade</a>
                   </p>
                   <p  class="resource-des ">Enjoy a filtered version of the Agade mailing list with manually curated entries centering around Mesopotamian studies only.</p>
                </div>
             </div>
          </div>
        </div>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('Composite Texts', 'Composite Texts', 'h2') ?>
                <div>
                  <div class="row mt-3">
                     <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="#">Composite litrary texts & witnesses (scores)</a>
                           </p>
                           <p  class="resource-des ">Numerous cuneiform artifacts were inscribed with witnesses of compositions. In order to study these compositions, scholars align witnesses so they can see the variations and similarities between exemplars. There are composites of various genres of texts but mostly Royal inscriptions, literary compositions and lexical texts.</p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="#">Royal litrary texts & witnesses (scores)</a>
                           </p>
                           <p  class="resource-des ">Powerful figures of Mesopotamia of all periods have disseminated their accomplishements using varied mediums. </p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="https://cdli.ucla.edu/projects/seals/seals.html">Seals & seals impressions</a>
                           </p>
                           <p  class="resource-des ">Seals, mostly cylindrical, engraved with scenes and text, were rolled onto fresh clay to leave their imprint, an ingenious administrative tool. </p>
                        </div>
                     </div>
                  </div>
                </div>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('References', 'References', 'h2') ?>
                <div>
                  <div class="row mt-3">
                     <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="/abbreviations">Abbreviations</a>
                           </p>
                           <p  class="resource-des ">List of abbreviations used in Mesopotamian Studies.</p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="#">Preferred signreadings</a>
                           </p>
                           <p  class="resource-des ">Convention of sign values to use when submitting transliterations to the CDLI.</p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="https://cdli.ox.ac.uk/wiki/sign_lists">Sign lists</a>
                           </p>
                           <p  class="resource-des ">Published cuneifom sign lists for all periods.</p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="https://cdli.ucla.edu/tools/ur3months/month.html">UR III months names</a>
                           </p>
                           <p  class="resource-des ">A correspondance table showing months vs month number, period, ruler and rulership year.</p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="#">Years</a>
                           </p>
                           <p  class="resource-des ">A list of composite year names.</p>
                        </div>
                     </div>
                  </div>
                </div>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('Tools', 'Tools', 'h2') ?>
                <div>
                  <div class="row mt-3">
                     <div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="#">Tool1</a>
                           </p>
                           <p  class="resource-des ">Check your catalogue and bibliographic data for consistency before sending to CDLI.</p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="#">Tool2</a>
                           </p>
                           <p  class="resource-des ">Uqnu the transliteration (ATF) editor.</p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="#">Tool3</a>
                           </p>
                           <p  class="resource-des ">Check the consistency of your conll based annotations</p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="#">Tool4</a>
                           </p>
                           <p  class="resource-des ">Convert your transliterations and textual annotations among formats (C-ATF, CDLI-ConLL, ConLL-U, Brat, JSON-RDF, XML-RDF, and Turtle.  </p>
                        </div>
                     </div>
                     <div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
                        <div class="d-flex flex-column card-body text-left border">
                           <p class="card-title">
                              <a href="#">Tool5</a>
                           </p>
                           <p class="resource-des">Automatically annotate or translate your transliterations. </p>
                        </div>
                     </div>
                  </div>
                </div>
        <?= $this->Accordion->partClose() ?>
</div>
<?php echo $this->element('smoothscroll'); ?>
