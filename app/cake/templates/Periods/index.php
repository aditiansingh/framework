<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Period[]|\Cake\Collection\CollectionInterface $periods
 */
?>

  <h2 class="text-left display-4 pt-3"><?= __('Periods') ?></h2>
  <p class="text-left page-summary-text mt-4 pl"> Cuneiform writing has a long history and was in use as Proto-Cuneiform which appeared at the turn of the fourth millennium, up to the Christian Era. Scroll down to see the most recent periods of the timeline and click on the geo-temporal period of your choice to know more.</p>
  <div class="timelinemid timelinecontainer2">
    </div>
    <div class="timelinebody timeline">
      <?php $a=1 ?>
      <?php foreach ($periods as $period): ?>
        <?php if($a%2==0) : ?>
        <div class="timelinecontainer timelineleft">
          <div class="timelinecontentBlock">
            <p><a href="/periods/<?=h($period->id)?>"><?= h($period->period) ?></a></p>
          </div>
        </div>
    <?php endif; ?>
    <?php if($a%2!=0) : ?>
      <div class="timelinecontainer timelineright">
        <div class="timelinecontentBlock">
          <p><a href="/periods/<?=h($period->id)?>"><?= h($period->period) ?></a></p>
        </div>
      </div>
    <?php endif; ?>
    <?php $a=$a+1 ?>
      <?php endforeach; ?>
    </div>
    <div class="timelinemid2 timelinecontainer3">
      </div>
