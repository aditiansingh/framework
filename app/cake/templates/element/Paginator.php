<!-- When width is more than 992px -->
<div class="paginator d-none d-lg-block">
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?php if (!$this->Paginator->hasPrev()) : ?>
            <a class="disabled"><img class="arrow-svg rotate-svg" src="/images/arrow1.svg" alt="First Page"> First</a>
            <a class="disabled"><span class="fa disabled fa-angle-left"></span> Previous</a>
        <?php endif; ?>
        <?= $this->Paginator->first('<img class="arrow-svg rotate-svg" src="/images/arrow2.svg" alt="First Page"> First', array('escape' => false)) ?>
        <?= $this->Paginator->prev('<span class="fa fa-angle-left"></span> Previous', array('disabledTitle' => false, 'escape' => false)) ?>
        <?php if (($this->Paginator->current() >= 4) && ($this->Paginator->total() > 5)) : ?>
            <a>
                ...
            </a>
        <?php endif; ?>
        <?= $this->Paginator->numbers(array('modulus' => 4)) ?>
        <?php if (($this->Paginator->current() <= ($this->Paginator->total() - 3)) && ($this->Paginator->total() > 5)) : ?>
            <a>
                ...
            </a>
        <?php endif; ?>
        <?= $this->Paginator->next('Next <span class="fa fa-angle-right"></span>', array('disabledTitle' => false, 'escape' => false)) ?>
        <?= $this->Paginator->last('Last <img class="arrow-svg" src="/images/arrow2.svg" alt="Last Page">', array('escape' => false)) ?>
        <?php if (!$this->Paginator->hasNext()) : ?>
            <a class="disabled">Next <span class="fa disabled fa-angle-right"></span></a>
            <a class="disabled">Last <img class="arrow-svg" src="/images/arrow1.svg" alt="Last Page"></a>
        <?php endif; ?>
    </ul>
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>

<!-- When width is less than 992px -->
<div class="paginator d-lg-none">
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?php if (!$this->Paginator->hasPrev()) : ?>
            <a class="disabled"><img class="arrow-svg rotate-svg" src="/images/arrow1.svg" alt="First Page"></span></a>
            <a class="disabled d-none d-sm-block"><span class="fa disabled fa-angle-left"></span></a>
        <?php endif; ?>
        <?= $this->Paginator->first('<img class="arrow-svg rotate-svg" src="/images/arrow2.svg" alt="First Page">', array('escape' => false)) ?>
        <?= $this->Paginator->prev('<span class="fa fa-angle-left"></span>', array('disabledTitle' => false, 'escape' => false)) ?>
        <?php if (($this->Paginator->current() >= 4) && ($this->Paginator->total() > 5)) : ?>
            <a>
                ...
            </a>
        <?php endif; ?>
        <?= $this->Paginator->numbers(array('modulus' => 4)) ?>
        <?php if (($this->Paginator->current() <= ($this->Paginator->total() - 3)) && ($this->Paginator->total() > 5)) : ?>
            <a>
                ...
            </a>
        <?php endif; ?>
        <?= $this->Paginator->next('<span class="fa fa-angle-right"></span>', array('disabledTitle' => false, 'escape' => false)) ?>
        <?= $this->Paginator->last('<img class="arrow-svg" src="/images/arrow2.svg" alt="">', array('escape' => false)) ?>
        <?php if (!$this->Paginator->hasNext()) : ?>
            <a class="disable d-none d-sm-block"><span class="fa disabled fa-angle-right"></span></a>
            <a class="disabled"><img class="arrow-svg" src="/images/arrow1.svg" alt=""></a>
        <?php endif; ?>
    </ul>
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>
