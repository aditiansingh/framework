<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifact) ?>
            <legend class="capital-heading"><?= __('Edit Artifact') ?></legend>
            <?php
                echo $this->Form->control('ark_no');
                echo $this->Form->control('credit_id');
                echo $this->Form->control('primary_publication_comments');
                echo $this->Form->control('cdli_collation');
                echo $this->Form->control('cdli_comments');
                echo $this->Form->control('composite_no');
                echo $this->Form->control('condition_description');
                echo $this->Form->control('date_comments');
                echo $this->Form->control('dates_referenced', ['empty' => true]);
                echo $this->Form->control('designation');
                echo $this->Form->control('electronic_publication');
                echo $this->Form->control('elevation');
                echo $this->Form->control('excavation_no');
                echo $this->Form->control('findspot_comments');
                echo $this->Form->control('findspot_square');
                echo $this->Form->control('height');
                echo $this->Form->control('join_information');
                echo $this->Form->control('museum_no');
                echo $this->Form->control('artifact_preservation');
                echo $this->Form->control('is_public');
                echo $this->Form->control('is_atf_public');
                echo $this->Form->control('are_images_public');
                echo $this->Form->control('seal_no');
                echo $this->Form->control('seal_information');
                echo $this->Form->control('stratigraphic_level');
                echo $this->Form->control('surface_preservation');
                echo $this->Form->control('general_comments');
                echo $this->Form->control('thickness');
                echo $this->Form->control('width');
                echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true]);
                echo $this->Form->control('period_id', ['options' => $periods, 'empty' => true]);
                echo $this->Form->control('is_provenience_uncertain');
                echo $this->Form->control('is_period_uncertain');
                echo $this->Form->control('artifact_type_id', ['options' => $artifactTypes, 'empty' => true]);
                echo $this->Form->control('accession_no');
                echo $this->Form->control('accounting_period');
                echo $this->Form->control('alternative_years');
                echo $this->Form->control('dumb2');
                echo $this->Form->control('custom_designation');
                echo $this->Form->control('period_comments');
                echo $this->Form->control('provenience_comments');
                echo $this->Form->control('is_school_text');
                echo $this->Form->control('written_in');
                echo $this->Form->control('is_object_type_uncertain');
                echo $this->Form->control('archive_id', ['options' => $archives, 'empty' => true]);
                echo $this->Form->control('created_by');
                echo $this->Form->control('db_source');
                echo $this->Form->control('weight');
                echo $this->Form->control('translation_source');
                echo $this->Form->control('atf_up');
                echo $this->Form->control('atf_source');
                // echo $this->Form->control('credits._ids', ['options' => $credits]);
                echo $this->Form->control('collections._ids', ['options' => $collections]);
                echo $this->Form->control('dates._ids', ['options' => $dates]);
                echo $this->Form->control('external_resources._ids', ['options' => $externalResources]);
                echo $this->Form->control('genres._ids', ['options' => $genres]);
                echo $this->Form->control('languages._ids', ['options' => $languages]);
                echo $this->Form->control('materials._ids', ['options' => $materials]);
                echo $this->Form->control('publications._ids', ['options' => $publications]);
            ?>
        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifact->id],
                ['class' => 'btn btn-danger float-right',
                'confirm' => __('Are you sure you want to delete # {0}?', $artifact->id)]
            )
        ?>
        </div>

    </div>

</div>
