<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience $provenience
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($provenience) ?>
            <legend class="capital-heading"><?= __('Add Provenience') ?></legend>
            <?php
                echo $this->Form->control('provenience');
                echo $this->Form->control('region_id', ['options' => $regions, 'empty' => true]);
                echo $this->Form->control('geo_coordinates');
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
