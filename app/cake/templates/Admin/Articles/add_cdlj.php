<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">
    <div id="articleEditDiv" class="col-lg-7 boxed">
        <h4>Add Cuneiform Digital Library Journal</h4>
        <hr>
        <?= $this->Form->create(null,array('id' => 'article-form')) ?>
            <div class="form-group">
                <label>Article Title</label>
                <input type="text" name="cdlj_title" class="article_title_input_lg form-control" placeholder="">
                <small class="form-text text-muted">This will be displayed as page headers.</small>
            </div>
            <div class="form-group" style="display: inline-block" >
                <label>Year</label>
                <input type="text" name="year" value="2021" class="article_title_input_sm form-control" placeholder="">
                <small class="form-text text-muted" >This will be displayed as link parameter.</small>
            </div>
            <div class="form-group" style="display: inline-block; margin-left: 5%" >
                <label>Article No.</label>
                <input type="text" name="article_no" value="" class="article_title_input_sm form-control" placeholder="">
                <small class="form-text text-muted" >This will be displayed as link parameter.</small>
            </div>
            <div class="form-group">
                <label for="">Author(s)</label>
                <input id="article_author_input" name="cdlj_authors" type="text" class="article_title_input_lg form-control">
                <div id="input-foot-tags-parent" class="input-foot-tags-parent">
                </div>
            </div>
             <div class="form-group">
                <label for="">Upload CDLJ latex</label>
                <input type='hidden' name="artilce_latex_dir" id="artilce_latex_dir"  value=''>
                <input id="articleImage" name="articleImage" type="file" multiple="multiple" style="display:none;" />
                <textarea style="display:none" id="artilce_html_content" name="artilce_html_content"></textarea>
                <textarea style="display:none" id="article_latex_content" name="article_latex_content"></textarea>

                <div id="uploadCDLJLatex">Upload</div>
                <a id="showConvertHTMLButton" onclick="toggle_latex_preview('show')" class="">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-code-slash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M4.854 4.146a.5.5 0 0 1 0 .708L1.707 8l3.147 3.146a.5.5 0 0 1-.708.708l-3.5-3.5a.5.5 0 0 1 0-.708l3.5-3.5a.5.5 0 0 1 .708 0zm6.292 0a.5.5 0 0 0 0 .708L14.293 8l-3.147 3.146a.5.5 0 0 0 .708.708l3.5-3.5a.5.5 0 0 0 0-.708l-3.5-3.5a.5.5 0 0 0-.708 0zm-.999-3.124a.5.5 0 0 1 .33.625l-4 13a.5.5 0 0 1-.955-.294l4-13a.5.5 0 0 1 .625-.33z"/>
                    </svg>
                    Manage converted HTML
                </a>
            </div>

            <div class="form-group" id="article_pdf_check_add">
                <label for="">Upload CDLJ pdf</label>
                <input type='hidden' name="cdlj_pdf_link" id="cdlj_pdf_link"  value=''>
                <div id="uploadCDLJPdf">Upload</div>
            </div>
            <div class="form-group">
                <label for="">Status</label>
                <select class="form-control" id="" name="cdlj_status">
                    <option value="0">Created</option>
                    <option value="1">Under Review</option>
                    <option value="2">Accepted</option>
                    <option value="3">Published</option>
                    <option value="4">Unpublish</option>
                </select>
                <small class="form-text text-muted">This article will be assigned to the logged in user.</small>
            </div>
            <div id="cdlj_error" style="display:none;" class="alert alert-danger" role="alert">
                Please fill all the required details.
            </div>
             <button id="showPreviewButton" type="button" onclick="toggle_article_preview('show')" class="btn btn-primary">Preview</button>
            <button type="button" onclick="submit_cdlj()" class="btn btn-success">Save</button>
        <?= $this->Form->end() ?>
        <button id="image_manager" style="margin-bottom:5%;display:none;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#manager_modal">
        Image Manager <i class="fa fa-picture-o" aria-hidden="true"></i>
        </button>
        <button id="bib_manager" style="margin-bottom:5%;display:none;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#bib_manager_modal">
        Bib Manager <i class="fa fa-file-text" aria-hidden="true"></i>
        </button>
        <button id="reConvertButton" style="margin-bottom:5%;display:none;" type="button" onclick="re_convert_article()" class="btn btn-primary">
            Refresh Converter <i class="fa fa-refresh" aria-hidden="true"></i>
        </button>
         <button id="closeHTMLPreviewButton" style="display:none" type="button" onclick="toggle_latex_preview('hide')" class="btn btn-primary">
            Close Converter <i class="fa fa-times" aria-hidden="true"></i>
        </button>
         <button id="closePreviewButton" style="display:none" type="button" onclick="toggle_article_preview('hide')" class="btn btn-primary">
            Close Preview <i class="fa fa-times" aria-hidden="true"></i>
        </button>
    </div>
    <div class="col-lg boxed"  id="articleConvertDiv" style="display:none">
        <div class="capital-heading"><?= __('Conversion of CDLJ Latex file to HTML') ?></div>
        <hr>
        <ul class="nav nav-tabs" style="float:right">
            <li class="nav-item">
                <a id="AEditHTML" class="nav-link active" href="#" onclick="toggle_converter_tab('EditHTML')">Edit HTML</a>
            </li>
            <li class="nav-item">
                <a id="AEditLatex" class="nav-link" href="#" onclick="toggle_converter_tab('EditLatex')">Edit Latex</a>
            </li>
            <li class="nav-item">
                <a id="APreviewLogs" class="nav-link disabled" href="#" onclick="toggle_converter_tab('PreviewLogs')">Logs</a>
            </li>
        </ul>
        <div style="clear:both;"></div>
        <div id="myProgress" style="background-color:white">
            <div id="myBar"></div>
        </div>
        <div class="alert alert-primary alert-dismissible fade show" role="alert">
            Tip: <span id="converter_tip_msg">All the details are auto-saved.</span>
        </div>

        <div id="EditHTML" class="preview" style="height:auto;overflow:auto;background-color:white;">
            <div id="article_editor"></div>
        </div>
        <div id="EditLatex" class="preview" style="height:auto;overflow:auto;background-color:white;">
            <textarea id="latex_file_editor" style="height:100vh;width:100%;"></textarea>
        </div>
        <div id="PreviewLogs" class="preview" style="height:auto;overflow:auto;background-color:white;">
            <div id="conversion_success_notify" class="alert alert-warning alert-dismissible fade show" role="alert" style="display:none">
            <span id="conversion_success_msg"></span>
            </div>
            <div id="preview_success_notify" class="alert alert-warning alert-dismissible fade show" role="alert" style="display:none">
            <span id="preview_success_msg"></span>
            </div>
        </div>
    </div>
    <div class="col-lg boxed"  id="articlePreviewDiv" style="display:none">
               <div class="capital-heading"><?= __('Preview') ?></div>
        <div id="myProgress">
            <div id="myBar"></div>
        </div>
        <hr>
        <div class="preview" style="height:80vh;overflow-x:scroll;overflow-y:scroll;background-color:white;padding:5%">
                        <table width="750" border="0" cellpadding="0" cellspacing="5">
                <tr>
                    <td valign="top" width="400">
                    <p style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif; font-size:9pt">
                        Cuneiform Digital Library Journal <br>
                        <b> 2020:xx </b>
                        <br>
                        <font size="1"> ISSN 1540-8779 <br>
                        &#169; <i> Cuneiform Digital Library Initiative </i> </font> &nbsp;
                    </p>
                    </td>
                    <td width="200" height="200" rowspan="2" align="right" nowrap bgcolor="#1461ab">
                    <p style="line-height:15.0pt; font-size:9pt;color:white;">
                        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif"> <a href="/"> CDLI Home </a> <br>
                        <a href=""> CDLI Publications </a> <br>
                        <a href="" target="link"> Editorial Notes </a> <br>
                        <a href="" target="link"> Abbreviations </a> <br>
                        <a href="" target="link"> Bibliography </a> <br>
                        <br>
                        <a href="" target="blank">PDF Version of this Article </a>
                        <br>
                        <a href="" target="link"> Get Acrobat Reader </a> <br>
                        <a href="" target="link"> <font color="#800517"><b>Download Cuneiform Font</b></font> </a> </font>
                    </p>
                    </td>
                    <td width="1" rowspan="2" align="right" nowrap bgcolor="#99CCCC"> &nbsp; </td>
                </tr>
                <tr>
                    <td>
                    <p>
                        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
                        <h2 style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
                        <br>
                        <p id="pArticleName"></p>
                        </h2>
                        <b> <p id="pArticleAuthors"> Name </p</b>
                        <br>
                        <i>University</i>
                        <br>
                        <br>
                        <b> Keywords </b> <br>
                        </font>
                    </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                    <hr align="center" width="600" size="2">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="left">
                    <br>
                    <p>
                    <i><b>Abstract</b><br><br></i>
                    </p>
                    <div id="pArticleContent"></div>

                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<!-- Button trigger modal -->
<!-- Modal for image manager-->
<div class="modal fade bd-example-modal-lg" id="manager_modal" tabindex="-1" role="dialog" aria-labelledby="manager_modal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Image Manager</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="ImageManagerUploadAll"></div>
        <table id="modal-table">

        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" onclick="convert_imgmgr_results()" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal for bib manager-->
<div class="modal fade bd-example-modal-lg" id="bib_manager_modal" tabindex="-1" role="dialog" aria-labelledby="bib_manager_modal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Bib Manager</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div id="BibManagerUpload">

        </div>
              <small class="form-text text-muted" style="text-align:left;margin-bottom:5%;">
              Please upload a bib file to referrence the identified citations in your latex file.
              </small>


        <table id="bib_table">
            <tbody>
                <tr>
                <th>Identified citations
                <small class="form-text text-muted">This is the list of citations identified in your latex file.</small></th>
                <th style="width:60%;">Converted citations to HTML
                <small class="form-text text-muted">This is the populated referrence in HTML format w.r.t the citation.</small></th>
                </tr>
            </tbody>
        </table>
        <div class="bm_wr">

        <table id="bm_1" style="border-top:none;">
            <tr>
           <td>-</td>
           </tr>
        </table>
        <table id="bm_2" style="border-top: none;border-left:none;">
           <tr>
           <td>Need a bib file.</td>
           </tr>
        </table>
        </div>
        <table id="bm_2x" style="display:none">

        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- page script for admin/journals/add_cdlj -->
<script type="text/javascript" async
  src="/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML">
</script>
<script src="/assets/js/ckeditor/ckeditor.js"></script>
<script src="/assets/js/jquery.uploadfile.min.js"></script>
<script>
var article_type = "cdlj";
var addPage = true;
CKEDITOR.replace('article_editor', {
        extraPlugins: 'mathjax',
        mathJaxLib: '/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML',
        height: 530,
        allowedContent: true,
        removeFormatAttributes: '',
    });

</script>
<script src="/assets/js/journals_dashboard.js"></script>
