<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactType $artifactType
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactType) ?>
            <legend class="capital-heading"><?= __('Add Artifact Type') ?></legend>
            <?php
                echo $this->Form->control('artifact_type');
                echo $this->Form->control('parent_id', ['options' => $parentArtifactTypes, 'empty' => true]);
            ?>

<?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>

    </div>

</div>
