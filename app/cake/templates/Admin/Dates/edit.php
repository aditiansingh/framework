<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Date $date
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($date) ?>
            <legend class="capital-heading"><?= __('Edit Date') ?></legend>
            <?php
                echo $this->Form->control('day_no');
                echo $this->Form->control('day_remarks');
                echo $this->Form->control('month_id', ['options' => $months, 'empty' => true]);
                echo $this->Form->control('is_uncertain');
                echo $this->Form->control('month_no');
                echo $this->Form->control('year_id', ['options' => $years, 'empty' => true]);
                echo $this->Form->control('dynasty_id', ['options' => $dynasties, 'empty' => true]);
                echo $this->Form->control('ruler_id', ['options' => $rulers, 'empty' => true]);
                echo $this->Form->control('absolute_year');
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

        <div>
        <?= $this->Form->button(__('Save'),['class'=>'btn btn-success']) ?>
        <?= $this->Form->end() ?>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $date->id],
                ['class' => 'btn btn-danger float-right'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $date->id)]
            )
        ?>
        </div>

    </div>

</div>
