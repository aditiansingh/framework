<?php
   /**
    * @var \App\View\AppView $this
    * @var \App\Model\Entity\Artifact $artifact
    */
    $roles = $this->getRequest()->getSession()->read('Auth.User.roles');
    $name = $this->getRequest()->getSession()->read('Auth.User.username');
    $ifRoleExists = !is_null($roles) ?  array_intersect($roles, [1, 2]) : [];
    $ifRoleExists = !empty($ifRoleExists) ? 1 : 0;
?>
<div class="mr-5">
	<div class="left1">
		 <span class="display-4 dash-txt ls ml-4">Administrative Dashboard </span>
     <span class="display-4 dash-txt ss">Admin Dashboard </span>
		 <div class="dropdown box">
				<span class="fa fa-file-text-o" id="file-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>
				<div class="dropdown-menu shadow">
					 <a class="dropdown-item" href="https://cdli-gh.github.io/guides/guides_intro.html">Documentation</a>
					 <a class="dropdown-item" href="#">Activity</a>
				</div>
		 </div>
	</div>
	<div class="right-add-btn ls">
		 <div class="dropdown">
				<button class="btn cdli-btn-blue" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-plus-circle plus-icon" aria-hidden="true"> <a class="add-btn"> Add</a></span></button>
				<div class="dropdown-menu dropdown-menu-right shadow">
					 <a class="dropdown-item" href="/admin/artifact-types/add">Add Artifact</a>
					 <a class="dropdown-item" href="/admin/authors/add">Add Author</a>
					 <a class="dropdown-item" href="/admin/users/add">Add User</a>
					 <a class="dropdown-item" href="/admin/posting-types/add">Add Posting</a>
					 <a class="dropdown-item" href="/admin/publications/add">Add Reference</a>
				</div>
		 </div>
	</div>
</div>
<div class="emp"></div>
<div class="emp ls"></div>
<div class="accordion my-acc">
    <?= $this->Accordion->partOpen('CDLI Journals and Postings', 'CDLI Journals and Postings', 'h2') ?>
        <div>
          <div class="row mt-3">
     				<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
     					 <div class="d-flex flex-column card-body text-left border">
     							<h3 class="mb-4">Jounal Articles</h3>
     							<p class="admin-text">Our journals: CDLB, J, P and Notes.</p>
     							<div class="mt-auto text-center">
     								 <a href="#"><button id="dashboardbtn11" class="btn cdli-btn-light dashboardbtn1">OJS Dashboard</button></a>
     								 <a href="/admin/articles/add"><button id="dashboardbtn22" class="btn cdli-btn-blue dashboardbtn2">Publish Article</button></a>
     							</div>
     					 </div>
     				</div>
     				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
     					 <div class="d-flex flex-column card-body text-left border">
     							<h3 class="mb-4">Postings</h3>
     							<p class="admin-text">CDLI News, information pages, & Highlights (100's).</p>
     							<div class="mt-auto text-center">
     								 <a href="/postings"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
     								 <a href="/admin/posting-types/add"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
     							</div>
     					 </div>
     				</div>
     				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
     					 <div class="d-flex flex-column card-body text-left border">
     							<h3 class="mb-4">CDLI Tablet</h3>
     							<p class="admin-text">Our mobile app.</p>
     							<div class="mt-auto text-center">
     								 <a href="/cdli-tablet"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
     								 <a href="/admin/cdli-tablet/add"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
     							</div>
     					 </div>
     				</div>
     		 </div>
        </div>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('Bibliography Management', 'Bibliography Management', 'h2') ?>
                <div>
                  <div class="row mt-3">
             				<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">References</h3>
             							<p class="admin-text">Full bibliographic references.</p>
             							<div class="mt-auto text-center">
             								 <a href="/publications"><button class="btn align-self-end cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="/admin/publications/add"><button class="btn align-self-end cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Authors</h3>
             							<p class="admin-text">Scholars, scientists and other contributors of publications and cdli content.</p>
             							<div class="mt-auto text-center">
             								 <a href="/authors"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="/admin/authors/add"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Merge References</h3>
             							<p class="admin-text">References cleaning tool to merge existing references together.</p>
             							<div class="mt-auto text-center">
             								 <a href="#"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="#"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             		 </div>
                </div>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('Image Management', 'Image Management', 'h2') ?>
                <div>
                  <div class="row mt-3">
             				<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Upload Raw Scans</h3>
             							<p class="admin-text">Digital scans of artifacts uplaod, combining and management.</p>
             							<a href="/admin/uploads"><button class="mt-auto btn cdli-btn-blue dashboardbtn3">Add New</button></a>
             					 </div>
             				</div>
             				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Manage Web Images</h3>
             							<p class="admin-text">Management of unlinked / orphan archival images.</p>
             							<div class="mt-auto text-center">
             							<a href="#"><button class="btn cdli-btn-blue dashboardbtn3">Manage Orphan Archival Images</button></a>
             						</div>
             					 </div>
             				</div>
             		 </div>
                </div>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('Users and Staff', 'Users and Staff', 'h2') ?>
                <div>
                  <div class="row mt-3">
             				<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Users</h3>
             							<p class="admin-text">Access and users on this website.</p>
             							<div class="mt-auto text-center">
             								 <a href="/admin/users"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="/admin/users/add"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Staff</h3>
             							<p class="admin-text">Additional information on authors contributing to the cdli.</p>
             							<div class="mt-auto text-center">
             								 <a href="#"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="#"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             		 </div>
                </div>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('Data and Metadata Entities', 'Data and Metadata Entities', 'h2') ?>
                <div>
                  <?php if ($ifRoleExists) { ?>
             		 <p class="more-ent-mobile btn cdli-btn-light moreentity-btn dropdown-toggle mt-3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More Entities</p>
             		 <ul class="dropdown-menu dropdown-menu1 shadow multi-column columns-3">
             				<div class="row">
             					 <div class="col-sm-4">
             							<ul class="multi-column-dropdown">
             								 <li><a href="/artifact-types">Artifact Types</a></li>
             								 <li><a href="/regions">Regions</a></li>
             								 <li><a href="/periods">Periods</a></li>
             								 <li><a href="/proveniences">Proveniences</a></li>
             								 <li><a href="/rulers">Rulers</a></li>
             							</ul>
             					 </div>
             					 <div class="col-sm-4">
             							<ul class="multi-column-dropdown">
             								 <li><a href="/genres">Genres</a></li>
             								 <li><a href="/materials">Materials</a></li>
             								 <li><a href="/material-aspects">Materials Aspects</a></li>
             								 <li><a href="/material-colors">Materials Colors</a></li>
             							</ul>
             					 </div>
             					 <div class="col-sm-4">
             							<ul class="multi-column-dropdown">
             								 <li><a href="/collections">Collections</a></li>
             								 <li><a href="/dynasties">Dynasties</a></li>
             								 <li><a href="/languages">Languages</a></li>
             								 <li><a href="/dates">Dates</a></li>
             							</ul>
             					 </div>
             				</div>
             		 </ul>
             		 <?php } ?>
             		 <div class="row mt-4">
             				<div class="bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Archives</h3>
             							<p class="admin-text">Historical archives of cuneiform tablets.</p>
             							<div class="mt-auto text-center">
             								 <a href="/archives"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="/admin/proveniences/add"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Journals</h3>
             							<p class="admin-text">List of periodicals which publish articles relevant to the field.</p>
             							<div class="mt-auto text-center">
             								 <a href="/journals"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="/admin/journals/add"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">External Resources</h3>
             							<p class="admin-text">Pojects and other ressources which can be linked to artifacts and other entities (eg. BDTNS)</p>
             							<div class="mt-auto text-center">
             								 <a href="/external-resources"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="/admin/external-resources/add"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Abbreviations</h3>
             							<p class="admin-text">List of abbreviation for the field.</p>
             							<div class="mt-auto text-center">
             								 <a href="/abbreviations"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="/admin/abbreviations/add"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Agade Mails</h3>
             							<p class="admin-text">Cooked version of the agade mails.</p>
             							<button href="/agade-mails" class="mt-auto btn cdli-btn-light dashboardbtn3">Index</button>
             					 </div>
             				</div>
             				<div class="mid-sp bs4-cdli-card-horizontal border-0 card col-md-12 col-lg-4 bot-sp">
             					 <div class="d-flex flex-column card-body text-left border">
             							<h3 class="mb-4">Sign Readings</h3>
             							<p class="admin-text">Sign readings and management of preferred values.</p>
             							<div class="mt-auto text-center">
             								 <a href="/sign-readings"><button class="btn cdli-btn-light dashboardbtn1">Index</button></a>
             								 <a href="#"><button class="btn cdli-btn-blue dashboardbtn2">Add New</button></a>
             							</div>
             					 </div>
             				</div>
             		 </div>
                </div>
        <?= $this->Accordion->partClose() ?>
</div>
<div class="ss right-add-btn mt-4 mb-4">
      <div class="dropdown">
         <img src="/images/Group146.svg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <div class="dropdown-menu dropdown-menu-right shadow">
            <a class="dropdown-item" href="/admin/artifact-types/add">Add Artifact</a>
            <a class="dropdown-item" href="/admin/authors/add">Add Author</a>
            <a class="dropdown-item" href="/admin/users/add">Add User</a>
            <a class="dropdown-item" href="/admin/posting-types/add">Add Posting</a>
            <a class="dropdown-item" href="/admin/publications/add">Add Reference</a>
         </div>
      </div>
   </div>

<?php echo $this->element('smoothscroll'); ?>
