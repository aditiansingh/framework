<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<main class="row justify-content-md-center">

    <div class="login-box">
        <div class="capital-heading text-center">
            <h1 class="display-4 header-txt">Login</h1>
        </div>
        <?= $this->Flash->render() ?>

        <?= $this->Form->create() ?>
            <?= $this->Form->control('username', [
                'class' => 'col-12 input-background-child-md'
            ]) ?>
            <?= $this->Form->control('password', [
                'class' => 'col-12 input-background-child-md'
            ]) ?>
            <?= $this->Html->link('Forgot password?', [
                'controller' => 'Forgot',
                'action' => 'index',
                'password'
            ]) ?>
            <div class="userloginbuttoncenter">
                <?= $this->Form->submit('Submit', [
                    'class' => 'btn cdli-btn-blue'
                ]) ?>
            </div>
        <?= $this->Form->end() ?>
    </div>

</main>
