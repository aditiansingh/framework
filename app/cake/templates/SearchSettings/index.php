<main>
	<div class="ads searchSettings">
    <?= $this->Form->create(null, ['type'=>'post']) ?>
		<div class="">
			<?php if($isSettingsUpdated) :?>
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<span class="fa fa-check-circle mr-2"></span>
					<?php echo "Search settings saved successfully" ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php endif;?>	
			<div class="d-flex align-items-center justify-content-between mb-3">
				<h1 class="display-3 text-left advanced-title">Search Settings</h4>
				<?= $this->Form->button(
    'Save',
    [
                        'type' => 'submit',
                        'class' => 'search-btn btn d-none d-md-block px-5'
                    ]
); ?>
			</div>
		</div>

		<div class="layout-grid text-left">
			<div class="grid-1">
				<div class="darken">
					<p class="form-title mb-0">Search Result options</p>
					<span class="subscript-txt">Number of search results per page</span>

					<div class="mt-4">
						<label for="pageSize">Results per page:
							<?= $this->Form->select(
    'PageSize',
    $SearchSettingOption['PageSize'],
    [
                                    'value' => $searchSettings['PageSize'],
                                    'empty' => false,
									'class' => 'ss-sort-select mb-0',
									'id' => 'pageSize'
                                ]
); ?>
						</label>
					</div>

				</div>

				<div class="darken ads-space-top">
					<p class="form-title mb-0">filter options</p>
					<span class="subscript-txt">Add/remove filter options in sidebar</span>

					<div class="mt-4">
						<p class="ss-filter-head">Object data</p>
						<fieldset>
							<legend class="d-none">Object data list</legend>
							<?= $this->Form->select('object',
								$SearchSettingOption['Filter']['Object'], [
									'multiple' => 'checkbox',
									'value' => $searchSettings['object']
								]
							); ?>
						</fieldset>
					</div>

					<div class="mt-4">
						<p class="ss-filter-head">Textual data</p>
						<fieldset>
							<legend class="d-none">Textual data list</legend>
							<?= $this->Form->select('textual',
								$SearchSettingOption['Filter']['Textual'], [
									'multiple' => 'checkbox',
									'value' => $searchSettings['textual']
								]
							); ?>
						</fieldset>
					</div>

					<div class="mt-4">
						<p class="ss-filter-head">Publication data</p>
						<fieldset>
							<legend class="d-none">Publication data list</legend>
							<?= $this->Form->select('publication',
								$SearchSettingOption['Filter']['Publication'], [
									'multiple' => 'checkbox',
									'value' => $searchSettings['publication']
								]
							); ?>
						</fieldset>
					</div>
					
				</div>

				<div class="darken ads-space-top">
					<p class="form-title mb-0">Compact view options</p>
					<span class="subscript-txt">Add/remove elements in compact view</span>

					<div>
						<p class="ss-filter-head mt-4">
						Show/hide filter sidebar (compact)
						<p>
						
						<fieldset>
							<legend class="d-none">Sidebar visibility(compactview) options </legend>
							<?= $this->Form->radio('compact:sidebar',
								$SearchSettingOption['Filter']['Sidebar'], [
									'multiple' => true,
									'value' => $searchSettings['compact:sidebar']
								]
							)?>
						</fieldset>

						<p class="ss-filter-head mt-4">
						Add/remove fields from search results
						<p>

						<div class="ml-3">
							<div class="mt-4">
								<p class="ss-filter-head">Object data</p>
								<fieldset>
									<legend class="d-none">Sidebar Object data list</legend>
									<?= $this->Form->select('compact:object',
										$SearchSettingOption['Filter']['Object'], [
											'multiple' => 'checkbox',
											'value' => $searchSettings['compact:object']
										]
									); ?>
								</fieldset>
							</div>

							<div class="mt-4">
								<p class="ss-filter-head">Textual data</p>
								<fieldset>
									<legend class="d-none">Sidebar Textual data list</legend>
									<?= $this->Form->select('compact:textual', 
										$SearchSettingOption['Filter']['Textual'], [
											'multiple' => 'checkbox',
											'value' => $searchSettings['compact:textual']
										]
									); ?>
								</fieldset>
							</div>

							<div class="mt-4">
								<p class="ss-filter-head">Publication data</p>
								<fieldset>
									<legend class="d-none">Sidebar Publication data list</legend>
									<?= $this->Form->select('compact:publication',
										$SearchSettingOption['Filter']['Publication'], [
											'multiple' => 'checkbox',
											'value' => $searchSettings['compact:publication']
										]
									); ?>
								</fieldset>
							</div>
						</div>
					</div>
						
				</div>
			</div>
			
            <div class="grid-2">
			<div class="darken h-fit-content">
				<p class="form-title mb-0">Full view options</p>
				<span class="subscript-txt">Add/remove elements in full view</span>

				<div>
					<p class="ss-filter-head mt-4">
					Show/hide filter sidebar (desktop)
					<p>

					<fieldset>
						<legend class="d-none">Sidebar visibility(fullview) options </legend>
						<?= $this->Form->radio('full:sidebar',
							$SearchSettingOption['Filter']['Sidebar'], [
								'multiple' => true,
								'value' => $searchSettings['full:sidebar']
							]
						); ?>
					</fieldset>

					<p class="ss-filter-head mt-4">
					Add/remove fields from search results
					<p>

					<div class="ml-3">
						<div class="mt-4">
							<p class="ss-filter-head">Object data</p>
							<fieldset>
								<legend class="d-none">Fullview Object data list</legend>
								<?= $this->Form->select('full:object',
									$SearchSettingOption['Filter']['Object'], [
										'multiple' => 'checkbox',
										'value' => $searchSettings['full:object']
									]
								); ?>
							</fieldset>
						</div>

						<div class="mt-4">
							<p class="ss-filter-head">Textual data</p>
							<fieldset>
								<legend class="d-none">Fullview Textual data list</legend>
								<?= $this->Form->select('full:textual',
									$SearchSettingOption['Filter']['Textual'], [
										'multiple' => 'checkbox',
										'value' => $searchSettings['full:textual']
									]
								); ?>
							</fieldset>
						</div>

						<div class="mt-4">
							<p class="ss-filter-head">Publication data</p>
							<fieldset>
								<legend class="d-none">Fullview Publication data list</legend>
								<?= $this->Form->select('full:publication',
									$SearchSettingOption['Filter']['Publication'], [
										'multiple' => 'checkbox',
										'value' => $searchSettings['full:publication']
									]
								); ?>
							</fieldset>
						</div>
					</div>
				</div>

			</div>
			</div>
		</div>
		
		<?= $this->Form->button(
                                        'Save',
                                        [
            'type' => 'submit',
            'class' => 'wide-btn btn cdli-btn-blue mt-5'
            ]
                                    ); ?>
		<?= $this->Form->button(
                            'Reset',
                            [
            'name' => 'Reset',
            'type' => 'submit',
            'class' => 'wide-btn btn cdli-btn mt-5'
            ]
                        ); ?>
	<?= $this->Form->end()?>
</div>
<hr class="line"/>
<?php echo $this->element('smoothscroll'); ?>
</main>