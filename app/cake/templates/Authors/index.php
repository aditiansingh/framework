<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author[]|\Cake\Collection\CollectionInterface $authors
 */
?>


<h1 class="display-3 header-txt text-left">Authors Index</h1>
<?php foreach (range('A', 'Z') as $char): ?>
    <a href="./authors?letter=<?php  echo $char; ?>" class="btn btn-action">
        <?php echo $char?>
    </a>
<?php endforeach ?>

<table class="table-bootstrap my-3 mx-0">
    <thead>
        <tr>
            <th scope="col">Author Name</th>
            <th scope="col">Institution</th>
            <th scope="col">Email</th>
            <th scope="col">ORCID ID</th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($authors as $author): ?>
        <tr>
            <td><?= $this->Html->link($author->author, ['prefix' => false, 'action' => 'view', $author->id]) ?></td>
            <td><?= h($author->institution) ?></td>
            <td><?= h($author->email) ?></td>
            <td><?= h($author->orcid_id) ?></td>
            <td>
            <?php if ($access_granted): ?>
                <?= $this->Html->link(__('Edit'),['prefix' => 'Admin', 'action' => 'edit', $author->id],
                        ['escape' => false , 'class' => "btn btn-warning btn-sm", 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(__('Delete'), ['prefix'=>'Admin','action' => 'delete',  $author->id], 
                        ['confirm' => __('Are you sure you want to delete # {0}?',  $author->id),'escape'=>false,'class'=>"btn btn-danger btn-sm"]) ?>
            <?php endif ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>

