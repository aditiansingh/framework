<div>
   <?= $this->Html->css('3dviewer'); ?>
   <?= $this->Html->script('3dviewer/jquery-3.1.0.min'); ?>
   <?= $this->Html->script('3dviewer/three.min'); ?>
   <?= $this->Html->script('3dviewer/PLYLoader'); ?>
   <?= $this->Html->script('3dviewer/Detector'); ?>
   <?= $this->Html->script('3dviewer/Arcball'); ?>
   <?= $this->Html->script('3dviewer/InteractiveViewer'); ?>
   <?= $this->Html->charset(); ?>
   
</div>
 
  

   <div class="canvas">
       <div class="threeDcontainer" id="canvas" style="border: none;">
       </div>
       <div id="progress">
           <div id="progressGif"><img src="../images/3dviewericons/progress.gif"></div>
           <div id="message">Loading 3D model data ...</div>     
       </div>
   </div>
       <p class="infoText">3D model view of CDLI <a id="cdlilink"><?php echo "$CDLI_NO"; ?></a></p>
       <p class="infoText">3D Viewer Designed by <a href="http://virtualcuneiform.org/">The Virtual Cuneiform Tablet Reconstruction Project</a></p>
