<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 */

$title = [
    'cdlb' => 'Cuneiform Digital Library Bulletins',
    'cdlj' => 'Cuneiform Digital Library Journals',
    'cdln' => 'Cuneiform Digital Library Notes',
    'cdlp' => 'Cuneiform Digital Library Preprints'
][$type];

$edit_type = [
    'cdlb' => 'CDLB',
    'cdlj' => 'CDLJ',
    'cdln' => 'CDLN',
    'cdlp' => 'CDLP'
][$type];

$hasHtml = $type != 'cdlp';
$hasPdf = $type != 'cdln';
?>

<div class="container">
  <div class="row">
    <div class="col-10 text-left">
      <h3 class="display-4 pt-3 text-left pl-0">
        <?= __($title) ?>
      </h3>
    </div>
    <?php if ($type == 'cdlj' || $type == 'cdlb'): ?>
    <div class="col-2">
      <div class="dropdown pt-4 text-right">
        <button class="btn cdli-btn-blue dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Open Journal Options
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <!-- ojs container should be running
               change the static links          -->
          <a class="dropdown-item" href="http://localhost:8081/index.php/articles/submissions">Articles Submission and Review Dashboard</a>
          <a class="dropdown-item" href="http://localhost:8081/index.php/articles/about/submissions">Guidelines for submission</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
     <p class="text-justify page-summary-text mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
  </div>
</div>
<?php if ($access_granted): ?>
<div class="btn-group float-right ">
  <div class="dropdown display: inline-block;">
    <button type="button" class=" filter_icon btn cdli-btn-blue" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
       <span class="fa fa-filter" aria-hidden="true"></span> 
    </button>
  </div>
    <div style="display: inline-block;">
      <select class="filter_btn form-select" aria-label=".form-select">
        <option selected>Published</option>
        <option value="1">Reviewed</option>
      </select>
    </div>
</div>
</br>
<?php endif ?>
<?php endif ?>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Author(s)</th>
            <th scope="col">Title</th>
            <th scope="col">Date</th>
            <?php if ($access_granted): ?>
                <th scope="col"><?= __('Action') ?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody align="left" class="journals-view-table">
        <?php foreach ($articles as $article):  ?>
            <tr>
                <?php if ($hasHtml): ?>
                  <td width="5%"><?= $article['year'].':'.$article['article_no']; ?></td>
                <?php endif; ?>
                <?php if (!($hasHtml)): ?>
                  <td width="5%"><?= $article['article_no']; ?></td>
                <?php endif; ?>
                <td width="25%">
                    <?php foreach ($article['authors'] as $author):
                        $end = $author == end($article['authors']) ? '' : '; '; ?>
                        <a href="/authors/<?= $author['id'] ?>"><?=
                            $author['author']
                        ?></a><?= $end ?>
                    <?php endforeach; ?>
                </td>
                <?php if ($hasHtml): ?>
                    <td width="40%"><a href="<?= $article['article_type']; ?>/<?= $article['id']; ?>"><?= $article['title']; ?></a></td>
                <?php endif; ?>
                <?php if (!($hasHtml)): ?>
                <td width="40%"><a href="https://cdli.ucla.edu/files/publications/<?= $article['pdf_link']; ?>"><?= $article['title']; ?></a></td>
                <?php endif; ?>
                <td width="15%"><?= date("Y-m-d", strtotime($article['created'])); ?></td>
                <?php if ($access_granted): ?>
                <td>
                    <a class="btn btn-warning btn-sm" href="/admin/articles/edit/<?= $edit_type; ?>/<?= $article['id']; ?>">Edit</a>
                </td>
                <?php endif ?>           
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php echo $this->element('Paginator'); ?>
