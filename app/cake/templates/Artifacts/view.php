<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */

$CDLI_NO = $artifact->getCdliNumber();
$this->assign('title', $artifact->designation . ' (' . $CDLI_NO . ')');
$this->assign('image', '/dl/photo/' . $CDLI_NO . '.jpg');

$flatCatalogues = ['CSV' => 'csv','XLSX' => 'xlsx','TSV' => 'tsv','PDF'=>'pdf'];
$expandedCatalogues = ['JSON' => 'json'];
$linkedCatalogues = ['TTL' => 'ttl','JSON-RDF' => 'json','XML-RDF'=>'xml'];
$textData = ['ATF' => 'atf' ,'JTF'=>'jtf', 'PDF'=>'pdf'];
$linkedAnnotations = ['TTL' => 'ttl', 'JSON-RDF'=>'json-rdf' ,'XML-RDF' => 'xml'];
$relatedData = ['Sign list', 'Vocabulary list'];
$imageTypes = ['High resolution', 'Low resolution'];
$docId = explode("/", $_SERVER['REQUEST_URI'])[2];

function processInscriptions($line) {
    //Translitertaions
    $processedLine = "";
    if (preg_match("/^[0-9]+/", $line)) {
        $processedLine = $line."<br>";
    }
    //Translations 
    elseif (preg_match("/^(#tr)/", $line)) {
        $line = substr($line, 5);
        $processedLine =  "<i> &emsp;".$line."</i> <br>";
    }
    //Comments
    elseif (preg_match("/^(\$ )/", $line) || preg_match("/^(# )/", $line) ) {
        $line = substr($line, 2);
        $processedLine =  "<i> &emsp;".$line."</i><br>";
    }
    //structure
    elseif (preg_match("/^@/", $line))  {
        $line = substr($line, 1);
        $processedLine =  "<b>".$line."</b><br>";
    }
    return $processedLine;
}

?>

<main id="artifact">
    <a id="back" href="#" onclick="history.back()"><i class="fa fa-chevron-left"></i> Back to Search Results</a>
    <h1 class="display-3 header-txt"><?= h($artifact->designation) ?> (<?= h($CDLI_NO) ?>)</h1>
    <h2 class="my-4 artifact-desc">
        <?php if (!empty($artifact->genres)): ?>
            <?= h($artifact->genres[0]->genre) ?>,
        <?php endif; ?>
        <?php if (!empty($artifact->artifact_type)): ?>
            <?= h($artifact->artifact_type->artifact_type) ?>,
        <?php endif; ?>
        <?php if (!empty($artifact->provenience)): ?>
            <?= h($artifact->provenience->provenience) ?>
        <?php endif; ?>
        <?php if (!empty($artifact->period)): ?>
            in <?= h($artifact->period->period) ?>
        <?php endif; ?>
        <?php if (!empty($artifact->collections)): ?>
            and kept at <?= h($artifact->collections[0]->collection) ?>
        <?php endif; ?>
    </h2>

    <div class="export-grid">
        <?=$this->Dropdown->open("Export artifact <i class='fa fa-chevron-down' aria-hidden='true'></i>")?>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <h3>Metadata / catalogue</h3>
                        <h4 class="ml-4">Flat catalogue</h4>
                        <?php foreach ($flatCatalogues as $format => $link): ?>
                                    <?= $this->Html->link("Get {$format}", "/artifacts/{$docId}/{$link}", [
                                        'controller' => 'Artifacts',
                                        'action' => 'view',
                                        $artifact->id,
                                        'class' => 'ml-5'
                                        // '_ext' => $format,
                                    ])?>
                        <?php endforeach;?>

                        <h4 class="ml-4">Expanded catalogue</h4>
                        <?php foreach ($expandedCatalogues as $format => $link): ?>
                                    <?= $this->Html->link("Get {$format}", "/artifacts/{$docId}/{$link}", [
                                        'controller' => 'Artifacts',
                                        'action' => 'view',
                                        $artifact->id,
                                        'class' => 'ml-5'
                                        // '_ext' => $format,
                                    ])?>
                        <?php endforeach;?>

                        <h4 class="ml-4">Linked catalogue</h4>
                        <?php foreach ($linkedCatalogues as $format => $link): ?>
                                    <?= $this->Html->link("Get {$format}", "/artifacts/{$docId}/{$link}", [
                                        'controller' => 'Artifacts',
                                        'action' => 'view',
                                        $artifact->id,
                                        'class' => 'ml-5'
                                        // '_ext' => $format,
                                    ])?>
                        <?php endforeach;?>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <h3>Text / annotations</h3>
                    <h4 class="ml-4">Text data</h4>
                    <?php foreach ($textData as $format => $link): ?>
                                <?= $this->Html->link("As {$format}", "/artifacts/{$docId}/{$link}", [
                                    'controller' => 'Artifacts',
                                    'action' => 'view',
                                    $artifact->id,
                                    'class' => 'ml-5'
                                    // '_ext' => $format,
                                ])?>
                    <?php endforeach;?>

                    <h4 class="ml-4">Annotation data</h4>
                                <?= $this->Html->link('CDLI-CoNLL', [
                                    'controller' => 'Artifacts',
                                    'action' => 'view',
                                    $artifact->id,
                                    // '_ext' => $format,
                                ], ['class' => 'ml-5'])?>

                    <h4 class="ml-4">Linked annotations</h4>
                    <?php foreach ($linkedAnnotations as $format => $link): ?>
                                <?= $this->Html->link("As {$format}", "/artifacts/{$docId}/{$link}", [
                                    'controller' => 'Artifacts',
                                    'action' => 'view',
                                    $artifact->id,
                                    'class' => 'ml-5'
                                    // '_ext' => $format,
                                ])?>
                    <?php endforeach;?>

                    <h4 class="ml-4">Related data</h4>
                    <?php foreach ($relatedData as $format): ?>
                                <?= $this->Html->link('As '.$format, [
                                    'controller' => 'Artifacts',
                                    'action' => 'view',
                                    $artifact->id,
                                    // '_ext' => $format,
                                ], ['class' => 'ml-5'])?>
                    <?php endforeach;?>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <h3>Images</h3>
                    <?php foreach ($imageTypes as $format): ?>
                                <?= $this->Html->link('As '.$format, [
                                    'controller' => 'Artifacts',
                                    'action' => 'view',
                                    $artifact->id,
                                    '_ext' => $format,
                                ], [
                                    'class' => 'ml-5'
                                ])?>
                            <?php endforeach;?>
                </div>
            </div>
        <?=$this->Dropdown->close()?>
    </div>

    <?php $CDLI_NO = "P" . str_pad($artifact->id, 6, '0', STR_PAD_LEFT) ?>
    <div id="artifact-media">
        <div>
            <a href="/dl/photo/<?= $CDLI_NO ?>.jpg">
                <figure>
                    <img src="/dl/tn_photo/<?= $CDLI_NO ?>.jpg" alt="Artifact impressions"/>
                    <figcaption>View full image <i class="fa fa-external-link"></i></figcaption>
                </figure>
            </a>
        </div>
        <footer class="d-none d-md-block">
          <a href="/dl/line_art/<?= $CDLI_NO ?>_ld.jpg">View line art</a>
          &nbsp;
          &bullet;
          &nbsp;
          <?php
           $model = WWW_ROOT . 'dl' . DS . 'vcmodels' . DS . $CDLI_NO . '.ply';
           $texture = WWW_ROOT . 'dl' . DS . 'vcmodels' . DS . $CDLI_NO . '.jpg'; 
           if (file_exists($model))
            {
              if (file_exists($texture))
              {
                echo "<a href=/3dviewer/$CDLI_NO>View in 3D</a> &nbsp";
              }
            }
          ?>
          View RTI:
          &nbsp;
          <u>Observe</u>
          &nbsp;
          <u>Reverse</u>
        </footer>

        <footer class="d-md-none">
          <a href="/dl/line_art/<?= $CDLI_NO ?>_ld.jpg">View line art</a>                    
            <div class="mt-3 mb-0 p-0">
                View RTI:
                &nbsp;
                <u>Observe</u>
                &nbsp;
                <u>Reverse</u>
            </div>
        </footer>
    </div>

    <div class="artifact-summary font-weight-light">
        <h2 class="font-weight-light">Summary</h2>
        <div class="mt-4 d-block d-md-flex justify-content-between">
            <div>
                <p>Musuem Collection</p>
                <?php foreach ($artifact->collections as $collection): ?>
                    <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
                <?php endforeach; ?>

                <p>Period</p>
                <?php if (!empty($artifact->period)): ?>
                    <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
                <?php endif; ?>

                <p>Provenience</p>
                <?php if (!empty($artifact->provenience)): ?>
                    <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
                <?php endif; ?>

                <p>Artifact Type</p>
                <?php if (!empty($artifact->artifact_type)): ?>
                    <?= $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
                <?php endif; ?>
            </div>


            <div>
                <p>Material</p>
                <?php foreach ($artifact->materials as $material): ?>
                    <?= $this->Html->link($material->material, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
                <?php endforeach; ?>

                <p>Genre / Subgenre</p>
                <?php foreach ($artifact->genres as $genre): ?>
                    <?= $this->Html->link($genre->genre, ['controller' => 'Genres', 'action' => 'view', $genre->id]) ?>
                <?php endforeach; ?>

                <p>Language</p>
                <?php foreach ($artifact->languages as $language): ?>
                    <?= $this->Html->link($language->language, ['controller' => 'Languages', 'action' => 'view', $language->id]) ?>
                <?php endforeach; ?>
            </div>

        </div>
    </div>

    <div class="artifact-detail accordion">
        <?php if (!empty($artifact->inscription)): ?>
            <?php
                //split seperate lines into array
                $lines = preg_split('/\r\n|\r|\n/', $artifact->inscription->atf);
                $processedATF = '';
                foreach($lines as $line) {
                    if ($line == "" || substr($line, 0, 2) == ">>") {
                        continue;
                    }
                    $processedLine = processInscriptions($line);
                    if (!empty($processedLine)) {
                        $processedATF .= $processedLine;
                    }
                }
            ?>
            <?= $this->Accordion->partOpen('inscriptions', 'Inscriptions', 'h2') ?>
            <div>
                <p>No. <?= h($artifact->inscription->id) ?>:</p>
                <?= $processedATF ?>
                <br/>
            </div>
            <?= $this->Accordion->partClose() ?>
        <?php endif; ?>

        <?= $this->Accordion->partOpen('collections', 'Collections', 'h2') ?>
            <ul>
                <?php foreach ($artifact->collections as $collection): ?>
                <li>
                    <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
                </li>
                <?php endforeach; ?>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('physical_information', 'Physical Information', 'h2') ?>
            <ul>
                <li>
                    <?= __('Object type') ?>:
                    <?php foreach ($artifact->genres as $genre): ?>
                        <?= $this->Html->link($genre->genre, ['controller' => 'Genres', 'action' => 'view', $genre->id]) ?>
                    <?php endforeach; ?>
                </li>
                <li>
                    <?= __('Is Object Type Uncertain') ?>:
                    <?= $artifact->is_object_type_uncertain ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Material') ?>:
                    <?php foreach ($artifact->materials as $material): ?>
                        <?= $this->Html->link($material->material, ['controller' => 'Materials', 'action' => 'view', $material->id]) ?>
                    <?php endforeach; ?>
                </li>
                <li>
                    <?= __('Measurements (mm)') ?>:
                    <?= h($artifact->height) ?> x
                    <?= h($artifact->width) ?> x
                    <?= h($artifact->thickness) ?>
                </li>
                <li>
                    <?= __('Weight') ?>:
                    <?= h($artifact->weight) ?>
                </li>
                <li>
                    <?= __('Artifact Preservation') ?>:
                    <?= h($artifact->artifact_preservation) ?>
                </li>
                <li>
                    <?= __('Condition Description') ?>:
                    <?= h($artifact->condition_description) ?>
                </li>
                <li>
                    <?= __('Join Information') ?>:
                    <?= h($artifact->join_information) ?>
                </li>
                <li>
                    <?= __('Seal no.') ?>:
                    <?= h($artifact->seal_no) ?>
                </li>
                <li>
                    <?= __('Seal information') ?>:
                    <?= $this->Text->autoParagraph(h($artifact->seal_information)) ?>
                </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('publications', 'Publication Data', 'h2') ?>
            <ul>
                <?php foreach ($artifact->publications as $publication): ?>
                <li>
                    <?= $this->Html->link($publication->_joinData->publication_type, ['controller' => 'Publications', 'action' => 'view', $publication->id]) ?>
                    <?php if (!empty($publication->designation)): ?>
                        : <?= h($publication->designation) ?>
                    <?php endif; ?>
                    <?php foreach ($publication->authors as $author): ?>
                        (<?= $this->Html->link($author->author, ['controller' => 'Authors', 'action' => 'view', $author->id]) ?>)
                    <?php endforeach; ?>
                </li>
                <?php endforeach; ?>
            </ul>

            <?php if (strlen($artifact->primary_publication_comments)) :?>
                <h3 class="artifact-sub-heading">Author comments:</h3>
                <?= $this->Text->autoParagraph(h($artifact->primary_publication_comments)) ?>
            <?php endif;?>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('identifiers', 'Identifiers', 'h2') ?>
            <ul>
              <li>
                  <?= __('Ark No.') ?>:
                  <?= h($artifact->ark_no) ?>
              </li>
              <li>
                  <?= __('Composite No.') ?>:
                  <?= h($artifact->composite_no) ?>
              </li>
              <li>
                  <?= __('Museum No.') ?>:
                  <?= h($artifact->museum_no) ?>
              </li>
              <li>
                  <?= __('Accession No.') ?>:
                  <?= h($artifact->accession_no) ?>
              </li>
              <li>
                  <?= __('Designation') ?>:
                  <?= h($artifact->designation) ?>
              </li>
              <li>
                  <?= __('Custom Designation') ?>:
                  <?= h($artifact->custom_designation) ?>
              </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('provenience', 'Provenience', 'h2') ?>
            <ul>
                <li>
                    <?= __('Provenience') ?>:
                    <?php if (!empty($artifact->provenience)): ?>
                      <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?= __('Is Provenience Uncertain') ?>:
                    <?= $artifact->is_provenience_uncertain ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Provenience Comments') ?>:
                    <?= $this->Text->autoParagraph(h($artifact->provenience_comments)); ?>
                </li>
                <li>
                    <?= __('Elevation') ?>:
                    <?= h($artifact->elevation) ?>
                </li>
                <li>
                    <?= __('Stratigraphic Level') ?>:
                    <?= h($artifact->stratigraphic_level) ?>
                </li>
                <li>
                    <?= __('Excavation No') ?>:
                    <?= h($artifact->excavation_no) ?>
                </li>
                <li>
                    <?= __('Findspot Square') ?>:
                    <?= h($artifact->findspot_square) ?>
                </li>
                <li>
                    <?= __('Findspot Comments') ?>:
                    <?= $this->Text->autoParagraph(h($artifact->findspot_comments)) ?>
                </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('chronology', 'Chronology', 'h2') ?>
            <ul>
                <li>
                    <?= __('Period') ?>:
                    <?php if (!empty($artifact->period)): ?>
                      <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
                    <?php endif; ?>
                </li>
                <li>
                    <?= __('Is Period Uncertain') ?>:
                    <?= $artifact->is_period_uncertain ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Period Comments') ?>:
                    <?= $this->Text->autoParagraph(h($artifact->period_comments)) ?>
                </li>
                <li>
                    <?= __('Accounting Period') ?>:
                    <?= $this->Number->format($artifact->accounting_period) ?>
                </li>
                <li>
                    <?= __('Dates Referenced') ?>:
                    <?= h($artifact->dates_referenced) ?>
                </li>
                <li>
                    <?= __('Date Comments') ?>:
                    <?= h($artifact->date_comments) ?>
                </li>
                <li>
                  <?= __('Alternative Years') ?>:
                  <?= h($artifact->alternative_years) ?>
                </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('credits', 'Credits', 'h2') ?>
            <ul>
              <?php foreach ($artifact->credits as $credit): ?>
              <li>
                  <?= $this->Html->link($credit->author->author, ['controller' => 'Authors', 'action' => 'view', $credit->author->id]) ?>
                  (<?= h($credit->date) ?>)<!--
                  --><?php if (!empty($credit->comments)): ?>: <?= h($credit->comments) ?><?php endif; ?>
              </li>
              <?php endforeach; ?>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('administrative', 'Administrative', 'h2') ?>
            <ul>
                <li>
                    <?= __('CDLI Collation') ?>:
                    <?= h($artifact->cdli_collation) ?>
                </li>
                <li>
                    <?= __('Created') ?>:
                    <?= h($artifact->created) ?>
                </li>
                <li>
                    <?= __('Created By') ?>:
                    <?= h($artifact->created_by) ?>
                </li>
                <li>
                    <?= __('Modified') ?>:
                    <?= h($artifact->modified) ?>
                </li>
                <li>
                    <?= __('Is Public') ?>:
                    <?= $artifact->is_public ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Is Atf Public') ?>:
                    <?= $artifact->is_atf_public ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Are Images Public') ?>:
                    <?= $artifact->are_images_public ? __('Yes') : __('No'); ?>
                </li>
                <li>
                    <?= __('Is School Text') ?>:
                    <?= $artifact->is_school_text ? __('Yes') : __('No'); ?>
                </li>
            </ul>
        <?= $this->Accordion->partClose() ?>

        <?= $this->Accordion->partOpen('external_resources', 'External Resources', 'h2') ?>
            <ul>
                <?php foreach ($artifact->external_resources as $external_resource): ?>
                <li>
                    <?= $this->Html->link(
                                    $external_resource->external_resource,
                                    $external_resource->base_url . $external_resource->_joinData->external_resource_key
                                ) ?>
                </li>
                <?php endforeach; ?>
            </ul>
        <?= $this->Accordion->partClose() ?>
    </div>

    <div class="artifact-detail accordion">
        <?php if (!empty($artifact->credits)): ?>
            <?= $this->Accordion->partOpen('related_credits', 'Related Credits', 'h2') ?>
                <div class="related">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('Id') ?></th>
                                <th scope="col"><?= __('User Id') ?></th>
                                <th scope="col"><?= __('Artifact Id') ?></th>
                                <th scope="col"><?= __('Date') ?></th>
                                <th scope="col"><?= __('Comments') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->credits as $credits): ?>
                            <tr>
                                <td>
                                    <a href="/credits/<?= $credits->id ?>" >
                                        <?= h($credits->id) ?>
                                    </a>
                                </td>
                                <td><?= h($credits->author_id) ?></td>
                                <td><?= h($credits->artifact_id) ?></td>
                                <td><?= h($credits->date) ?></td>
                                <td><?= h($credits->comments) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
             <?= $this->Accordion->partClose() ?>

        <?php endif; ?>


        <?php if (!empty($artifact->collections)): ?>
            <?= $this->Accordion->partOpen('related_collections', 'Related Collections', 'h2') ?>
                <div class="related">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                            <th scope="col"><?= __('Id') ?></th>
                            <th scope="col"><?= __('Collection') ?></th>
                            <th scope="col"><?= __('Geo Coordinates') ?></th>
                            <th scope="col"><?= __('Slug') ?></th>
                            <th scope="col"><?= __('Is Private') ?></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($artifact->collections as $collections): ?>
                            <tr>
                                <td>
                                    <a href="/collections/<?= $collections->id ?>" >
                                        <?= h($collections->id) ?>
                                    </a>
                                </td>
                                <td><?= h($collections->collection) ?></td>
                                <td><?= h($collections->geo_coordinates) ?></td>
                                <td><?= h($collections->slug) ?></td>
                                <td><?= h($collections->is_private) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?= $this->Accordion->partClose() ?>
        <?php endif; ?>

        <?php if (!empty($artifact->genres)): ?>
            <?= $this->Accordion->partOpen('related_genres', 'Related Genres', 'h2') ?>
                <div class="related">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('Id') ?></th>
                                <th scope="col"><?= __('Genre') ?></th>
                                <th scope="col"><?= __('Parent Id') ?></th>
                                <th scope="col"><?= __('Genre Comments') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->genres as $genres): ?>
                            <tr>
                                <td>
                                    <a href="/genres/<?= $genres->id ?>" >
                                        <?= h($genres->id) ?>
                                    </a>
                                </td>
                                <td><?= h($genres->genre) ?></td>
                                <td><?= h($genres->parent_id) ?></td>
                                <td><?= h($genres->genre_comments) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?= $this->Accordion->partClose() ?>

        <?php endif; ?>

        <?php if (!empty($artifact->languages)): ?>
            <?= $this->Accordion->partOpen('related_languages', 'Related Languages', 'h2') ?>
                <div class="related">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('Id') ?></th>
                                <th scope="col"><?= __('Order') ?></th>
                                <th scope="col"><?= __('Parent Id') ?></th>
                                <th scope="col"><?= __('Language') ?></th>
                                <th scope="col"><?= __('Protocol Code') ?></th>
                                <th scope="col"><?= __('Inline Code') ?></th>
                                <th scope="col"><?= __('Notes') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->languages as $languages): ?>
                            <tr>
                                <td>
                                    <a href="/languages/<?= $languages->id ?>" >
                                        <?= h($languages->id) ?>
                                    </a>
                                </td>
                                <td><?= h($languages->order) ?></td>
                                <td><?= h($languages->parent_id) ?></td>
                                <td><?= h($languages->language) ?></td>
                                <td><?= h($languages->protocol_code) ?></td>
                                <td><?= h($languages->inline_code) ?></td>
                                <td><?= h($languages->notes) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?= $this->Accordion->partClose() ?>
        <?php endif; ?>

        <?php if (!empty($artifact->artifacts_composites)): ?>
            <?= $this->Accordion->partOpen('related_artifacts_composites', 'Related Artifacts Composites', 'h2') ?>
                <div class="related">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('Id') ?></th>
                                <th scope="col"><?= __('Composite') ?></th>
                                <th scope="col"><?= __('Artifact Id') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->artifacts_composites as $artifactsComposites): ?>
                            <tr>
                                <td>
                                    <a href="/artifacts-composites/<?= $artifactsComposites->id ?>" >
                                        <?= h($artifactsComposites->id) ?>
                                    </a>
                                </td>
                                <td><?= h($artifactsComposites->composite_no) ?></td>
                                <td><?= h($artifactsComposites->artifact_id) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?= $this->Accordion->partClose() ?>

        <?php endif; ?>

        <?php if (!empty($artifact->artifacts_date_referenced)): ?>
            <?= $this->Accordion->partOpen('related_artifacts_date_referenced', 'Related Artifacts Date Referenced', 'h2') ?>
                <div class="related">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('Id') ?></th>
                                <th scope="col"><?= __('Artifact Id') ?></th>
                                <th scope="col"><?= __('Ruler Id') ?></th>
                                <th scope="col"><?= __('Month Id') ?></th>
                                <th scope="col"><?= __('Month No') ?></th>
                                <th scope="col"><?= __('Year Id') ?></th>
                                <th scope="col"><?= __('Day No') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->artifacts_date_referenced as $artifactsDateReferenced): ?>
                            <tr>
                                <td>
                                    <a href="/artifacts-date-referenced/<?= $artifactsDateReferenced->id ?>" >
                                        <?= h($artifactsDateReferenced->id) ?>
                                    </a>
                                </td>
                                <td><?= h($artifactsDateReferenced->artifact_id) ?></td>
                                <td><?= h($artifactsDateReferenced->ruler_id) ?></td>
                                <td><?= h($artifactsDateReferenced->month_id) ?></td>
                                <td><?= h($artifactsDateReferenced->month_no) ?></td>
                                <td><?= h($artifactsDateReferenced->year_id) ?></td>
                                <td><?= h($artifactsDateReferenced->day_no) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?= $this->Accordion->partClose() ?>

        <?php endif; ?>

        <?php if (!empty($artifact->artifacts_seals)): ?>
            <?= $this->Accordion->partOpen('related_artifacts_seals', 'Related Artifacts Seals', 'h2') ?>
                <div class="related">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('Id') ?></th>
                                <th scope="col"><?= __('Seal No') ?></th>
                                <th scope="col"><?= __('Artifact Id') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->artifacts_seals as $artifactsSeals): ?>
                            <tr>
                                <td>
                                    <a href="/artifacts-seals/<?= $artifactsSeals->id ?>" >
                                        <?= h($artifactsSeals->id) ?>
                                    </a>
                                </td>
                                <td><?= h($artifactsSeals->seal_no) ?></td>
                                <td><?= h($artifactsSeals->artifact_id) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?= $this->Accordion->partClose() ?>

        <?php endif; ?>

        <?php if (!empty($artifact->artifacts_shadow)): ?>
            <?= $this->Accordion->partOpen('related_artifacts_shadow', 'Related Artifacts Shadow', 'h2') ?>
                <div class="related">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('Id') ?></th>
                                <th scope="col"><?= __('Artifact Id') ?></th>
                                <th scope="col"><?= __('Cdli Comments') ?></th>
                                <th scope="col"><?= __('Collection Location') ?></th>
                                <th scope="col"><?= __('Collection Comments') ?></th>
                                <th scope="col"><?= __('Acquisition History') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->artifacts_shadow as $artifactsShadow): ?>
                            <tr>
                                <td>
                                    <a href="/artifacts-shadow/<?= $artifactsShadow->id ?>" >
                                        <?= h($artifactsShadow->id) ?>
                                    </a>
                                </td>
                                <td><?= h($artifactsShadow->artifact_id) ?></td>
                                <td><?= h($artifactsShadow->cdli_comments) ?></td>
                                <td><?= h($artifactsShadow->collection_location) ?></td>
                                <td><?= h($artifactsShadow->collection_comments) ?></td>
                                <td><?= h($artifactsShadow->acquisition_history) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?= $this->Accordion->partClose() ?>


        <?php endif; ?>

        <?php if (!empty($artifact->retired_artifacts)): ?>
            <?= $this->Accordion->partOpen('related_retired_artifacts', 'Related Retired Artifacts', 'h2') ?>
                <div class="related">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col"><?= __('Id') ?></th>
                                <th scope="col"><?= __('Artifact Id') ?></th>
                                <th scope="col"><?= __('New Artifact Id') ?></th>
                                <th scope="col"><?= __('Artifact Remarks') ?></th>
                                <th scope="col"><?= __('Is Public') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($artifact->retired_artifacts as $retiredArtifacts): ?>
                            <tr>
                                <td>
                                    <a href="/retired-artifacts/<?= $retiredArtifacts->id ?>" >
                                        <?= h($retiredArtifacts->id) ?>
                                    </a>
                                </td>
                                <td><?= h($retiredArtifacts->artifact_id) ?></td>
                                <td><?= h($retiredArtifacts->new_artifact_id) ?></td>
                                <td><?= h($retiredArtifacts->artifact_remarks) ?></td>
                                <td><?= h($retiredArtifacts->is_public) ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?= $this->Accordion->partClose() ?>

        <?php endif; ?>

        <?= $this->Accordion->partOpen('comments', 'Comments', 'h2') ?>
            <div>
                <?= __('General Comments') ?>:
                <?= $this->Text->autoParagraph(h($artifact->general_comments)); ?>
            </div>
            <div>
                <?= __('CDLI Comments') ?>:
                <?= $this->Text->autoParagraph(h($artifact->cdli_comments)); ?>
            </div>

        <?= $this->Accordion->partClose() ?>
    </div>
</main>
<?= $this->Html->script('focus-visible.js') ?>
<?php echo $this->element('smoothscroll'); ?>
