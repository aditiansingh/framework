<?php 
    $sortBy = [
        'relevance' => 'Relevance'
    ];

    $publicationTypes = [
        "primary" => "Primary",
        "history" => "History",
        "citation" => "Citation",
        "collation" => "Collation",
        "other" => "Other"
    ];
?>
<div class="ads">
    <?= $this->Form->create(null, [
        'type'=>'POST',
    ]) ?>
        <div class="">
            <div class="d-flex align-items-center justify-content-between mb-3">
                <h1 class="display-3 text-left advanced-title">Advanced Search</h4>
                <?= $this->Form->button('Search', [
                        'type' => 'submit',
                        'class' => 'search-btn btn d-none d-lg-block d-xl-block'
                    ]); ?>
            </div>
            <div class="d-flex search-links">
                <?= $this->Html->link('Search settings', '/SearchSettings', ['class' => 'mr-5'])?>
                <?= $this->Html->link('Search Help', 'https://cdli-gh.github.io/guides/cdli_search.html')?>
            </div>
        </div>

        <div class="text-left my-5">
            <label>Sort By:</label>
            <?php
                echo $this->Form->select('sortBy',
                    $sortBy, [
                        'class' => 'ads-sort-select'
                    ])
            ?>
        </div>

        <div class="layout-grid text-left">
            <div  class="grid-1">
                <div class="darken">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <p class="form-title">Publication Data</p>

                        <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                    </div>
                    <?= $this->Form->control('pdesignation', [
                        'label' => 'Publication Designation',
                        'value' => isset($_GET['pdesignation']) ? $_GET['pdesignation'] : ''
                    ]); ?>
                    <?= $this->Form->control('bibtexkey', [
                        'label' => 'Bibtex Key',
                        'value' => isset($_GET['bibtexkey']) ? $_GET['bibtexkey'] : ''
                    ]); ?>
                    <?= $this->Form->control('authors', [
                        'label' => 'Author(s)',
                        'value' => isset($_GET['authors']) ? $_GET['authors'] : ''
                    ]); ?>
                    <?= $this->Form->control('editors', [
                        'label' => 'Editor(s)',
                        'value' => isset($_GET['editors']) ? $_GET['editors'] : ''
                    ]); ?>
                    <?= $this->Form->control('year', [
                        'label' => 'Publication Year',
                        'value' => isset($_GET['year']) ? $_GET['year'] : ''
                    ]); ?>
                    <?= $this->Form->control('title', [
                        'label' => 'Title',
                        'value' => isset($_GET['title']) ? $_GET['title'] : ''
                    ]); ?>
                    <?= $this->Form->control('ptype', [
                            'options' => $publicationTypes,
                            'label' => 'Publication Type',
                            'empty' => 'Select a publication type',
                            'value' => isset($_GET['ptype']) ? $_GET['ptype'] : ''
                        ]
                    );?>
                    <?= $this->Form->control('publisher', [
                        'label' => 'Publisher',
                        'value' => isset($_GET['publisher']) ? $_GET['publisher'] : ''
                    ]); ?>
                    <?= $this->Form->control('series', [
                        'label' => 'Series',
                        'value' => isset($_GET['series']) ? $_GET['series'] : ''
                    ]); ?>
                    <?= $this->Form->control('designation_exactref', [
                        'label' => 'Exact Reference',
                        'value' => isset($_GET['designation_exactref']) ? $_GET['designation_exactref'] : ''
                    ]); ?>
                </div>

                <div class="darken ads-space-top">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <p class="form-title">Artifact Data</p>

                        <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                    </div>
                    <?= $this->Form->control('atype', [
                        'label' => 'Artifact Type',
                        'value' => isset($_GET['atype']) ? $_GET['atype'] : ''
                    ]); ?>
                    <?= $this->Form->control('materials', [
                        'label' => 'Material',
                        'value' => isset($_GET['materials']) ? $_GET['materials'] : ''
                    ]); ?>
                    <?= $this->Form->control('collection', [
                        'label' => 'Collection',
                        'value' => isset($_GET['collection']) ? $_GET['collection'] : ''
                    ]); ?>
                    <?= $this->Form->control('provenience', [
                        'label' => 'Provenience',
                        'value' => isset($_GET['provenience']) ? $_GET['provenience'] : ''
                    ]); ?>
                    <?= $this->Form->control('written_in', [
                        'label' => 'Written in',
                        'value' => isset($_GET['written_in']) ? $_GET['written_in'] : ''
                    ]); ?>
                    <?= $this->Form->control('archive', [
                        'label' => 'Archive',
                        'value' => isset($_GET['archive']) ? $_GET['archive'] : ''
                    ]); ?>
                    <?= $this->Form->control('period', [
                        'label' => 'Period',
                        'value' => isset($_GET['period']) ? $_GET['period'] : ''
                    ]); ?>
                    <?= $this->Form->control('acomments', [
                        'label' => 'Artifact Comment',
                        'value' => isset($_GET['acomments']) ? $_GET['acomments'] : ''
                    ]); ?>
                </div>
            </div>
            

            <div class="grid-2">
                <div class="darken">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <p class="form-title">Inscriptions Data</p>

                        <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                    </div>
                    <?= $this->Form->control('translation', [
                        'label' => 'Translation',
                        'value' => isset($_GET['translation']) ? $_GET['translation'] : ''
                    ]); ?>
                    <?= $this->Form->control('transliteration_permutation', [
                        'label' => 'Transliteration Permutation',
                        'value' => isset($_GET['transliteration_permutation']) ? $_GET['transliteration_permutation'] : ''
                    ]); ?>
                    <?= $this->Form->control('transliteration', [
                        'label' => 'Transliteration',
                        'value' => isset($_GET['transliteration']) ? $_GET['transliteration'] : ''
                    ]); ?>
                    <?= $this->Form->control('icomments', [
                        'label' => 'Comment',
                        'value' => isset($_GET['icomments']) ? $_GET['icomments'] : ''
                    ]); ?>
                    <?= $this->Form->control('structure', [
                        'label' => 'Structure',
                        'value' => isset($_GET['structure']) ? $_GET['structure'] : ''
                    ]); ?>

                    <hr class="line"/>
                    <?= $this->Form->control('genres', [
                        'label' => 'Genre',
                        'value' => isset($_GET['genres']) ? $_GET['genres'] : ''
                    ]); ?>
                    <?= $this->Form->control('languages', [
                        'label' => 'Language',
                        'value' => isset($_GET['languages']) ? $_GET['languages'] : ''
                    ]); ?>
                </div>


                <div class="darken ads-space-top">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <p class="form-title">Artifact Identification</p>

                        <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                    </div>
                    <?= $this->Form->control('adesignation', [
                        'label' => 'Designation',
                        'value' => isset($_GET['adesignation']) ? $_GET['adesignation'] : ''
                    ]); ?>
                    <?= $this->Form->control('museum_no', [
                        'label' => 'Museum Number',
                        'value' => isset($_GET['museum_no']) ? $_GET['museum_no'] : ''
                    ]); ?>
                    <?= $this->Form->control('accession_no', [
                        'label' => 'Accession Number',
                        'value' => isset($_GET['accession_no']) ? $_GET['accession_no'] : ''
                    ]); ?>
                    <?= $this->Form->control('id', [
                        'label' => 'Artifact Number',
                        'value' => isset($_GET['id']) ? $_GET['id'] : ''
                    ]); ?>
                    <?= $this->Form->control('seal_no', [
                        'label' => 'Seal Number',
                        'value' => isset($_GET['seal_no']) ? $_GET['seal_no'] : ''
                    ]); ?>
                    <?= $this->Form->control('composite_no', [
                        'label' => 'Composite Number',
                        'value' => isset($_GET['composite_no']) ? $_GET['composite_no'] : ''
                    ]); ?>
                </div>

                <div class="darken ads-space-top ads-space-top">
                    <div class="d-flex align-items-baseline justify-content-between">
                        <p class="form-title">Credits</p>

                        <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                    </div>
                    <?= $this->Form->control('cauthor', [
                        'disabled',
                        'label' => 'Author/Contributor (Not Set up Yet)',
                        'value' => isset($_GET['cauthor']) ? $_GET['cauthor'] : ''
                    ]); ?>
                    <?= $this->Form->control('project', [
                        'disabled',
                        'label' => 'Project (Not Set up Yet)',
                        'value' => isset($_GET['project']) ? $_GET['project'] : ''
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

        <?= $this->Form->button('Search', [
            'type' => 'submit',
            'class' => 'wide-btn btn cdli-btn-blue mt-4'
        ]); ?>
    <?= $this->Form->end()?>

    <hr class="line" />

<?php echo $this->element('smoothscroll'); ?>

<hr class="my-5">

<div class="visualize">
    <div class="pills-container d-flex justify-content-center">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-bar-chart-tab" data-toggle="pill" href="#pills-bar-chart" role="tab" aria-controls="pills-bar-chart" aria-selected="true">Bar Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-donut-chart-tab" data-toggle="pill" href="#pills-donut-chart" role="tab" aria-controls="pills-donut-chart" aria-selected="false">Donut Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-radar-chart-tab" data-toggle="pill" href="#pills-radar-chart" role="tab" aria-controls="pills-radar-chart" aria-selected="false">Radar Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-line-chart-tab" data-toggle="pill" href="#pills-line-chart" role="tab" aria-controls="pills-line-chart" aria-selected="false">Line Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-dendrogram-chart-tab" data-toggle="pill" href="#pills-dendrogram-chart" role="tab" aria-controls="pills-dendrogram-chart" aria-selected="false">Dendrogram Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-choropleth-map-tab" data-toggle="pill" href="#pills-choropleth-map" role="tab" aria-controls="pills-choropleth-map" aria-selected="false">Choropleth Map</a>
            </li>
        </ul>
    </div>
    
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-bar-chart" role="tabpanel" aria-labelledby="pills-bar-chart-tab">
            <div id="bar-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/bar.png', ['class' => 'fallback-image', 'alt' => 'CDLI Bar Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
      
        <div class="tab-pane fade" id="pills-donut-chart" role="tabpanel" aria-labelledby="pills-donut-chart-tab">
            <div id="donut-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/donut.png', ['class' => 'fallback-image', 'alt' => 'CDLI Donut Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-radar-chart" role="tabpanel" aria-labelledby="pills-radar-chart-tab">
            <div id="radar-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/radar.png', ['class' => 'fallback-image', 'alt' => 'CDLI Radar Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-line-chart" role="tabpanel" aria-labelledby="pills-line-chart-tab">
            <div class="d-flex">
                <div class="ml-auto mb-3">
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="mr-3" for="line-chart-filter">Line Chart </label>
                            <select class="form-control" id="line-chart-filter">
                                <option value="period-vs-genre" selected>Period vs Genre</option>
                                <option value="period-vs-language">Period vs Language</option>
                                <option value="period-vs-material">Period vs Material</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div id="line-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/line.png', ['class' => 'fallback-image', 'alt' => 'CDLI Line Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-dendrogram-chart" role="tabpanel" aria-labelledby="pills-dendrogram-chart-tab">
            <div id="dendrogram-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/dendrogram.png', ['class' => 'fallback-image', 'alt' => 'CDLI Dendrogram Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-choropleth-map" role="tabpanel" aria-labelledby="pills-choropleth-map-tab">
            <div class="d-flex">
                <div class="map-controls-container">
                    <div class="btn-group" role="group" aria-label="Map Controls">
                        <button type="button" id="zoom-in" class="btn btn-secondary">
                            Zoom In <span class="fa fa-search-plus ml-2" aria-hidden="true"></span>
                        </button>
                        <button type="button" id="zoom-out" class="btn btn-secondary">
                            Zoom Out <span class="fa fa-search-minus ml-2" aria-hidden="true"></span>
                        </button>
                        <button type="button" id="reset" class="btn btn-secondary">
                            Reset
                        </button>
                    </div>
                </div>
                
                <div class="ml-auto mb-3">
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="mr-3" for="choropleth-map-filter">Filter Map By</label>
                            <select class="form-control" id="choropleth-map-filter">
                                <option value="proveniences" selected>Proveniences</option>
                                <option value="regions">Regions</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div id="choropleth-map">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/choropleth.png', ['class' => 'fallback-image', 'alt' => 'CDLI Choropleth Map']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
    </div>
</div>

<?php echo $this->Html->script(['d3', 'd3-compiled']); ?>

<script type="text/javascript">
    var barChartData = <?php echo $barChart; ?>;
    var donutChartData = <?php echo $donutChart; ?>;
    var radarChartData = <?php echo $radarChart; ?>;
    var lineChartData = <?php echo $lineChart; ?>;
    var dendrogramChartData = <?php echo $dendrogramChart; ?>;
    var choroplethMapData = <?php echo $choroplethMap; ?>;
    
    barChart(barChartData);
    donutChart(donutChartData);
    radarChart(radarChartData);
    lineChart(lineChartData);
    dendrogramChart(dendrogramChartData);
    choroplethMap(choroplethMapData[0], "proveniences");
    
    
    // Line Chart Select Filter
    $('#line-chart-filter').change(function() {        
        var lineChartType = $(this).val();
        var targetURL = "<?= \Cake\Routing\Router::url(array('controller' => 'AdvancedSearch', 'action' => 'getUpdatedLineChartData'), true) ?>";
        var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
        
        // Create AJAX request for retrieving updated data
        $.ajax({
            type: "POST",
            url: targetURL,
            headers: { 'X-CSRF-Token': csrfToken },
            data: {lineChartType: lineChartType},
            dataType: "JSON",
            success: function(response) {
                // Remove the current SVG
                $("#line-chart").empty();
                
                // Add the new line chart
                lineChart(response);
            }
        })
    })
    
    
    // Get choropleth map by filter
    // 0 : Proveniences
    // 1 : Regions
    $('#choropleth-map-filter').change(function() {        
        // Remove the current SVG
        $("#choropleth-map").empty();
        
        var choroplethMapType = $(this).val();
        if (choroplethMapType == "proveniences")
            choroplethMap(choroplethMapData[0], choroplethMapType);
        else
            choroplethMap(choroplethMapData[1], choroplethMapType);
    })
</script>
