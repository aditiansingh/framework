<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Proveniences Controller
 *
 * @property \App\Model\Table\ProveniencesTable $Proveniences
 *
 * @method \App\Model\Entity\Provenience[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProveniencesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('LinkedData');
        $this->loadComponent('GeneralFunctions');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Regions', 'Archives', 'Dynasties']
        ];
        $proveniences = $this->paginate($this->Proveniences);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('proveniences', 'access_granted'));
        $this->set('_serialize', 'proveniences');
    }

    /**
     * View method
     *
     * @param string|null $id Provenience id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $provenience = $this->Proveniences->get($id, [
            'contain' => ['Regions', 'Archives', 'Dynasties']
        ]);
        $access_granted = $this->GeneralFunctions->checkIfRolesExists([1]);

        $this->set(compact('access_granted'));
        $this->set('provenience', $provenience);
        $this->set('_serialize', 'provenience');
    }
}
