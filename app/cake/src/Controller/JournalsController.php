<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Journals Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\Journal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class JournalsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $journals = $this->paginate($this->Journals);

        $this->set(compact('journals'));
    }

    /**
     * View method
     *
     * @param string|null $id Journal id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $journal = $this->Journals->get($id, [
            'contain' => ['Publications']
        ]);

        $abbreviations = TableRegistry::get('Abbreviations');
        $name = $abbreviations->find('all', ['conditions' => [strpos('abbreviation', $journal->journal)]]);
        $result = $name->toArray();
        $fullform = $result[0]->fullform;

        $this->set(compact('journal', 'fullform'));
    }
}
