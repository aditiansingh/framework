<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsExternalResources Controller
 *
 * @property \App\Model\Table\ArtifactsExternalResourcesTable $ArtifactsExternalResources
 *
 * @method \App\Model\Entity\ArtifactsExternalResource[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsExternalResourcesController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('LinkedData');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'ExternalResources']
        ];
        $artifactsExternalResources = $this->paginate($this->ArtifactsExternalResources);

        $this->set(compact('artifactsExternalResources'));
        $this->set('_serialize', 'artifactsExternalResources');
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts External Resource id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsExternalResource = $this->ArtifactsExternalResources->get($id, [
            'contain' => ['Artifacts', 'ExternalResources']
        ]);

        $this->set('artifactsExternalResource', $artifactsExternalResource);
        $this->set('_serialize', 'artifactsExternalResource');
    }
}
