<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;

/**
 * ArtifactsPublications Controller
 *
 * @property \App\Model\Table\ArtifactsPublicationsTable $ArtifactsPublications
 *
 * @method \App\Model\Entity\ArtifactsPublication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsPublicationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(): void
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method.
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'order' => [
                'artifact_id' => 'ASC',
                'publication_id' => 'ASC'
            ],
            'contain' => [
                'Artifacts', 'Publications'
            ]
        ];
        $artifactsPublications = $this->paginate($this->ArtifactsPublications);

        $this->set(compact('artifactsPublications'));
    }

    /**
     * Add method.
     *
     * @param string $flag type of add operation.
     * '' => Normal add,
     * 'artifact' => Add link to a selected publication,
     * 'publication' => Add link to a selected artifact,
     * 'bulk' => Bulk add links.
     * @param int $id Id of the selected publication or selected artifact for $flag = 'artifact' and $flag = 'publication' respectively.
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($flag = '', $id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        if ($flag == '') {
            $artifactsPublication = $this->ArtifactsPublications->newEntity();
            if ($this->getRequest()->is('post')) {
                $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->getRequest()->getData());
                if ($this->ArtifactsPublications->save($artifactsPublication)) {
                    $this->Flash->success('New link has been saved.');
                    return $this->redirect(['action' => 'add']);
                }
                $this->Flash->error('New link could not be saved. Please, try again.');
            }
            $this->set(compact('artifactsPublication', 'flag'));
        } elseif ($flag == 'artifact') {
            // Check if publication id is provided
            if (!isset($id)) {
                $this->Flash->error('No Publication selected');
                return $this->redirect(['action' => 'add']);
            }

            $this->paginate = [
                'limit' => 10
            ];
            $artifactsPublication = $this->ArtifactsPublications->newEntity();
            if ($this->getRequest()->is('post')) {
                $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->getRequest()->getData());
                if ($this->ArtifactsPublications->save($artifactsPublication)) {
                    $this->Flash->success(__('The artifact has been linked.'));
                    return $this->redirect(['action' => 'add', $flag, $id]);
                }
                $this->Flash->error(__('The artifact could not be linked. Please, try again.'));
            }
            $artifactsPublications = $this->paginate($this->ArtifactsPublications->find('all', ['order' => 'artifact_id', 'contain' => ['Artifacts', 'Publications']])->where(['publication_id' => $id]));
            $this->set(compact('id', 'artifactsPublications', 'artifactsPublication', 'flag'));
        } elseif ($flag == 'publication') {
            // Check if artifact id is provided
            if (!isset($id)) {
                $this->Flash->error('No Artifact selected');
                return $this->redirect(['action' => 'add']);
            }

            $this->paginate = [
                'limit' => 10
            ];
            $artifactsPublication = $this->ArtifactsPublications->newEntity();
            if ($this->getRequest()->is('post')) {
                $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->getRequest()->getData());
                if ($this->ArtifactsPublications->save($artifactsPublication)) {
                    $this->Flash->success(__('The publication has been linked.'));
                    return $this->redirect(['action' => 'add', $flag, $id]);
                }
                $this->Flash->error(__('The publication could not be linked. Please, try again.'));
            }
            $artifactsPublications = $this->paginate($this->ArtifactsPublications->find('all', [
                'order' => 'publication_id',
                'contain' => [
                    'Artifacts',
                    'Publications' => [
                        'EntryTypes',
                        'Journals',
                        'Editors' => [
                            'sort' => ['EditorsPublications.sequence' => 'ASC']
                            ],
                        'Authors' => [
                            'sort' => ['AuthorsPublications.sequence' => 'ASC']
                            ]
                        ]
                    ]
                ])->where(['artifact_id' => $id]));
            $this->set(compact('id', 'artifactsPublications', 'artifactsPublication', 'flag'));
        } elseif ($flag == 'bulk') {
            $this->loadComponent('BulkUpload', ['table' => 'ArtifactsPublications']);
            $this->BulkUpload->upload();
            
            $this->set(compact('flag'));
        }
    }

    /**
     * Edit method.
     *
     * @param int $id Artifact Publication Link id.
     * @param int $flag page to redirect to.
     * '' => Normal edit,
     * 'artifact' => Edit page for a selected publication,
     * 'publication' => Edit page for a selected artifact.
     * @param int $parent_id Id of the selected publication or selected artifact for $flag = 'artifact' and $flag = 'publication' respectively.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $flag = '', $parent_id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        // Check if id is provided
        if (!isset($id)) {
            $this->Flash->error('No Artifact-Publication link selected');
            return $this->redirect(['action' => 'add']);
        }

        $artifactsPublication = $this->ArtifactsPublications->get($id, [
            'contain' => ['Artifacts', 'Publications']
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->getRequest()->getData());
            if ($this->ArtifactsPublications->save($artifactsPublication)) {
                $this->Flash->success(__('Changes has been saved.'));

                if (!isset($parent_id)) {
                    return $this->redirect(['controller' => 'ArtifactsPublications', 'action' => 'index']);
                } else {
                    return $this->redirect(['action' => 'add', $flag, $parent_id]);
                }
            }
            $this->Flash->error(__('Changes could not be saved. Please, try again.'));
        }
        $this->set(compact('artifactsPublication', 'flag', 'parent_id'));
    }

    /**
     * Delete method.
     *
     * @param int $id Artifact Publication Link id.
     * @param int $flag page to redirect to.
     * '' => Index page,
     * 'artifact' => Add page for a selected publication,
     * 'publication' => Add page for a selected artifact.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $flag = '', $parent_id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->getRequest()->allowMethod(['post', 'delete']);
        $artifactsPublication = $this->ArtifactsPublications->get($id);
        if ($this->ArtifactsPublications->delete($artifactsPublication)) {
            $this->Flash->success(__('The link has been deleted.'));
        } else {
            $this->Flash->error(__('The link could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add', $flag, $parent_id]);
    }

    /**
     * Export method for downloading the entries containing errors.
     */
    public function export()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->loadComponent('BulkUpload', ['table' => 'ArtifactsPublications']);
        $this->BulkUpload->export();
    }
}
