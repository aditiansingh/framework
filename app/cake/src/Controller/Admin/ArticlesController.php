<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Http\Exception\ForbiddenException;
use Cake\Datasource\ConnectionManager;
use PhpLatex_Parser;
use PhpLatex_Renderer_Html;
use RenanBr\BibTexParser\Listener;
use RenanBr\BibTexParser\Parser;
use RenanBr\BibTexParser\Processor;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\Journal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadModel('Articles');
        $this->loadModel('Publications');
        $this->loadModel('ArticlesAuthors');
        $this->loadModel('Authors');
        $this->loadModel('ArticlesPublications');
    }

    /**
    * authorToAuthorID helper.
    */
    public function authorToAuthorID($authorNames)
    {
        $authorIds = array();
        $authorNames = explode(',', $authorNames);
        foreach ($authorNames as $authorName) {
            $author =  $this->Authors->find()->where(['author' => $authorName])->first();

            if (!$author) {
                $author = $this->Authors->newEmptyEntity();
                $author->author = $authorName;
                $this->Authors->save($author);
            }
            array_push($authorIds, $author->id);
        }

        return $authorIds;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
    }

    /**
     * Add CDLN Artcile.
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addCdln()
    {
        if ($this->getRequest()->is('post')) {
            $this->autoRender = false;
            $requestData = $this->getRequest()->getData();
            $article = $this->Articles->newEmptyEntity();
            $article->year = $requestData['year'];
            $article->article_no = $requestData['article_no'];
            $article->title = $requestData['cdln_title'];
            $article->content_html = $requestData['cdln_content'];
            $article->content_latex = '';
            $article->is_pdf_uploaded = 1;
            $article->article_type = 'cdln';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdln_status'];
            $article->pdf_link = '';
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $authorIds = $this->authorToAuthorID($requestData['cdln_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEmptyEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;

                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/articles/cdln');
        }
    }

    /**
     * Add CDLP Artcile.
     */
    public function addCdlp()
    {
        if ($this->getRequest()->is('post')) {
            $this->autoRender = false;
            $requestData = $this->getRequest()->getData();

            $article = $this->Articles->newEmptyEntity();
            $article->year = $requestData['year'];
            $article->article_no = $requestData['article_no'];
            $article->title = $requestData['cdlp_title'];
            $article->content_html = '';
            $article->content_latex = '';
            $article->is_pdf_uploaded = 1;
            $article->article_type = 'cdlp';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdlp_status'];
            $article->pdf_link = $requestData['cdlp_pdf_link'];
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $authorIds = $this->authorToAuthorID($requestData['cdlp_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEmptyEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;

                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/articles/cdlp');
        }
    }

    /**
     * Add CDLJ Artcile.
     */
    public function addCdlj()
    {
        if ($this->getRequest()->is('post')) {
            $this->autoRender = false;
            $requestData = $this->getRequest()->getData();

            $article = $this->Articles->newEmptyEntity();

            $article->year = $requestData['year'];
            $article->article_no = $requestData['article_no'];
            $article->title = $requestData['cdlj_title'];
            $article->content_html = $requestData['artilce_html_content'];
            $article->content_latex = $requestData['article_latex_content'];
            $article->is_pdf_uploaded = 1;
            $article->article_type = 'cdlj';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdlj_status'];
            $article->pdf_link = $requestData['cdlj_pdf_link'];
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $authorIds = $this->authorToAuthorID($requestData['cdlj_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEmptyEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;

                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/articles/cdlj');
        }
    }

    /**
     * Add CDLB Artcile.
     */
    public function addCdlb()
    {
        if ($this->getRequest()->is('post')) {
            $this->autoRender = false;
            $requestData = $this->getRequest()->getData();

            $article = $this->Articles->newEmptyEntity();
            $article->year = $requestData['year'];
            $article->article_no = $requestData['article_no'];
            $article->title = $requestData['cdlb_title'];
            $article->content_html = $requestData['artilce_html_content'];
            $article->content_latex = $requestData['article_latex_content'];
            $article->is_pdf_uploaded = 1;
            $article->article_type = 'cdlb';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdlb_status'];
            $article->pdf_link = $requestData['cdlb_pdf_link'];
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $authorIds = $this->authorToAuthorID($requestData['cdlb_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEmptyEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;

                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/articles/cdlb');
        }
    }


    /**
     * Delete Artcile.
     *
     */
    public function deleteArticle()
    {
        $this->autoRender = false;
        if (1) {
            $article_id = $this->getRequest()->getParam('article');

            $this->ArticlesAuthors->deleteAll(['article_id' => $article_id]);
            $this->Articles->deleteAll(['id' => $article_id]);
            echo 'deleted';
        } else {
            throw new ForbiddenException(__('Request not allowed.'));
        }
    }

    /**
     * Edit CDLN Artcile.
     *
     */
    public function editCdln()
    {
        $connection = ConnectionManager::get('default');
        $article_id = $this->getRequest()->getParam('article');
        $article =  $this->Articles->find()->where(['id' => $article_id, 'article_type' => 'cdln'])->first();

        $authors = $connection->execute("select authors.author from articles_authors join authors where
         articles_authors.article_id = ".$article_id." and articles_authors.author_id = authors.id")->fetchAll('assoc');

        $authorNames = array();
        foreach ($authors as $author) {
            array_push($authorNames, $author['author']);
        }

        if ($article) {
            $this->set(['article' => $article, 'authorNames'=> implode(',', $authorNames)]);
        } else {
            throw new ForbiddenException(__('No such CDLN to edit.'));
        }

        if ($this->getRequest()->is('post')) {
            $requestData = $this->getRequest()->getData();

            $article->title = $requestData['cdln_title'];
            $article->year = $requestData['year'];
            $article->article_no = $requestData['article_no'];
            $article->content_html = $requestData['cdln_content'];
            $article->content_latex = '';
            $article->is_pdf_uploaded = 1;
            $article->article_type = 'cdln';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdln_status'];
            $article->pdf_link = '';
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $this->ArticlesAuthors->deleteAll(['article_id' => $article->id]);

            $authorIds = $this->authorToAuthorID($requestData['cdln_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEmptyEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;

                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/articles/cdln');
        }
    }

    /**
     * Edit CDLJ Artcile.
     *
     */
    public function editCdlb()
    {
        $connection = ConnectionManager::get('default');
        $article_id = $this->getRequest()->getParam('article');
        $article =  $this->Articles->find()->where(['id' => $article_id, 'article_type' => 'cdlb'])->first();

        $authors = $connection->execute("select authors.author from articles_authors join authors where
         articles_authors.article_id = ".$article_id." and articles_authors.author_id = authors.id")->fetchAll('assoc');

        $authorNames = array();
        foreach ($authors as $author) {
            array_push($authorNames, $author['author']);
        }

        if ($article) {
            $this->set(['article' => $article, 'authorNames'=> implode(',', $authorNames)]);
        } else {
            throw new ForbiddenException(__('No such CDLN to edit.'));
        }

        if ($this->getRequest()->is('post')) {
            $requestData = $this->getRequest()->getData();

            $article->title = $requestData['cdlb_title'];
            $article->year = $requestData['year'];
            $article->article_no = $requestData['article_no'];
            $article->content_html = $requestData['artilce_html_content'];
            $article->content_latex = $requestData['article_latex_content'];
            $article->is_pdf_uploaded = 1;
            $article->article_type = 'cdlb';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdlb_status'];
            $article->pdf_link = $requestData['cdlb_pdf_link'];
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $this->ArticlesAuthors->deleteAll(['article_id' => $article->id]);

            $authorIds = $this->authorToAuthorID($requestData['cdlb_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEmptyEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;

                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/articles/cdlb');
        }
    }



    /**
     * Edit CDLJ Artcile.
     *
     */
    public function editCdlj()
    {
        $connection = ConnectionManager::get('default');
        $article_id = $this->getRequest()->getParam('article');
        $article =  $this->Articles->find()->where(['id' => $article_id, 'article_type' => 'cdlj'])->first();

        $authors = $connection->execute("select authors.author from articles_authors join authors where
         articles_authors.article_id = ".$article_id." and articles_authors.author_id = authors.id")->fetchAll('assoc');

        $authorNames = array();
        foreach ($authors as $author) {
            array_push($authorNames, $author['author']);
        }

        if ($article) {
            $this->set(['article' => $article, 'authorNames'=> implode(',', $authorNames)]);
        } else {
            throw new ForbiddenException(__('No such CDLN to edit.'));
        }

        if ($this->getRequest()->is('post')) {
            $requestData = $this->getRequest()->getData();

            $article->title = $requestData['cdlj_title'];
            $article->year = $requestData['year'];
            $article->article_no = $requestData['article_no'];
            $article->content_html = $requestData['artilce_html_content'];
            $article->content_latex = $requestData['article_latex_content'];
            $article->is_pdf_uploaded = 1;
            $article->article_type = 'cdlj';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdlj_status'];
            $article->pdf_link = $requestData['cdlj_pdf_link'];
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $this->ArticlesAuthors->deleteAll(['article_id' => $article->id]);

            $authorIds = $this->authorToAuthorID($requestData['cdlj_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEmptyEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;

                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/articles/cdlj');
        }
    }


    /**
     * Edit CDLP Artcile.
     *
     */
    public function editCdlp()
    {
        $connection = ConnectionManager::get('default');
        $article_id = $this->getRequest()->getParam('article');
        $article =  $this->Articles->find()->where(['id' => $article_id, 'article_type' => 'cdlp'])->first();

        $authors = $connection->execute("select authors.author from articles_authors join authors where
         articles_authors.article_id = ".$article_id." and articles_authors.author_id = authors.id")->fetchAll('assoc');

        $authorNames = array();
        foreach ($authors as $author) {
            array_push($authorNames, $author['author']);
        }

        if ($article) {
            $this->set(['article' => $article, 'authorNames'=> implode(',', $authorNames)]);
        } else {
            throw new ForbiddenException(__('No such CDLP to edit.'));
        }

        if ($this->getRequest()->is('post')) {
            $requestData = $this->getRequest()->getData();

            $article->title = $requestData['cdlp_title'];
            $article->year = $requestData['year'];
            $article->article_no = $requestData['article_no'];
            $article->content_html = '';
            $article->content_latex = '';
            $article->is_pdf_uploaded = 1;
            $article->article_type = 'cdlp';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdlp_status'];
            $article->pdf_link = $requestData['cdlp_pdf_link'];
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $this->ArticlesAuthors->deleteAll(['article_id' => $article->id]);

            $authorIds = $this->authorToAuthorID($requestData['cdlp_authors']);
            $sequence = 1;

            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEmptyEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;

                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/articles/cdlp');
        }
    }

    /**
     * Upload Article Helper.
     */
    public function uploadArticlePdf()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $article = strtolower($article);
        if ($this->getRequest()->is('post')) {
            $file = $this->getRequest()->getData('file');

            if ($file->getClientMediaType()=='application/pdf') {
                $ArticleFilePath = 'webroot/pubs/'.$article.'/'.$file->getClientFilename();

                try {
                    $file->moveTo($ArticleFilePath);
                    echo $file->getClientFilename();
                } catch (\Exception $e) {
                    echo 'File is not uploaded.';
                }
            } else {
                throw new ForbiddenException(__('Only pdf files are allowed.'));
            }
        }
    }

    /**
     * Upload Article latex Helper.
    */
    public function uploadArticleLatex()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $article = strtolower($article);
        $folder="TMP_".sha1(time());
        if ($this->getRequest()->is('post')) {
            $file = $this->getRequest()->getData('file');

            if (1) {
                $ArticleDirPath = 'webroot/pubs/'.$article.'/'.'tmp'.'/'.$folder;
                if (!is_dir($ArticleDirPath)) {
                    mkdir($ArticleDirPath);
                } else {
                    $folder="TMP_".sha1(time());
                    $ArticleDirPath = 'webroot/pubs/'.$article.'/'.'tmp'.'/'.$folder;
                    mkdir($ArticleDirPath);
                }
                $ArticleFilePath = $ArticleDirPath.'/'.$file->getClientFilename();

                try {
                    $file->moveTo($ArticleFilePath);
                    echo $ArticleFilePath;
                } catch (\Exception $e) {
                    echo 'File is not uploaded.';
                }
            } else {
                throw new ForbiddenException(__('Only pdf files are allowed.'));
            }
        }
    }

    /**
     * viewCdln method
     */
    public function viewCdln()
    {
        $this->autoRender = false;

        $article = $this->getRequest()->getParam('cdln');
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdln'])->first();

        if ($article) {
            $this->getResponse()->file(
                'webroot/pubs/cdln/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }

    /**
     * viewCdln method
     */
    public function viewCdlnWeb()
    {
        $this->autoRender = false;

        $article = $this->getRequest()->getParam('cdln');

        $connection = ConnectionManager::get('default');

        $article = $connection->execute("select * , articles.id as article_id, GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.id=".$article." and articles.article_type='cdln'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id order by articles.serial; ")->fetchAll('assoc');


        if ($article) {
            $this->layout= '';
            $this->set(['cdln' => $article ]);
            $this->render('cdln_web_template');
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }

    public function imageManagerUpload()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $article = strtolower($article);
        if ($this->getRequest()->is('post')) {
            $file = $this->getRequest()->getData('file');

            if (1) {
                $ArticleFilePath = 'webroot/pubs/'.$article.'/images/'.$file->getClientFilename();

                try {
                    $file->moveTo($ArticleFilePath);
                    echo $file->getClientFilename();
                } catch (\Exception $e) {
                    echo 'File is not uploaded.';
                }
            } else {
                throw new ForbiddenException(__('Only images files are allowed.'));
            }
        }
    }

    /**
     * viewCdlb method
     */
    public function viewCdlbWeb()
    {
        $this->autoRender = false;

        $article = $this->getRequest()->getParam('cdlb');

        $connection = ConnectionManager::get('default');

        $article = $connection->execute("select * , articles.id as article_id, GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.id=".$article." and articles.article_type='cdlb'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id order by articles.serial; ")->fetchAll('assoc');


        if ($article) {
            $this->layout= '';
            $this->set(['cdln' => $article ]);
            $this->render('cdln_web_template');
        } else {
            throw new ForbiddenException(__('No such CDLB article.'));
        }
    }
    /**
     * viewCdln method
     */
    public function viewCdljWeb()
    {
        $this->autoRender = false;

        $article = $this->getRequest()->getParam('cdlj');

        $connection = ConnectionManager::get('default');

        $article = $connection->execute("select * , articles.id as article_id, GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.id=".$article." and articles.article_type='cdlj'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id order by articles.serial; ")->fetchAll('assoc');


        if ($article) {
            // $this->layout= '';
            $this->set(['cdln' => $article ]);
            $this->render('cdln_web_template');
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }

    public function viewArticleImage()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $name = $this->getRequest()->getParam('name');
        $this->getResponse()->file(
            'webroot/pubs/'.$article.'/images/'.$name
        );
    }

    /**
     * viewCdlp method
     */
    public function viewCdlp()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('cdlp');
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlp'])->first();
        if ($article) {
            $this->getResponse()->file(
                'webroot/pubs/cdlp/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLP article.'));
        }
    }

    /**
     * viewCdlp method
     */
    public function viewCdlj()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('cdlj');
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlj'])->first();
        if ($article) {
            $this->getResponse()->file(
                'webroot/pubs/cdlj/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLP article.'));
        }
    }

    /**
     * viewCdlp method
     */
    public function viewCdlb()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('cdlb');
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlb'])->first();
        if ($article) {
            $this->getResponse()->file(
                'webroot/pubs/cdlb/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLB article.'));
        }
    }

    /**
     * This function provides
     * Latex content
     * via Ajax.
     */
    public function convertLatexContent()
    {
        $requestData = $this->getRequest()->getData();
        $article_type = $this->getRequest()->getParam('article');
        $this->autoRender = false;

        if ($requestData['type']=='db') {
            $article =  $this->Articles->find()->where(['id' => $requestData['latex'], 'article_type' => $article_type])->first();
            $data =  $article->content_latex;
        } else {
            $data =  nl2br(file_get_contents($requestData['latex']));
            $data = str_replace("<br />", "", $data);
        }

        return $this->getResponse()->withStringBody($data);
    }

    /**
     * This function writes
     * Latex content
     * via Ajax.
     */
    public function writeLatexContent()
    {
        $this->autoRender = false;

        $article_type = $this->getRequest()->getParam('article');

        $requestData = $this->getRequest()->getData();

        if ($requestData['type']=='db') {
            $article =  $this->Articles->find()->where(['id' => $requestData['latex'], 'article_type' => $article_type])->first();
            $article->content_latex = $requestData['latex_content'];
            $this->Articles->save($article);
        } else {
            $LatexContent = $requestData['latex_content'];
            $LatexContent = str_replace("<br />", "", $LatexContent);
            $file = fopen($requestData['latex'], "w");
            fwrite($file, $LatexContent);
        }

        echo "success";
    }

    /**
     * This function converts
     * Latex to HtML provides data
     * via Ajax.
     */
    public function convertLatexToHtml()
    {
        $requestData = $this->getRequest()->getData();
        $article_type = $this->getRequest()->getParam('article');
        $this->autoRender = false;
        $template = '
            <table id="CKAddedHeaderTable" style="margin:0;" width="750" border="0" cellpadding="0" cellspacing="5">
                    <tr>
                        <td valign="top" width="400">
                        <p style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif; font-size:9pt">
                            Cuneiform Digital Library Journal <br>
                            <b> 2018:001 </b>
                            <br>
                            <font size="1"> ISSN 1540-8779 <br>
                            &#169; <i> Cuneiform Digital Library Initiative </i> </font> &nbsp;
                        </p>
                        </td>
                        <td width="200" height="200" rowspan="2" align="right" nowrap bgcolor="#1461ab">
                        <p style="line-height:15.0pt; font-size:9pt;color:white;">
                            <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
                            <a style="color:white;" href="/"> CDLI Home </a> <br>
                            <a style="color:white;" href=""> CDLI Publications </a> <br>
                            <a style="color:white;" href="" target="link"> Editorial Notes </a> <br>
                            <a style="color:white;" href="" class="tes" target="link"> Abbreviations </a> <br>
                            <a style="color:white;" href="" target="link"> Bibliography </a> <br>
                            <br>
                            <a style="color:white;" href="" target="blank">PDF Version of this Article </a>
                            <br>
                            <a style="color:white;" href="" target="link"> Get Acrobat Reader </a> <br>
                            <a style="color:white;" href="" target="link"> <font color="#800517"><b>Download Cuneiform Font</b></font> </a> </font>
                        </p>
                        </td>
                        <td width="1" rowspan="2" align="right" nowrap bgcolor="#99CCCC"> &nbsp; </td>
                    </tr>
                    <tr>
                        <td>
                        <p>
                            <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
                            <h2 style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
                            <br>
                            <p id="pArticleName">Article Title</p>
                            </h2>
                            <b> <p id="pArticleAuthors"> Name </p</b>
                            <br>
                            <i>University</i>
                            <br>
                            <br>
                            <b> Keywords </b> <br>
                            </font>
                        </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                        <hr align="center" width="600" size="2">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left">
                        <br>
                        <p>
                        <i><b>Abstract</b><br><br></i>
                        </p>
                        <div id="pArticleContent"></div>

                        </td>
                    </tr>
                </table>';

        $parser = new PhpLatex_Parser();
        $htmlRenderer = new PhpLatex_Renderer_Html();

        $parser->addCommands([
            '\\printbibliography' => [
                'numOptArgs' => 1
            ],
            '\\nocite' => [
                'numArgs' => 1,
                'parseArgs' => false
            ],
            '\\parencite' => [
                'numArgs' => 1,
                'numOptArgs' => 1,
                'parseArgs' => false
            ],
            '\\cite' => [
                'numArgs' => 1,
                'numOptArgs' => 1,
                'parseArgs' => false
            ]
        ]);

        $bibData = $requestData['bib_data'];
        $htmlRenderer->addCommandRenderer('\\printbibliography', function () use ($bibData) {
            if ($bibData) {
                $entries = array_map(function ($entry) {
                    return $entry[1];
                }, $bibData['bibliography']);
                return '<div class="csl-bibliography">' . implode("\n", $entries) . '</div>';
            } else {
                return '';
            }
        });

        $htmlRenderer->addCommandRenderer('\\nocite', function () {
            return '';
        });
        $htmlRenderer->addCommandRenderer('\\cite', function () use (&$bibData) {
            if ($bibData) {
                return array_shift($bibData['citation']);
            } else {
                return '';
            }
        });
        $htmlRenderer->addCommandRenderer('\\parencite', function () use (&$bibData) {
            if ($bibData) {
                return array_shift($bibData['citation']);
            } else {
                return '';
            }
        });

        if ($requestData['type']=='db') {
            $article =  $this->Articles->find()->where(['id' => $requestData['latex'], 'article_type' => $article_type])->first();
            $data =  $article->content_latex;
        } else {
            $data =  nl2br(file_get_contents($requestData['latex']));
        }

        preg_match_all('/.begin{document}(.*).end{document}/s', $data, $output_array);

        $data = $output_array[1][0];

        $data = preg_replace('/%(.*)/', '', $data);
        $data = preg_replace('/[^a-zA-Z0-9]bf [^a-zA-Z0-9]S/', '', $data);
        $data = preg_replace('/.thispagestyle{(.*?)}/', '', $data);
        $data = preg_replace('/.newpage/', '', $data);
        $data = preg_replace('/.begin{flushleft}(.*).end{flushleft}/s', '', $data);
        $data = preg_replace('/.begingroup(.*).endgroup/s', '', $data);
        // $data = preg_replace('/.printbibliography(.*)/', '', $data);
        $data = str_replace('\\&', "and", $data);
        preg_match_all('/[^b]section.{}[<br \/>]*\n*\t*{[^}]*}/ms', $data, $all_sections);
        preg_match_all('/.subsection.{}[<br \/>]*\n*\t*{[^}]*}/ms', $data, $all_sub_sections);

        preg_match_all('/.superscript{[^}]*}[}]*/ms', $data, $all_superscript);
        preg_match_all('/.subscript{[^}]*}[}]*/ms', $data, $all_subscript);


        preg_match_all('/\\\\(parencite|cite|nocite)(\[.*?\])?{.*?}/ms', $data, $amf);
        $identified_cits = $amf[0];

        // For sections.
        foreach ($all_sections[0] as $section) {
            $HTMLSection = str_replace("\n", "", $section);
            $HTMLSection = str_replace("\t", "", $HTMLSection);
            $HTMLSection = str_replace("\section", "", $HTMLSection);
            $HTMLSection = str_replace("*", "", $HTMLSection);
            $HTMLSection = str_replace("{", "", $HTMLSection);
            $HTMLSection = str_replace("}", "", $HTMLSection);
            $HTMLSection = str_replace("", "", $HTMLSection);

            $data = str_replace($section, "<b>".$HTMLSection."</b>", $data);
        }

        // For sub sections.
        foreach ($all_sub_sections[0] as $sbsection) {
            $HTMLsbSection = str_replace("\n", "", $sbsection);
            $HTMLsbSection = str_replace("\t", "", $HTMLsbSection);
            $HTMLsbSection = str_replace("\subsection", "", $HTMLsbSection);
            $HTMLsbSection = str_replace("*", "", $HTMLsbSection);
            $HTMLsbSection = str_replace("{", "", $HTMLsbSection);
            $HTMLsbSection = str_replace("}", "", $HTMLsbSection);
            $HTMLsbSection = str_replace("", "", $HTMLsbSection);

            $data = str_replace($sbsection, "<b>".$HTMLsbSection."</b>", $data);
        }

        // For longtables.
        preg_match_all('/.begin{longtable}(.*?).end{longtable}/ms', $data, $all_tables);
        foreach ($all_tables[0] as $table) {
            $table = str_replace('\\&', "and", $table);
            $AllTables = explode('\\\\', $table);

            $PopCount = array();

            foreach ($AllTables as $AllTablesI) {
                preg_match_all('/&/', $AllTablesI, $output_array);
                $mai = count($output_array[0]);
                array_push($PopCount, $mai);
            }
            $values = array_count_values($PopCount);
            arsort($values);
            $popular = array_slice(array_keys($values), 0, 5, true);

            $HTMLTable ='';
            foreach ($AllTables as $AllTablesI) {
                $HTMLTable =   $HTMLTable .'<tr>';
                $AllTablesPop = explode('&', $AllTablesI);
                if (($popular[0]+1)!=count($AllTablesPop)) {
                    // Print log details here.
                }
                for ($i=0;$i<count($AllTablesPop);$i++) {
                    if ($AllTablesPop[$i]!=='' && $AllTablesPop[$i]!==' ') {
                        $HTMLTable = $HTMLTable.'<td>'.$AllTablesPop[$i].'</td>';
                    }
                }
                $HTMLTable =   $HTMLTable .'</tr>';
            }

            $data = str_replace($table, "<table>".$HTMLTable."</table>", $data);
        }


        foreach ($all_superscript[0] as $superscript) {
            $PopCount = 0;
            $HTMLSuperScript="";
            $HTMLSuperScriptInc = str_replace("\superscript{", "", $superscript);
            for ($i=0;$i<(strlen($HTMLSuperScriptInc)-1);$i++) {
                if ($HTMLSuperScriptInc[$i]=='{') {
                    $PopCount++;
                }
                if ($HTMLSuperScriptInc[$i]=='}') {
                    $PopCount--;
                }
                if ($PopCount>=0) {
                    $HTMLSuperScript = $HTMLSuperScript.$HTMLSuperScriptInc[$i];
                }
            }
            $HTMLSuperScriptInc_ex = "\superscript{".$HTMLSuperScript."}";
            $data = str_replace($HTMLSuperScriptInc_ex, "<sup>".$HTMLSuperScript."</sup>", $data);
        }

        foreach ($all_subscript[0] as $subscript) {
            $PopCount = 0;
            $HTMLSubScript="";
            $HTMLSubScriptInc = str_replace("\subscript{", "", $subscript);
            for ($i=0;$i<(strlen($HTMLSubScriptInc)-1);$i++) {
                if ($HTMLSubScriptInc[$i]=='{') {
                    $PopCount++;
                }
                if ($HTMLSubScriptInc[$i]=='}') {
                    $PopCount--;
                }
                if ($PopCount>=0) {
                    $HTMLSubScript = $HTMLSubScript.$HTMLSubScriptInc[$i];
                }
            }
            $HTMLSubScriptInc_ex = "\subscript{".$HTMLSubScript."}";
            $data = str_replace($HTMLSubScriptInc_ex, "<sub>".$HTMLSubScript."</sub>", $data);
        }

        // Extract footnotes.
        $footnotes_all="";
        $zx=1;
        for ($i=0;$i<(strlen($data)-1);$i++) {
            if ($data[$i]=='\\') {
                $SearchKeyFooter = substr($data, $i, 10);
                if ($SearchKeyFooter=='\\footnote{') {
                    $PopCount = 1 ;
                    $ix = 10;
                    while ($PopCount>0) {
                        if ($data[$i+$ix]=='{') {
                            $PopCount++;
                        }
                        if ($data[$i+$ix]=='}') {
                            $PopCount--;
                        }
                        $SearchKeyFooter = $SearchKeyFooter.$data[$i+$ix];
                        $ix++;
                    }
                    $i = $i + $ix;

                    $data = str_replace($SearchKeyFooter, "<sup><a href=\#f".$zx.">[".$zx."]</a></sup>", $data);
                    $SearchKeyFooter = substr($SearchKeyFooter, 0, -1);
                    $SearchKeyFooter = str_replace('\\footnote{', "", $SearchKeyFooter);
                    $footnotes_all=$footnotes_all."<li id=f".$zx.">".$SearchKeyFooter."</li>";
                    $zx++;
                    // echo $footnotes_all;
                }
            }
        }

        $parsedTree = $parser->parse($footnotes_all);
        $footnotes_all = $htmlRenderer->render($parsedTree);


        $all_images =array();
        $ChecKinImages = 0;
        $zix=0;
        $ChecKinImagesStatic="";
        for ($i=0;$i<(strlen($data)-1);$i++) {
            if ($data[$i]=='\\') {
                $HTMLImg = substr($data, $i, 14);
                ;
                if ($HTMLImg =='\\begin{figure}') {
                    $ChecKinImages = 1;
                    $zix = $i+14;
                    while ($ChecKinImages) {
                        $HTMLImg2 = substr($data, $zix, 12);
                        $ChecKinImagesStatic = $ChecKinImagesStatic.$data[$zix];
                        if ($HTMLImg2 =='\\end{figure}') {
                            $ChecKinImages=0;
                            array_push($all_images, $ChecKinImagesStatic.addslashes("end{figure}"));
                            $ChecKinImagesStatic = "";
                        }
                        $zix++;
                    }
                    $i = $zix;
                }
            }
        }

        $AllImagesHTML ="";
        $image_manager = array();
        foreach ($all_images as $all_image) {
            preg_match('/.caption{(.*)}/', $all_image, $caption);
            preg_match('/includegraphics.*{(.*)}/', $all_image, $link);
            preg_match('/label{(.*)}/', $all_image, $label);


            $parsedTree = $parser->parse($label[1]);
            $label = $htmlRenderer->render($parsedTree);

            $parsedTree = $parser->parse($caption[1]);
            $caption = $htmlRenderer->render($parsedTree);

            $MakeImgHTML ="";
            array_push($image_manager, $link[1]."*res*".$caption);
            $MakeImgHTML = "<img onerror=\"\" src='".$link[1]."'><span><b>".$label."</b>: ".$caption."</span>";
            $AllImagesHTML = $AllImagesHTML.$MakeImgHTML ;
            $data = str_replace($all_image, "", $data);
        }

        $data = str_replace("<br />", "", $data);

        $data = mb_convert_encoding($data, "HTML-ENTITIES", "UTF-8");
        $parsedTree = $parser->parse($data);

        $html = ' <link rel="stylesheet" href="/assets/css/artilce_web_template.css"><script src="/assets/js/jquery.min.js"></script><body>';
        $html = $html." ".$htmlRenderer->render($parsedTree);

        $html = str_replace("<h3>*</h3>", "", $html);
        if ($AllImagesHTML!="") {
            $AllImagesHTML = "<hr><p style='text-align:center'><b>Figures</b></p><hr>".$AllImagesHTML."<hr><br>";
        }

        $html = $html.$AllImagesHTML."<hr><p style='text-align:center'><b>Footnotes</b></p><hr><ul>".$footnotes_all."</ul><hr><p style='text-align:center'><b>Version</b></p><hr></body><script src='some.js'></script>";

        $ResponseData = json_encode(array('result' =>html_entity_decode($template.$html),'image_manager'=>$image_manager,'bib_manager'=>$identified_cits));
        return $this->getResponse()->withStringBody($ResponseData)->withType('json');
    }

    public function bibUpload()
    {
        $file = $this->getRequest()->getData('file');
        $citations = $this->getRequest()->getData('citations');

        // Create and configure a Listener
        $listener = new Listener();
        $listener->addProcessor(new Processor\TagNameCaseProcessor(CASE_LOWER));

        $parser = new Parser();
        $parser->addListener($listener);

        // Parse the content, then read processed data from the Listener
        $parser->parseString($file->getStream()->getContents());
        $entries = $listener->export();

        $this->set('entries', $entries);
        $this->set('_citations', $citations);
        $this->set('_serialize', 'entries');

        $this->viewBuilder()->setClassName('JournalBibliography');
    }

    public function linkPublications()
    {
    }

    public function linkSuggest()
    {
        $this->autoRender = false;
        $search_key = $this->getRequest()->getParam('key');
        $type = $this->getRequest()->getParam('type');
        if ($type=='articles') {
            $articles =  $this->Articles->find('all', [
            'fields' => ['title','id'],
            'order' => ['title' => 'asc']
            ])->where(['title LIKE' => '%'. $search_key.'%'])->all();

            $resultJ = json_encode($articles);
        } else {
            $pubs =  $this->Publications->find('all', [
            'fields' => ['bibtexkey','id'],
            'order' => ['bibtexkey' => 'asc']
            ])->where(['bibtexkey LIKE' => '%'. $search_key.'%'])->all();

            $resultJ = json_encode($pubs);
        }
        return $this->getResponse()->withStringBody($resultJ)->withType('json');
    }

    public function getArticleByID()
    {
        $this->autoRender = false;
        $id = $this->getRequest()->getParam('id');
        $type = $this->getRequest()->getParam('type');

        if ($type=='article') {
            $article =  $this->Articles->find()->where(['id' => $id])->first();
            $resultJ = json_encode($article);
        } else {
            $pubs =  $this->Publications->find()->where(['id' => $id])->first();
            $resultJ = json_encode($pubs);
        }

        return $this->getResponse()->withStringBody($resultJ)->withType('json');
    }

    public function completeLink()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $publication = $this->getRequest()->getParam('publication');

        if (is_numeric($article)=="" || is_numeric($publication)=="") {
            throw new ForbiddenException(__('Request not allowed.'));
        }
        $link =  $this->ArticlesPublications->find()->where(['article_id' => $article, 'publication_id' => $publication])->first();
        if (!$link) {
            $link = $this->ArticlesPublications->newEntity();
            $link->article_id = $article;
            $link->publication_id = $publication;
        }

        $link->status=1;
        $this->ArticlesPublications->save($link);
        echo 'success';
    }
    public function viewLink()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $publication = $this->getRequest()->getParam('publication');

        if (is_numeric($article)=="" || is_numeric($publication)=="") {
            throw new ForbiddenException(__('Request not allowed.'));
        }

        $connection = ConnectionManager::get('default');

        $resultJ = $connection->execute("select a.title, p.bibtexkey, articles_publications.id,articles_publications.status, a.id as aid, p.id as pid from articles_publications join articles as a,
        publications p where article_id = '".$article."' and publication_id = '".$publication."'
         and p.id = publication_id and a.id = article_id;'")->fetchAll('assoc');

        if (count($resultJ)==0) {
            $resultJ = json_encode(array('status'=>'norecord', 'data' => "0"));
        } else {
            $resultJ = json_encode($resultJ);
        }

        return $this->getResponse()->withStringBody($resultJ)->withType('json');
    }

    public function completeUnlink()
    {
        $this->autoRender = false;
        $article = $this->getRequest()->getParam('article');
        $publication = $this->getRequest()->getParam('publication');
        $link =  $this->ArticlesPublications->find()->where(['article_id' => $article, 'publication_id' => $publication])->first();
        if ($link) {
            $link ->status=0;
            $this->ArticlesPublications->save($link);
            echo 'success';
        } else {
            throw new ForbiddenException(__('Unable to find a link.'));
        }
    }

    public function viewAllArticleLinks()
    {
        $this->autoRender = false;
        $id = $this->getRequest()->getParam('id');
        $type = $this->getRequest()->getParam('type');
        $connection = ConnectionManager::get('default');

        if ($type=='article') {
            $resultJ = $connection->execute("select a.title, p.bibtexkey, articles_publications.id,articles_publications.status, a.id as aid, p.id as pid from articles_publications join articles as a,
        publications p where articles_publications.status = 1 and article_id = '".$id."'
         and p.id = publication_id and a.id = article_id;'")->fetchAll('assoc');
        } else {
            $resultJ = $connection->execute("select a.title, p.bibtexkey, articles_publications.id,articles_publications.status, a.id as aid, p.id as pid from articles_publications join articles as a,
        publications p where articles_publications.status = 1 and publication_id = '".$id."'
         and p.id = publication_id and a.id = article_id;'")->fetchAll('assoc');
        }


        if (count($resultJ)==0) {
            $resultJ = json_encode(array('status'=>'norecord', 'data' => "0"));
        } else {
            $resultJ = json_encode($resultJ);
        }

        return $this->getResponse()->withStringBody($resultJ)->withType('json');
    }
}
