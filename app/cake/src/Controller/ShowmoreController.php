<?php
namespace App\Controller;

// Once the database is updated, bake a new cake.
// 		1. Test Regex
//		2. Add validation for search and advancedsearch
//		3. Test search adn advancedsearch
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

class ShowmoreController extends AppController
{
    public static function showmore($cdli = null, $publication = null)
    {
        $artifact = TableRegistry::get('Artifacts');
        $query =   $artifact->find()->where(['Artifacts.id'=>$cdli]);
        $query->matching('Collections');
        $query->matching('Periods');
        $query->matching('Publications.Authors', function ($q) use ($publication) {
            return $q->where(['Publications.id' => $publication]);
        });
        $query->matching('Languages');
        $query->matching('Genres');
        $query->matching('Collections');
        $query->matching('Materials');
        $query->matching('ArtifactTypes');
        $query->matching('Proveniences');
        $query->matching('Inscriptions');
        $query->matching('Materials');
        $query->matching('ArtifactTypes');
        $query->matching('Genres');
        // debug($query);
        $result = $query->all();
        return $result;
    }

    public static function findSecPub($cdli = null)
    {
        $artifact = TableRegistry::get('Artifacts');
        $query =   $artifact->find()->where(['Artifacts.id'=>$cdli]);
        $query->matching('Publications', function ($q) {
            return $q->where(['publication_type' => "secondary"]);
        });
        // debug($query);
        $result = $query->all();
        return $result;
    }

    public function index()
    {
        $cdli = $this->getRequest()->getQuery('CDLI');
        $publication = $this->getRequest()->getQuery('Publication');

        if (!empty($cdli) and !empty($publication)) {
            $result = $this->showmore($cdli, $publication);
            $secPubs = $this->findSecPub($cdli);
            $this->set('result', $result);
            $this->set('secPubs', $secPubs);
        }
    }
}
