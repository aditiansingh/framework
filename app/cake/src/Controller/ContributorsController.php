<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Contributors Controller
 *
 * @property \App\Model\Table\ContributorsTable $Contributors
 *
 * @method \App\Model\Entity\Contributor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContributorsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Authors']
        ];
        $contributors = $this->paginate($this->Contributors);

        $this->set(compact('contributors'));
    }

    /**
     * View method
     *
     * @param string|null $id Contributor id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contributor = $this->Contributors->get($id, [
            'contain' => ['Authors']
        ]);

        $this->set('contributor', $contributor);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contributor = $this->Contributors->newEntity();
        if ($this->getRequest()->is('post')) {
            $contributor = $this->Contributors->patchEntity($contributor, $this->getRequest()->getData());
            if ($this->Contributors->save($contributor)) {
                $this->Flash->success(__('The contributor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contributor could not be saved. Please, try again.'));
        }
        $authors = $this->Contributors->Authors->find('list', ['limit' => 200]);
        $this->set(compact('contributor', 'authors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Contributor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contributor = $this->Contributors->get($id, [
            'contain' => []
        ]);
        if ($this->getRequest()->is(['patch', 'post', 'put'])) {
            $contributor = $this->Contributors->patchEntity($contributor, $this->getRequest()->getData());
            if ($this->Contributors->save($contributor)) {
                $this->Flash->success(__('The contributor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contributor could not be saved. Please, try again.'));
        }
        $authors = $this->Contributors->Authors->find('list', ['limit' => 200]);
        $this->set(compact('contributor', 'authors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Contributor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->getRequest()->allowMethod(['post', 'delete']);
        $contributor = $this->Contributors->get($id);
        if ($this->Contributors->delete($contributor)) {
            $this->Flash->success(__('The contributor has been deleted.'));
        } else {
            $this->Flash->error(__('The contributor could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
