<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Mailer\Email;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;
use GoogleAuthenticator\GoogleAuthenticator;

/**
 * Forgot Password Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 */
class ForgotController extends AppController
{
    /**
     * Load Auth component
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->Auth->allow();
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        
        // If user is already authenticated
        if ($this->Auth->user()) {
            return $this->redirect([
                'controller' => 'Home',
                'action' => 'index',
            ]);
        }

        $session = $this->getRequest()->getSession();
        $session_forgot = $session->read('forgotPassword');
        $user = null;

        // If Session variable forgot is null and Controller's action is 'password'
        if (is_null($session_forgot) && ($this->getRequest()->getParam('action') === 'password')) {
            $token = $this->getRequest()->getParam('token');
            $generated_at = substr($token, 0, 5).substr($token, 18, 5);
            $token_pass = substr($token, 5, 13);
            $user = $this->userDetails([$token_pass, $generated_at], 2);
        } else {
            if ($this->getRequest()->getParam('action') === 'password') {
                $token = $this->getRequest()->getParam('token');
                $generated_at = substr($token, 0, 5).substr($token, 18, 5);
                $token_pass = substr($token, 5, 13);
    
                if ($token_pass != $session_forgot['token_pass']) {
                    $user = $this->userDetails([$token_pass, $generated_at], 2);
                }
            }
        }

        if (!is_null($user)) {
            $expired_at = $generated_at + 15*60;
            if ((Time::now())->toUnixString() < $expired_at) {
                $forgotPassword['2fa'] = false;
                $forgotPassword['token_pass'] = $token_pass;
                $forgotPassword['generated_at'] = $generated_at;
                $forgotPassword['expired_at'] = $expired_at;
                $session->write('forgotPassword', $forgotPassword);
            }
        }
    }

    /**
     * Index method
     * $type = ['password']
    **/
    public function index($type = null)
    {
        if ($type === 'password') {
            if ($this->getRequest()->is('post')) {
                $data = $this->getRequest()->getData();
                $user = $this->userDetails($data['email'], 1);

                // Check if user deactivated or banned
                if (!$user['active']) {
                    // If last login more than 6 months else banned
                    if (Time::now()->toUnixString() - $user['last_login_at']->toUnixString() > 6*30*24*60*60) {
                        return $this->Flash->error(__('Your account has been suspended due to inactivity.'));
                    } else {
                        return $this->Flash->error(__('Your account has been banned.'));
                    }
                }

                if (is_null($user)) {
                    $this->Flash->error(__('Email address does not exist. Please try again'));
                } else {
                    $time = Time::now();
                    $unixTime = ($time)->toUnixString();

                    // Check if reset password requested witin span of 5 minutes
                    if (!is_null($user['generated_at']) && ($unixTime - $user['generated_at']->toUnixString() < 5*60)) {
                        return $this->Flash->error(__('Please try after 5 min.'));
                    } else {
                        $randomKey = uniqid();
                        $uniqueKey = substr($unixTime, 0, 5).$randomKey.substr($unixTime, 5);
                        $url = Router::Url(['controller' => 'Forgot', 'action' => $type], true) . '/reset/' . $uniqueKey;

                        $updateUser['token_pass'] = $randomKey;
                        $updateUser['generated_at'] = $time;

                        $saveUser = $this->Users->patchEntity($user, $updateUser);

                        if (empty($saveUser->getErrors()) && $this->Users->save($saveUser)) {
                            $this->Flash->success(__('Check you email for password reset link!'), ['class' => 'alert alert-danger']);
                            $this->Flash->set(__($url));

                            // Code for Sending Email
                            // $this->sendResetEmail($url, $user);
                            $this->redirect(['controller' => 'Home', 'action' => 'index']);
                        } else {
                            $this->Flash->error(__('Error in Processing token!'));
                        }
                    }
                }
            }
        } else {
            return $this->redirect(['controller' => 'Home', 'action' => 'index']);
        }
    }

    // Function to send email
    private function sendResetEmail($url, $user)
    {
        $email = new Email();
        $email->template('resetpassword');
        $email->emailFormat('both');
        $email->from('no-reply@cdli.org');
        $email->to($user['email'], $user['username']);
        $email->subject('Reset your password');
        $email->viewVars([
            'url' => $url,
            'username' => $user['username']
        ]);

        if ($email->send()) {
            $this->Flash->success(__('Check your email for your reset password link'));
        } else {
            $this->Flash->error(__('Error sending email: ') . $email->smtpError);
        }
    }

    // New Password Page
    public function password($token = null)
    {
        if ($token) {
            $session = $this->getRequest()->getSession();

            if (is_null($session->read('forgotPassword'))) {
                $this->Flash->error(__('Invalid or expired passkey. Please check your email or try again'));
                return $this->redirect([
                    'controller' => 'Home',
                    'action' => 'index'
                ]);
            } else {
                $session_forgot = $session->read('forgotPassword');
                if (!$session_forgot['2fa']) {
                    $this->setAction('twofaverify');
                } else {
                    if ($this->getRequest()->is('post')) {
                        $formData = $this->getRequest()->getData();
                        $user = ($this->userDetails([$session_forgot['token_pass'], $session_forgot['generated_at']], 2));

                        if (!empty($formData)) {
                            $badpasswordStatus = (new \App\Controller\UsersController())->checkBadPasswords($user, $formData['password']);
                            
                            $previousPassword = $user['password'];
                            $sameAsPreviousPassword = (new DefaultPasswordHasher())->check($formData['password'], $previousPassword);

                            // Password check is kept with only previous password. (Not the list of previously used passwords by that user)
                            if ($sameAsPreviousPassword) {
                                return $this->Flash->error(__("The new password cannot be the same as your previously used passwords."));
                            }

                            $formData['token_pass'] = null;
                            $formData['generated_at'] = null;
                            $formData['modified_at'] = Time::now();

                            $updateduser = $this->Users->patchEntity($user, $formData);

                            $errors = $updateduser->getErrors();

                            $errorStatus = !empty($errors) || $badpasswordStatus;
                            
                            // Display errors generated after validation of user's data.
                            if ($errorStatus) {
                                if (!empty($errors)) {
                                    foreach ($errors as $error) {
                                        foreach ($error as $key => $value) {
                                            $this->Flash->error($value);
                                        }
                                    }
                                }
                                if ($badpasswordStatus) {
                                    return $this->Flash->error("Please use a more secure password. Try with a sentence.");
                                }
                            } else {
                                if ($this->Users->save($user)) {
                                    $session->delete('forgotPassword');
                                    $this->Flash->success(__('Your password has been updated.'));
                                    return $this->redirect([
                                        'controller' => 'Users',
                                        'action' => 'login'
                                    ]);
                                } else {
                                    $this->Flash->error(__('The password could not be updated. Please, try again.'));
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $this->Flash->error(__('Invalid or expired passkey. Please check your email or try again'));
            return $this->redirect([
                'controller' => 'Forgot',
                'action' => 'index'
            ]);
        }
    }

    /**
     * 2FA Verify method
     *
     * @return \Cake\Http\Response|void
     */
    public function twofaverify()
    {
        $this->loadComponent('GoogleAuthenticator');
        $ga = $this->GoogleAuthenticator;
        $forgotPassword = $this->getRequest()->getSession()->read('forgotPassword');

        if ($this->getRequest()->is('post')) {
            $twofa_key = ($this->userDetails([$forgotPassword['token_pass'], $forgotPassword['generated_at']], 2))['2fa_key'];
            $secret = $twofa_key;
            $oneCode = $this->getRequest()->getData('code');
            $checkResult = $ga->verifyCode($secret, $oneCode, 2);

            if ($checkResult) {
                $this->getRequest()->getSession()->write('forgotPassword.2fa', true);
                $token = substr($forgotPassword['generated_at'], 0, 5).$forgotPassword['token_pass'].substr($forgotPassword['generated_at'], 5, 5);
                return $this->redirect('/forgot/password/reset/'.$token);
            } else {
                $this->Flash->error(__('Wrong code entered.Please try again.'), [
                    'class' => 'alert alert-danger'
                ]);
            }
        }
    }

    // Get User Details
    // ($data, $type) = (email, 1), ([token_pass, generated_at], 2)
    public function userDetails($data, $type)
    {
        $this->loadModel('Users');
        
        if ($type == 1) {
            return $this->Users->findByEmail($data)->first();
        } else {
            // Converting Unixtime to timestamp
            $data[1] = date('Y-m-d H:i:s', $data[1]);
            return $this->Users->find('all', ['conditions' => ['token_pass' => $data[0], 'generated_at' => $data[1]]])->first();
        }
    }
}
