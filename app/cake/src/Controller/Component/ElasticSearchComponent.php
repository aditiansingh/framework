<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Http\Client;

class ElasticSearchComponent extends Component
{
    /**
     * elasticSearchCredentials method
     *
     * @return array [username, password]
     */
    private function elasticSearchCredentials()
    {
        $username = 'elastic';
        $password = 'elasticchangeme';
        return [$username, $password];
    }

    /**
     * filterESQuery method
     *
     * @return ElasticSearch query for filters. (string)
     */
    public function filterESQuery($filterData)
    {
        /**
            $filterForSameField = '
                {
                    \"match_phrase\":  {
                        \"materials\": \"limestone\"
                    }
                }';

            $filterForCombinedInnerField = '        {
                "bool": {
                    "should": [
                        {
                            "match_phrase":  {
                                "atype": "barrel"
                            }
                        },
                        {
                            "match_phrase":  {
                                "atype": "other (see object remarks)"
                            }
                        }
                    ]
                }
            }';
        */

        $filterWiseArray = [];

        foreach ($filterData as $filter => $valueAray) {
            $filterValuesArray = [];

            foreach ($valueAray as $value) {
                $tempInner = '
                                        {
                                            "match_phrase":  {
                                                "'.$filter.'": "'.$value.'"
                                            }
                                        }';
                array_push($filterValuesArray, $tempInner);
            }

            if (!empty($filterValuesArray)) {
                $filterWiseInnerValue = implode(',', $filterValuesArray);

                $filterWiseCompleteQuery = '
                            {
                                "bool": {
                                    "should": [ '.$filterWiseInnerValue.'
                                    ]
                                }
                            }';

                array_push($filterWiseArray, $filterWiseCompleteQuery);
            }
        }

        $finalFilterESQuery = implode(',', $filterWiseArray);

        return $finalFilterESQuery;
    }

    public function stripMuseumId($museumId)
    {
        $museumIdPattern = '/[A-Za-z]+\s*\d+/';
        $strippedMuseumId = '';
        if (preg_match($museumIdPattern, $museumId)) {
            $strippedMuseumId = preg_replace_callback(
                $museumIdPattern,
                function ($matches) {
                    return preg_filter('/[0-9]+/', '*$0', $matches[0]);
                },
                $museumId
            );
        }
        return $strippedMuseumId;
    }

    public function stripId($id)
    {
        //Regex that matches letters,spaces and frontal zeroes.
        $idPattern = '/[a-zA-Z]\s*0*/';
        $strippedId = '';
        if (preg_match($idPattern, $id)) {
            //Strip letters and frontal zeroes
            $strippedId = preg_replace($idPattern, '', $id);
        }
        return $strippedId;
    }

    public function multiIdSearch($field, $value)
    {
        $value = preg_replace('/,/', " OR ", $value);
        $keywords = preg_split("/(\s)/", $value);
        $query = $field.':(';
        foreach ($keywords as $keyword) {
            if ($keyword !== 'OR' || $keyword !== 'AND') {
                $strippedData = ($field === 'museum_no') ? $this->stripMuseumId($keyword) : $this->stripId($keyword);
                $query .= (empty($strippedData)) ? $keyword.' ' : $keyword.' OR '.$strippedData.' ';
            } else {
                $query .= ' '.$keyword.' ';
            }
        }
        $query .= ')';
        return $query;
    }
    
    public function idSearchQuery($field, $value)
    {
        $tempString = '';
        if ($field === 'museum_no') {
            //Multiple search fields
            if (preg_match('/(OR|AND|,)/', $value)) {
                $tempString = $this->multiIdSearch($field, $value);
            } else {
                $strippedMuseumId = $this->stripMuseumId($value);
                $tempString = (empty($strippedMuseumId)) ? $field.':('.$value.')' : $field.':('.$value.' OR '.$strippedMuseumId.')';
            }
        } else {
            //Multiple search fields
            if (preg_match('/(OR|AND|,)/', $value)) {
                $tempString = $this->multiIdSearch($field, $value);
            } else {
                $strippedId = $this->stripId($value);
                $tempString = (empty($strippedId)) ? $field.':('.$value.')' : $field.':('.$value.' OR '.$strippedId.')';
            }
        }
        return $tempString;
    }

    /**
     * searchAdvancedArtifacts method
     *
     * Advanced Search.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     *
     * @return
     */
    public function searchAdvancedArtifacts($queryData, $settings, $filterData = [])
    {
        $keyword = implode(' AND ', array_map(
            function ($value, $field) {
                $http = new Client();
                $fuzzyIdFields = [
                    'composite_no',
                    'seal_no',
                    'museum_no',
                    'id'
                ];
                
                // Processing input for Id's field
                if (in_array($field, $fuzzyIdFields)) {
                    $tempString = $this->idSearchQuery($field, $value);
                } elseif ($field === 'transliteration_permutation') {
                    $atf = ['atf' => $value];
                    $response = $http->post(
                        'http://jtf-lib:3003/jtf-lib/api/getSignnamesATFLINE/',
                        json_encode($atf),
                        ['type' => 'json']
                    );
                    $signNames = $response->getStringBody();
                    if (!empty($signNames)) {
                        $tempString = 'transliteration_sign_names :('.$signNames.') OR ';
                    }
                    $tempString .= 'atf :('.$value.') OR transliteration_for_search :('.$value.')';
                } else {
                    $tempString = $field.':('.$value.')';
                }
                return $tempString;
            },
            $queryData,
            array_keys($queryData)
        ));

        if (!empty($filterData)) {
            $filterQueryString = $this->filterESQuery($filterData);
        }

        if (!$settings['canViewPrivateArtifacts']) {
            $keyword = '('.$keyword.')'.' AND is_public:true';
        }

        $data = "
            {
                \"_source\": {
                    \"includes\": [
                        \"id\", \"adesignation\", \"collection\", \"materials\", \"artifact_type_id\", \"atype\", \"period_id\", \"period\", \"provenience_id\", \"provenience\", \"genres\", \"languages\", \"authors\", \"year\"
                    ]
                },
                \"query\": {
                    \"bool\": {
                        \"must\": [
                            {
                                \"query_string\": {
                                    \"query\": \"".$keyword."\",
                                    \"default_operator\": \"and\"
                                }
                            }";
        if (!empty($filterData)) {
            $data .= ",".$filterQueryString;
        }
        $data .= "
                        ]
                    }
                },
                \"sort\": [
                    {
                        \"_score\": {
                            \"order\": \"desc\"
                        }
                    }
                ],
                \"size\": 10000
            }
        ";

        $result = [];

        $filters = [
            'materials' => [],
            'collection' => [],
            'atype' => [],
            'period' => [],
            'provenience' => [],
            'genres' => [],
            'languages' => [],
            'authors' => [],
            'year' => []
        ];

        $resultArray = $this->getDataFromES('advanced_artifacts', $data, 0);

        $totalHits = $resultArray['total'];

        // Calculating results size till previous pages.
        $totalPreviousResults = ($settings['Page'] == 1) ? 0 : ($settings['Page'] - 1) * $settings['PageSize'];

        // Set No. of result to be stored in session from overall results.
        $resultSet = (int)floor($totalPreviousResults / 10000);

        // Result Size stored in Session.
        $resultSize = 10000;

        // If there is any need for extra result which will be required on certain page where the set of results stored in session is not enough to display results (as set by page size) on that page.
        $checkIfExtraResultsRequired = 10000/$settings['PageSize'];

        if ($checkIfExtraResultsRequired != floor($checkIfExtraResultsRequired)) {
            $changeResultSet = floor(($totalPreviousResults + $settings['PageSize'])/10000) == $resultSet ? 0 : 1;

            if ($changeResultSet) {
                $resultSize += ($settings['PageSize'] - ($resultSize - $totalPreviousResults));
            }
        }

        $lowerBound = $resultSet * 10000;
        $upperBound = $lowerBound + $resultSize - 1;

        $trackingCount = 0;

        foreach ($resultArray['hits'] as $resultRow) {
            if ($lowerBound <= $trackingCount && $trackingCount <= $upperBound) {
                $result[$resultRow['id']] = [
                    'artifact' => [
                        'designation' => $resultRow['adesignation']
                    ],
                    'collection' => [
                        'collection' => $resultRow['collection']
                    ],
                    'material' => [
                        'material' => $resultRow['materials']
                    ],
                    'artifact_type' => [
                        'id' => $resultRow['artifact_type_id'],
                        'artifact_type' => $resultRow['atype']
                    ],
                    'period' => [
                        'period_id' => $resultRow['period_id'],
                        'period' => $resultRow['period']
                    ],
                    'provenience' => [
                        'provenience_id' => $resultRow['provenience_id'],
                        'provenience' => $resultRow['provenience']
                    ],
                    'languages' => [
                        'languages' => $resultRow['languages']
                    ]
                ];
            }

            $filters['genres'] = array_merge($filters['genres'], array_fill_keys(array_filter(explode(' ; ', $resultRow['genres']), 'strlen'), 0));

            $filters['languages'] = array_merge($filters['languages'], array_fill_keys(array_filter(explode(' ; ', $resultRow['languages']), 'strlen'), 0));

            $filters['materials'] = array_merge($filters['materials'], array_fill_keys(array_filter(explode(', ', $resultRow['materials']), 'strlen'), 0));

            $filters['collection'] = $resultRow['collection'] != '' ? array_merge($filters['collection'], [$resultRow['collection'] => 0]) : $filters['collection'];

            $filters['atype'] = $resultRow['atype'] != '' ? array_merge($filters['atype'], [$resultRow['atype'] => 0]) : $filters['atype'];

            $filters['provenience'] = $resultRow['provenience'] != '' ? array_merge($filters['provenience'], [$resultRow['provenience'] => 0]) : $filters['provenience'];

            $filters['period'] = $resultRow['period'] != '' ? array_merge($filters['period'], [$resultRow['period'] => 0]) : $filters['period'];

            $filters['authors'] = array_merge($filters['authors'], array_fill_keys(array_filter(explode(PHP_EOL, $resultRow['authors']), 'strlen'), 0));

            $filters['year'] = $filters['year'] + array_fill_keys(array_filter(explode(' ', $resultRow['year']), 'strlen'), 0);

            $trackingCount += 1;
        }

        $customResult = [
            'result' => $result,
            'totalHits' => $totalHits,
            'resultSet' => $resultSet,
            'filters' => $filters
        ];

        return $customResult;
    }

    public function expandQuery($queryData)
    {
        $mappingQueryFields = [
            'publication' => [
                'designation_exactref',
                'pdesignation',
                'bibtexkey',
                'year',
                'publisher',
                'school',
                'series',
                'title',
                'book_title',
                'chapter',
                'journal',
                'editors',
                'authors'
            ],
            'collection' => [
                'collection'
            ],
            'provenience' => [
                'provenience',
                'written_in',
                'region'
            ],
            'period' => [
                'period'
            ],
            'inscription' => [
                'atf',
                'translation',
                'transliteration_for_search'
            ],
            'id' => [
                'pdesignation',
                'adesignation',
                'excavation_no',
                'bibtexkey',
                'designation_exactref',
                'composite_no',
                'seal_no',
                'museum_no',
                'id'
            ],
            'keyword' => [
                'designation_exactref',
                'pdesignation',
                'adesignation',
                'excavation_no',
                'bibtexkey',
                'year',
                'publisher',
                'school',
                'series',
                'title',
                'book_title',
                'chapter',
                'journal',
                'editors',
                'authors',
                'collection',
                'provenience',
                'written_in',
                'region',
                'period',
                'atf',
                'translation',
                'composite_no',
                'seal_no',
                'museum_no',
                'id'
            ]
        ];

        $fuzzyIdFields = [
            'composite_no',
            'seal_no',
            'museum_no',
            'id'
        ];

        $queryStringIndividual = '';

        foreach ($queryData as $keyValue) {
            $key = array_keys($keyValue)[0];
            $value = array_values($keyValue)[0];

            if ($key !== 'operator') {
                $tempQueryArray = [];
                
                foreach ($mappingQueryFields[$key] as $field) {
                    $tempString = '';
                    // Processing input for Id's field
                    if (in_array($field, $fuzzyIdFields)) {
                        $tempString = $this->idSearchQuery($field, $value);
                    } else {
                        $tempString = $field.':('.$value.')';
                    }
                    array_push($tempQueryArray, $tempString);
                }
                $queryStringIndividual .= '('.implode(' OR ', $tempQueryArray).')';
            } else {
                $queryStringIndividual .= ' '.$value.' ';
            }
        }

        return $queryStringIndividual;
    }


    /**
     * searchAdvancedArtifacts method
     *
     * Advanced Search.
     * Refer: https://gitlab.com/cdli/framework/-/issues/298
     *
     * @param
     *
     * @return
     */
    public function simpleSearch($queryData, $settings, $filterData = [])
    {
        $keyword = $this->expandQuery($queryData);

        if (!empty($filterData)) {
            $filterQueryString = $this->filterESQuery($filterData);
        }

        if (!$settings['canViewPrivateArtifacts']) {
            $keyword = '('.$keyword.')'.' AND is_public:true';
        }

        $data = "
            {
                \"_source\": {
                    \"includes\": [
                        \"id\", \"adesignation\", \"collection\", \"materials\", \"artifact_type_id\", \"atype\", \"period_id\", \"period\", \"provenience_id\", \"provenience\", \"genres\", \"languages\", \"authors\", \"year\"
                    ]
                },
                \"query\": {
                    \"bool\": {
                        \"must\": [
                            {
                                \"query_string\": {
                                    \"query\": \"".$keyword."\",
                                    \"default_operator\": \"and\"
                                }
                            }";
        if (!empty($filterData)) {
            $data .= ",".$filterQueryString;
        }
        $data .= "
                        ]
                    }
                },
                \"sort\": [
                    {
                        \"_score\": {
                            \"order\": \"desc\"
                        }
                    }
                ],
                \"size\": 10000
            }
        ";

        $result = [];

        $filters = [
            'materials' => [],
            'collection' => [],
            'atype' => [],
            'period' => [],
            'provenience' => [],
            'genres' => [],
            'languages' => [],
            'authors' => [],
            'year' => []
        ];

        $resultArray = $this->getDataFromES('advanced_artifacts', $data, 0);

        $totalHits = $resultArray['total'];

        // Calculating results size till previous pages.
        $totalPreviousResults = ($settings['Page'] == 1) ? 0 : ($settings['Page'] - 1) * $settings['PageSize'];

        // Set No. of result to be stored in session from overall results.
        $resultSet = (int)floor($totalPreviousResults / 10000);

        // Result Size stored in Session.
        $resultSize = 10000;

        // If there is any need for extra result which will be required on certain page where the set of results stored in session is not enough to display results (as set by page size) on that page.
        $checkIfExtraResultsRequired = 10000/$settings['PageSize'];

        if ($checkIfExtraResultsRequired != floor($checkIfExtraResultsRequired)) {
            $changeResultSet = floor(($totalPreviousResults + $settings['PageSize'])/10000) == $resultSet ? 0 : 1;

            if ($changeResultSet) {
                $resultSize += ($settings['PageSize'] - ($resultSize - $totalPreviousResults));
            }
        }

        $lowerBound = $resultSet * 10000;
        $upperBound = $lowerBound + $resultSize - 1;

        $trackingCount = 0;

        foreach ($resultArray['hits'] as $resultRow) {
            if ($lowerBound <= $trackingCount && $trackingCount <= $upperBound) {
                $result[$resultRow['id']] = [
                    'artifact' => [
                        'designation' => $resultRow['adesignation']
                    ],
                    'collection' => [
                        'collection' => $resultRow['collection']
                    ],
                    'material' => [
                        'material' => $resultRow['materials']
                    ],
                    'artifact_type' => [
                        'id' => $resultRow['artifact_type_id'],
                        'artifact_type' => $resultRow['atype']
                    ],
                    'period' => [
                        'period_id' => $resultRow['period_id'],
                        'period' => $resultRow['period']
                    ],
                    'provenience' => [
                        'provenience_id' => $resultRow['provenience_id'],
                        'provenience' => $resultRow['provenience']
                    ],
                    'languages' => [
                        'languages' => $resultRow['languages']
                    ],
                ];
            }

            $filters['genres'] = array_merge($filters['genres'], array_fill_keys(array_filter(explode(' ; ', $resultRow['genres']), 'strlen'), 0));

            $filters['languages'] = array_merge($filters['languages'], array_fill_keys(array_filter(explode(' ; ', $resultRow['languages']), 'strlen'), 0));

            $filters['materials'] = array_merge($filters['materials'], array_fill_keys(array_filter(explode(', ', $resultRow['materials']), 'strlen'), 0));

            $filters['collection'] = $resultRow['collection'] != '' ? array_merge($filters['collection'], [$resultRow['collection'] => 0]) : $filters['collection'];

            $filters['atype'] = $resultRow['atype'] != '' ? array_merge($filters['atype'], [$resultRow['atype'] => 0]) : $filters['atype'];

            $filters['provenience'] = $resultRow['provenience'] != '' ? array_merge($filters['provenience'], [$resultRow['provenience'] => 0]) : $filters['provenience'];

            $filters['period'] = $resultRow['period'] != '' ? array_merge($filters['period'], [$resultRow['period'] => 0]) : $filters['period'];

            $filters['authors'] = array_merge($filters['authors'], array_fill_keys(array_filter(explode(PHP_EOL, $resultRow['authors']), 'strlen'), 0));

            $filters['year'] = $filters['year'] + array_fill_keys(array_filter(explode(' ', $resultRow['year']), 'strlen'), 0);

            $trackingCount += 1;
        }

        $customResult = [
            'result' => $result,
            'totalHits' => $totalHits,
            'resultSet' => $resultSet,
            'filters' => $filters
        ];

        return $customResult;
    }

    /**
     * getInscriptionWithArtifactId method
     *
     * To get latest Inscription for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Inscription for specific artifact ID. (Size = 1 if presesnt else empty array)
     */
    public function getInscriptionWithArtifactId($artifactId, $canViewPrivateInscriptions)
    {
        $data = "
        {
            \"query\": {
                \"bool\": {
                    \"must\": [
                        {
                            \"term\": {
                                \"artifact_id\": ".$artifactId."
                            }
                        },
                        {
                            \"term\": {
                                \"is_latest\": true
                            }
                        }";

        if (!$canViewPrivateInscriptions) {
            $data .= ",
                            {
                                \"term\": {
                                    \"is_atf_public\": true
                                }
                            }";
        }

        $data .= "
                    ]
                }
            }
        }
        ";

        $resultArray = $this->getDataFromES('inscription', $data, 1);

        $result = [];

        if (!empty($resultArray['hits'])) {
            $inscription = $resultArray['hits'][0];
            $result = [
                'id' => $inscription['id'],
                'atf' => $inscription['atf'],
                'translation' => $inscription['translation']
            ];
        }

        return $result;
    }

    /**
     * getPublicationWithArtifactId method
     *
     * To get Publication for specified artifact ID.
     *
     * @param
     * artifactId : Artifact ID
     *
     * @return array of Publication for specific artifact ID.
     */
    public function getPublicationWithArtifactId($artifactId)
    {
        $data =  "
        {
            \"query\": {
                \"bool\": {
                    \"must\": [
                        {
                            \"term\": {
                                \"artifact_id\": $artifactId
                            }
                        },
                        {
                            \"term\": {
                                \"accepted\": false
                            }
                        }
                    ]
                }
            }
        }
        ";

        $resultArray = $this->getDataFromES('artifacts_publications', $data, 1);

        $resultArray = $resultArray['hits'];

        $result = [];

        if (!empty($resultArray)) {
            $publication = $resultArray[0];
            $result = [
                'publication' => [
                    'publication_id' => $publication['publication_id'],
                    'year' => $publication['year']
                ],
                'author' => [
                    'id' => $publication['authors_id'],
                    'authors' => $publication['author_authors']
                ]
            ];
        }

        return $result;
    }

    /**
     * getDataFromES method
     *
     * This functions fetched data for specific ElasticSearch query with required parameters using http from ElasticSearch Server.
     *
     * @param
     * esIndex : Elastic Search Index to be used
     * data : Query parameters with respective values
     *
     * @return array of total results count and array of results.
     */
    private function getDataFromES($esIndex, $data, $type)
    {
        [$elasticUser, $elasticPassword] = $this->elasticSearchCredentials();

        $resultArray = [];

        if ($type == 0) {
            $url = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/'.$esIndex.'/_search?scroll=5m';
        } else {
            $url = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/'.$esIndex.'/_search';
        }

        $result = $this->httpModule($url, $data);

        $resultArray['hits'] = array_column($result['hits']['hits'], '_source');

        if ($type == 0) {
            $totalResults = $result['hits']['total']['value'];
            $scrollId = $result['_scroll_id'];

            $scrollUrl = 'http://'.$elasticUser.':'.$elasticPassword.'@elasticsearch:9200/_search/scroll?';

            $scrollData = '
                {
                    "scroll": "5m",
                    "scroll_id": "'.$scrollId.'"
                }
            ';

            for ($loop = 0; $loop < ($totalResults/10000); $loop++) {
                $scrollResults = $this->httpModule($scrollUrl, $scrollData);

                $tempResult = array_column($scrollResults['hits']['hits'], '_source');

                array_push($resultArray['hits'], ...$tempResult);
            }

            $deleteScrollData = '
                {
                    "scroll_id": "'.$scrollId.'"
                }
            ';
            $deletedScroll = $this->httpModule($scrollUrl, $deleteScrollData, 'DELETE');
        }

        $resultArray['total'] = sizeof($resultArray['hits']);

        return $resultArray;
    }

    private function httpModule($url, $data, $requestType = 'GET')
    {
        $http = new Client();

        if ($requestType === 'GET') {
            /**
             * Request body search using HTTP can only be done using POST request
             * Reference:
             * 1. https://stackoverflow.com/questions/978061/http-get-with-request-body,
             * 2. https://stackoverflow.com/questions/36939748/elasticsearch-get-request-with-request-body
            */
            $response = $http->post(
                $url,
                $data,
                ['type' => 'json']
            );
        } elseif ($requestType === 'DELETE') {
            $response = $http->delete(
                $url,
                $data,
                ['type' => 'json']
            );
        }
        $result = $response->getJson();

        return $result;
    }
}
