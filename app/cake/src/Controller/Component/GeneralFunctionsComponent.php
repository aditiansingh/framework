<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class GeneralFunctionsComponent extends Component
{
    public $components = ['Auth'];

    /**
     * Check if role exists for the given user
     *
     * @param : $rolesToBeChecked
     * rolesToBeChecked -> array of roles to be checked
     *
     * @return : true or false
     */
    public function checkIfRolesExists($rolesToBeChecked)
    {
        $roles = $this->Auth->user('roles');

        if (!is_null($roles)) {
            $checkIfRoleExistsForAccess = array_intersect($roles, $rolesToBeChecked);

            if (!empty($checkIfRoleExistsForAccess)) {
                return true;
            }
        }

        return false;
    }

    /**
     * User Role
     *
     * @param : $userId
     * userId -> User ID
     *
     * @return : array of roles
     */
    public function getUsersRole($userId)
    {
        $this->RolesUsers = TableRegistry::get('RolesUsers');

        // GET User Role
        $rolesQuery = $this->RolesUsers->findByUserId($userId);

        $roles = [];

        foreach ($rolesQuery as $roleUser => $individualRole) {
            array_push($roles, $individualRole['role_id']);
        }
        return $roles;
    }

    /**
     * User Role
     *
     * @param : $userId
     * userId -> User ID
     *
     * @return : array of roles
     */
    public function initializeSearchSettings()
    {
        $this->searchSettings = [
            'PageSize' => '10',
            'object' => [
                'collection', 'period', 'provenience', 'atype', 'materials'
            ],
            'textual' => [
                'languages', 'genres'
            ],
            'publication' => [
                'authors', 'year'
            ],
            'compact:sidebar' => '1',
            'compact:object' => [
                'collection', 'period', 'provenience', 'atype', 'materials'
            ],
            'compact:textual' => [],
            'compact:publication' => [
                'authors', 'year'
            ],
            'full:sidebar' => '1',
            'full:object' => [
                'collection', 'period', 'provenience', 'atype', 'materials'
            ],
            'full:textual' => [
                'languages', 'genres'
            ],
            'full:publication' => [
                'authors', 'year'
            ]
        ];
        $this
            ->getController()
            ->getRequest()
            ->getSession()
            ->write('searchSettings', $this->searchSettings);
    }
}
