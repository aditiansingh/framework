<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Months Model
 *
 * @property \App\Model\Table\DatesTable&\Cake\ORM\Association\HasMany $Dates
 *
 * @method \App\Model\Entity\Month newEmptyEntity()
 * @method \App\Model\Entity\Month newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Month[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Month get($primaryKey, $options = [])
 * @method \App\Model\Entity\Month findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Month patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Month[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Month|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Month saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Month[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Month[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Month[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Month[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MonthsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('months');
        $this->setDisplayField('month_no');
        $this->setPrimaryKey('id');

        $this->hasMany('Dates', [
            'foreignKey' => 'month_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('composite_month_name')
            ->allowEmptyString('composite_month_name');

        $validator
            ->scalar('month_no')
            ->maxLength('month_no', 45)
            ->allowEmptyString('month_no');

        $validator
            ->integer('sequence')
            ->allowEmptyString('sequence');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);

        return $rules;
    }
}
