<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Articles Model
 *
 * @property \App\Model\Table\AuthorsTable&\Cake\ORM\Association\BelongsToMany $Authors
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsToMany $Publications
 *
 * @method \App\Model\Entity\Article newEmptyEntity()
 * @method \App\Model\Entity\Article newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Article[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Article get($primaryKey, $options = [])
 * @method \App\Model\Entity\Article findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Article patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Article[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Article|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Article[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArticlesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('articles');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Authors', [
            'foreignKey' => 'article_id',
            'targetForeignKey' => 'author_id',
            'joinTable' => 'articles_authors'
        ]);
        $this->belongsToMany('Publications', [
            'foreignKey' => 'article_id',
            'targetForeignKey' => 'publication_id',
            'joinTable' => 'articles_publications'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('serial')
            ->maxLength('serial', 50)
            ->allowEmptyString('serial');

        $validator
            ->scalar('title')
            ->maxLength('title', 350)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->scalar('article_no')
            ->requirePresence('article_no', 'create')
            ->notEmpty('article_no');

        $validator
            ->integer('year')
            ->requirePresence('year', 'create')
            ->notEmpty('year');

        $validator
            ->scalar('content_html')
            ->maxLength('content_html', 4294967295)
            ->requirePresence('content_html', 'create')
            ->notEmptyString('content_html');

        $validator
            ->scalar('content_latex')
            ->maxLength('content_latex', 4294967295)
            ->requirePresence('content_latex', 'create')
            ->notEmptyString('content_latex');

        $validator
            ->boolean('is_pdf_uploaded')
            ->requirePresence('is_pdf_uploaded', 'create')
            ->notEmptyString('is_pdf_uploaded');

        $validator
            ->scalar('article_type')
            ->requirePresence('article_type', 'create')
            ->notEmptyString('article_type');

        $validator
            ->integer('is_published')
            ->requirePresence('is_published', 'create')
            ->notEmptyString('is_published');

        $validator
            ->integer('article_status')
            ->requirePresence('article_status', 'create')
            ->notEmptyString('article_status');

        $validator
            ->scalar('pdf_link')
            ->maxLength('pdf_link', 4294967295)
            ->requirePresence('pdf_link', 'create')
            ->notEmptyString('pdf_link');

        $validator
            ->nonNegativeInteger('created_by')
            ->requirePresence('created_by', 'create')
            ->notEmptyString('created_by');

        return $validator;
    }
    
    public function findType(Query $query, array $options)
    {
        $type = $options['type'];
        return $query->where(['article_type' => $type]);
    }
}
