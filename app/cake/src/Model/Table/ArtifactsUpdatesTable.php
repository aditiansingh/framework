<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArtifactsUpdates Model
 *
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\UpdateEventsTable&\Cake\ORM\Association\BelongsTo $UpdateEvents
 *
 * @method \App\Model\Entity\ArtifactsUpdate newEmptyEntity()
 * @method \App\Model\Entity\ArtifactsUpdate newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArtifactsUpdate[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactsUpdatesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifacts_updates');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsTo('UpdateEvents', [
            'foreignKey' => 'update_events_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('cdli_comments')
            ->allowEmptyString('cdli_comments');

        $validator
            ->scalar('designation')
            ->allowEmptyString('designation');

        $validator
            ->scalar('artifact_type')
            ->allowEmptyString('artifact_type');

        $validator
            ->scalar('period')
            ->allowEmptyString('period');

        $validator
            ->scalar('provenience')
            ->allowEmptyString('provenience');

        $validator
            ->scalar('written_in')
            ->allowEmptyString('written_in');

        $validator
            ->scalar('archive')
            ->allowEmptyString('archive');

        $validator
            ->scalar('composite_no')
            ->maxLength('composite_no', 250)
            ->allowEmptyString('composite_no');

        $validator
            ->scalar('seal_no')
            ->maxLength('seal_no', 250)
            ->allowEmptyString('seal_no');

        $validator
            ->scalar('composites')
            ->allowEmptyString('composites');

        $validator
            ->scalar('seals')
            ->allowEmptyString('seals');

        $validator
            ->scalar('museum_no')
            ->maxLength('museum_no', 11)
            ->allowEmptyString('museum_no');

        $validator
            ->scalar('accession_no')
            ->maxLength('accession_no', 11)
            ->allowEmptyString('accession_no');

        $validator
            ->scalar('condition_description')
            ->allowEmptyString('condition_description');

        $validator
            ->scalar('artifact_preservation')
            ->allowEmptyString('artifact_preservation');

        $validator
            ->scalar('period_comments')
            ->allowEmptyString('period_comments');

        $validator
            ->scalar('provenience_comments')
            ->allowEmptyString('provenience_comments');

        $validator
            ->scalar('is_provenience_uncertain')
            ->maxLength('is_provenience_uncertain', 1)
            ->allowEmptyString('is_provenience_uncertain');

        $validator
            ->scalar('is_period_uncertain')
            ->maxLength('is_period_uncertain', 1)
            ->allowEmptyString('is_period_uncertain');

        $validator
            ->scalar('is_artifact_type_uncertain')
            ->maxLength('is_artifact_type_uncertain', 1)
            ->allowEmptyString('is_artifact_type_uncertain');

        $validator
            ->scalar('is_school_text')
            ->maxLength('is_school_text', 1)
            ->allowEmptyString('is_school_text');

        $validator
            ->scalar('height')
            ->maxLength('height', 11)
            ->allowEmptyString('height');

        $validator
            ->scalar('thickness')
            ->maxLength('thickness', 11)
            ->allowEmptyString('thickness');

        $validator
            ->scalar('width')
            ->maxLength('width', 11)
            ->allowEmptyString('width');

        $validator
            ->scalar('weight')
            ->maxLength('weight', 11)
            ->allowEmptyString('weight');

        $validator
            ->scalar('elevation')
            ->allowEmptyString('elevation');

        $validator
            ->scalar('excavation_no')
            ->allowEmptyString('excavation_no');

        $validator
            ->scalar('findspot_square')
            ->allowEmptyString('findspot_square');

        $validator
            ->scalar('findspot_comments')
            ->allowEmptyString('findspot_comments');

        $validator
            ->scalar('stratigraphic_level')
            ->allowEmptyString('stratigraphic_level');

        $validator
            ->scalar('surface_preservation')
            ->allowEmptyString('surface_preservation');

        $validator
            ->scalar('artifact_comments')
            ->allowEmptyString('artifact_comments');

        $validator
            ->scalar('seal_information')
            ->allowEmptyString('seal_information');

        $validator
            ->scalar('accounting_period')
            ->allowEmptyString('accounting_period');

        $validator
            ->scalar('is_public')
            ->maxLength('is_public', 1)
            ->allowEmptyString('is_public');

        $validator
            ->scalar('is_atf_public')
            ->maxLength('is_atf_public', 1)
            ->allowEmptyString('is_atf_public');

        $validator
            ->scalar('are_images_public')
            ->maxLength('are_images_public', 1)
            ->allowEmptyFile('are_images_public');

        $validator
            ->scalar('collections')
            ->allowEmptyString('collections');

        $validator
            ->scalar('dates')
            ->allowEmptyString('dates');

        $validator
            ->scalar('alternative_years')
            ->allowEmptyString('alternative_years');

        $validator
            ->scalar('external_resources')
            ->allowEmptyString('external_resources');

        $validator
            ->scalar('external_resources_key')
            ->maxLength('external_resources_key', 250)
            ->allowEmptyString('external_resources_key');

        $validator
            ->scalar('genres')
            ->allowEmptyString('genres');

        $validator
            ->scalar('genres_comment')
            ->allowEmptyString('genres_comment');

        $validator
            ->scalar('genres_uncertain')
            ->maxLength('genres_uncertain', 250)
            ->allowEmptyString('genres_uncertain');

        $validator
            ->scalar('languages')
            ->allowEmptyString('languages');

        $validator
            ->scalar('languages_uncertain')
            ->maxLength('languages_uncertain', 250)
            ->allowEmptyString('languages_uncertain');

        $validator
            ->scalar('materials')
            ->allowEmptyString('materials');

        $validator
            ->scalar('materials_aspect')
            ->allowEmptyString('materials_aspect');

        $validator
            ->scalar('materials_color')
            ->allowEmptyString('materials_color');

        $validator
            ->scalar('materials_uncertain')
            ->maxLength('materials_uncertain', 250)
            ->allowEmptyString('materials_uncertain');

        $validator
            ->scalar('shadow_cdli_comments')
            ->allowEmptyString('shadow_cdli_comments');

        $validator
            ->scalar('shadow_collection_location')
            ->allowEmptyString('shadow_collection_location');

        $validator
            ->scalar('shadow_collection_comments')
            ->allowEmptyString('shadow_collection_comments');

        $validator
            ->scalar('shadow_acquisition_history')
            ->allowEmptyString('shadow_acquisition_history');

        $validator
            ->scalar('publications_key')
            ->allowEmptyString('publications_key');

        $validator
            ->scalar('publications_type')
            ->allowEmptyString('publications_type');

        $validator
            ->scalar('publications_exact_ref')
            ->allowEmptyString('publications_exact_ref');

        $validator
            ->scalar('publications_comment')
            ->allowEmptyString('publications_comment');

        $validator
            ->scalar('retired')
            ->maxLength('retired', 1)
            ->allowEmptyString('retired');

        $validator
            ->scalar('has_fragments')
            ->maxLength('has_fragments', 1)
            ->allowEmptyString('has_fragments');

        $validator
            ->scalar('is_artifact_fake')
            ->maxLength('is_artifact_fake', 1)
            ->allowEmptyString('is_artifact_fake');

        $validator
            ->scalar('publication_error')
            ->requirePresence('publication_error', 'create')
            ->notEmptyString('publication_error');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'), ['errorField' => 'artifact_id']);
        $rules->add($rules->existsIn(['update_events_id'], 'UpdateEvents'), ['errorField' => 'update_events_id']);

        return $rules;
    }
}
