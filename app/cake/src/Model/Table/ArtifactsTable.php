<?php
namespace App\Model\Table;

use Cake\Database\Expression\IdentifierExpression;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * Artifacts Model
 *
 * @property \App\Model\Table\ProveniencesTable&\Cake\ORM\Association\BelongsTo $Proveniences
 * @property \App\Model\Table\ProveniencesTable&\Cake\ORM\Association\BelongsTo $Origins
 * @property \App\Model\Table\PeriodsTable&\Cake\ORM\Association\BelongsTo $Periods
 * @property \App\Model\Table\ArtifactTypesTable&\Cake\ORM\Association\BelongsTo $ArtifactTypes
 * @property \App\Model\Table\ArchivesTable&\Cake\ORM\Association\BelongsTo $Archives
 * @property \App\Model\Table\ArtifactsShadowTable&\Cake\ORM\Association\HasMany $ArtifactsShadow
 * @property \App\Model\Table\ArtifactsUpdatesTable&\Cake\ORM\Association\HasMany $ArtifactsUpdates
 * @property \App\Model\Table\InscriptionsTable&\Cake\ORM\Association\HasMany $Inscriptions
 * @property \App\Model\Table\PostingsTable&\Cake\ORM\Association\HasMany $Postings
 * @property \App\Model\Table\RetiredArtifactsTable&\Cake\ORM\Association\HasMany $RetiredArtifacts
 * @property \App\Model\Table\CollectionsTable&\Cake\ORM\Association\BelongsToMany $Collections
 * @property \App\Model\Table\DatesTable&\Cake\ORM\Association\BelongsToMany $Dates
 * @property \App\Model\Table\ExternalResourcesTable&\Cake\ORM\Association\BelongsToMany $ExternalResources
 * @property \App\Model\Table\GenresTable&\Cake\ORM\Association\BelongsToMany $Genres
 * @property \App\Model\Table\LanguagesTable&\Cake\ORM\Association\BelongsToMany $Languages
 * @property \App\Model\Table\MaterialsTable&\Cake\ORM\Association\BelongsToMany $Materials
 * @property \App\Model\Table\PublicationsTable&\Cake\ORM\Association\BelongsToMany $Publications
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsToMany $Witnesses
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsToMany $Impressions
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsToMany $Composites
 * @property \App\Model\Table\ArtifactsTable&\Cake\ORM\Association\BelongsToMany $Seals
 *
 * @method \App\Model\Entity\Artifact newEmptyEntity()
 * @method \App\Model\Entity\Artifact newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Artifact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Artifact get($primaryKey, $options = [])
 * @method \App\Model\Entity\Artifact findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Artifact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Artifact[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Artifact|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Artifact saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArtifactsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('artifacts');
        $this->setDisplayField('designation');
        $this->setPrimaryKey('id');

        $this->belongsTo('Proveniences', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->belongsTo('Origins', [
            'foreignKey' => 'written_in',
            'className' => 'Proveniences'
        ]);
        $this->belongsTo('Periods', [
            'foreignKey' => 'period_id'
        ]);
        $this->belongsTo('ArtifactTypes', [
            'foreignKey' => 'artifact_type_id'
        ]);
        $this->belongsTo('Archives', [
            'foreignKey' => 'archive_id'
        ]);
        $this->hasMany('ArtifactsShadow', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->hasMany('ArtifactsUpdates', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->hasOne('Inscriptions', [
            'foreignKey' => 'artifact_id',
            'conditions' => ['is_latest' => 1]
        ]);
        $this->hasMany('Postings', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->hasMany('RetiredArtifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->belongsToMany('Collections', [
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'collection_id',
            'joinTable' => 'artifacts_collections'
        ]);
        $this->belongsToMany('Dates', [
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'date_id',
            'joinTable' => 'artifacts_dates'
        ]);
        $this->belongsToMany('ExternalResources', [
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'external_resource_id',
            'joinTable' => 'artifacts_external_resources'
        ]);
        $this->belongsToMany('Genres', [
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'genre_id',
            'joinTable' => 'artifacts_genres'
        ]);
        $this->belongsToMany('Languages', [
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'language_id',
            'joinTable' => 'artifacts_languages'
        ]);
        $this->belongsToMany('Materials', [
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'material_id',
            'joinTable' => 'artifacts_materials'
        ]);
        $this->belongsToMany('MaterialAspects', [
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'material_aspect_id',
            'joinTable' => 'artifacts_materials'
        ]);
        $this->belongsToMany('MaterialColors', [
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'material_color_id',
            'joinTable' => 'artifacts_materials'
        ]);
        $this->belongsToMany('Publications', [
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'publication_id',
            'joinTable' => 'artifacts_publications'
        ]);

        // Artifacts.composite_no indicates the artifact is a composite.
        // ArtifactsComposites links a composite (.composite_no) to a witness
        // (.artifact_id).
        $this->belongsToMany('Witnesses', [
            // To get the witnesses of a composite, we take the composite_no and
            // match it to the composite_no in the join table. Simultaneously the
            // artifact_id in the join table is matched to the id of the witness.
            'bindingKey' => 'composite_no',
            'foreignKey' => 'composite_no',
            'targetForeignKey' => 'artifact_id',
            'targetBindingKey' => 'id',
            'joinTable' => 'artifacts_composites',
            'className' => 'Artifacts'
        ]);
        $this->belongsToMany('Composites', [
            'bindingKey' => 'id',
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'composite_no',
            'targetBindingKey' => 'composite_no',
            'joinTable' => 'artifacts_composites',
            'className' => 'Artifacts'
        ]);

        // Repeat the same for seals, but substitute seal_no for composite_no.
        $this->belongsToMany('Impressions', [
            'bindingKey' => 'seal_no',
            'foreignKey' => 'seal_no',
            'targetForeignKey' => 'artifact_id',
            'targetBindingKey' => 'id',
            'joinTable' => 'artifacts_seals',
            'className' => 'Artifacts'
        ]);
        $this->belongsToMany('Seals', [
            'bindingKey' => 'id',
            'foreignKey' => 'artifact_id',
            'targetForeignKey' => 'seal_no',
            'targetBindingKey' => 'seal_no',
            'joinTable' => 'artifacts_seals',
            'className' => 'Artifacts'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('ark_no')
            ->maxLength('ark_no', 20)
            ->allowEmptyString('ark_no');

        $validator
            ->scalar('cdli_comments')
            ->allowEmptyString('cdli_comments');

        $validator
            ->scalar('composite_no')
            ->maxLength('composite_no', 50)
            ->allowEmptyString('composite_no');

        $validator
            ->scalar('condition_description')
            ->allowEmptyString('condition_description');

        $validator
            ->scalar('designation')
            ->maxLength('designation', 200)
            ->allowEmptyString('designation');

        $validator
            ->scalar('elevation')
            ->maxLength('elevation', 200)
            ->allowEmptyString('elevation');

        $validator
            ->scalar('excavation_no')
            ->maxLength('excavation_no', 200)
            ->allowEmptyString('excavation_no');

        $validator
            ->scalar('findspot_comments')
            ->allowEmptyString('findspot_comments');

        $validator
            ->scalar('findspot_square')
            ->allowEmptyString('findspot_square');

        $validator
            ->scalar('museum_no')
            ->allowEmptyString('museum_no');

        $validator
            ->scalar('artifact_preservation')
            ->allowEmptyString('artifact_preservation');

        $validator
            ->boolean('is_public')
            ->allowEmptyString('is_public');

        $validator
            ->boolean('is_atf_public')
            ->allowEmptyString('is_atf_public');

        $validator
            ->boolean('are_images_public')
            ->allowEmptyFile('are_images_public');

        $validator
            ->scalar('seal_no')
            ->maxLength('seal_no', 50)
            ->allowEmptyString('seal_no');

        $validator
            ->scalar('seal_information')
            ->allowEmptyString('seal_information');

        $validator
            ->scalar('stratigraphic_level')
            ->allowEmptyString('stratigraphic_level');

        $validator
            ->scalar('surface_preservation')
            ->allowEmptyString('surface_preservation');

        $validator
            ->decimal('thickness')
            ->allowEmptyString('thickness');

        $validator
            ->decimal('height')
            ->allowEmptyString('height');

        $validator
            ->decimal('width')
            ->allowEmptyString('width');

        $validator
            ->decimal('weight')
            ->allowEmptyString('weight');

        $validator
            ->boolean('is_provenience_uncertain')
            ->allowEmptyString('is_provenience_uncertain');

        $validator
            ->boolean('is_period_uncertain')
            ->allowEmptyString('is_period_uncertain');

        $validator
            ->scalar('accession_no')
            ->allowEmptyString('accession_no');

        $validator
            ->scalar('alternative_years')
            ->allowEmptyString('alternative_years');

        $validator
            ->scalar('period_comments')
            ->allowEmptyString('period_comments');

        $validator
            ->scalar('provenience_comments')
            ->allowEmptyString('provenience_comments');

        $validator
            ->boolean('is_school_text')
            ->allowEmptyString('is_school_text');

        $validator
            ->integer('written_in')
            ->allowEmptyString('written_in');

        $validator
            ->boolean('is_artifact_type_uncertain')
            ->allowEmptyString('is_artifact_type_uncertain');

        $validator
            ->scalar('dates_referenced')
            ->allowEmptyString('dates_referenced');

        $validator
            ->scalar('accounting_period')
            ->allowEmptyString('accounting_period');

        $validator
            ->scalar('artifact_comments')
            ->allowEmptyString('artifact_comments');

        $validator
            ->nonNegativeInteger('created_by')
            ->allowEmptyString('created_by');

        $validator
            ->boolean('retired')
            ->notEmptyString('retired');

        $validator
            ->boolean('has_fragments')
            ->notEmptyString('has_fragments');

        $validator
            ->notEmptyString('is_artifact_fake');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['id']), ['errorField' => 'id']);
        $rules->add($rules->existsIn(['provenience_id'], 'Proveniences'), ['errorField' => 'provenience_id']);
        $rules->add($rules->existsIn(['period_id'], 'Periods'), ['errorField' => 'period_id']);
        $rules->add($rules->existsIn(['artifact_type_id'], 'ArtifactTypes'), ['errorField' => 'artifact_type_id']);
        $rules->add($rules->existsIn(['archive_id'], 'Archives'), ['errorField' => 'archive_id']);

        return $rules;
    }



    /**
     * GSoC 2019 Work start here (CDLI - Search Results Visualizations)
     * Helper functions for Stats page and 6 visualizations are
     * organized in the following lines
     */

    // Stats Information is loaded below
    public function getNumberOfRowsInTable($table)
    {
        $query = TableRegistry::getTableLocator()
            ->get($table)
            ->find();

        return $query->count();
    }

    // Get the number of rows in which the specified column has NULL values
    // These values are currently not displayed
    public function getNumberOfNullValuesByColumn($table, $column)
    {
        $query = TableRegistry::getTableLocator()
            ->get($table)
            ->find()
            ->select([$column])
            ->where([
                'OR' => [[$column . ' IS NULL'], [$column . ' = ""']],
            ]);

        return $query->count();
    }

    // Get the number of distinct values present in the specified column
    public function getNumberOfDistinctValuesByColumn($table, $column)
    {
        $query = TableRegistry::getTableLocator()
            ->get($table)
            ->find()
            ->select([$column])
            ->distinct([$column]);

        return $query->count();
    }

    // Get maximum value present in the specified column
    // Maximum value will be
    // 1. Length of the largest string if the column contains some text
    // 2. Latest date if the column contains dates
    // 3. Largest value if the column contains integers/real numbers
    public function getMaximumValueByColumn($table, $column, $type)
    {
        if (($type == 'string') || ($type == 'text')) {
            $query = TableRegistry::getTableLocator()
                ->get($table)
                ->find()
                ->select([$column])
                ->order(["LENGTH(" . $column . ")" => 'DESC'])
                ->first();

            return strlen($query[$column]);
        } elseif ($type == 'date') {
            $query = TableRegistry::getTableLocator()
                ->get($table)
                ->find()
                ->select([$column])
                ->where($column . ' IS NOT NULL')
                ->order([$column => 'DESC'])
                ->first();

            return $query[$column]->format('Y-m-d e');
        } else {
            $query = TableRegistry::getTableLocator()
                ->get($table)
                ->find()
                ->select([$column])
                ->where($column . ' IS NOT NULL')
                ->order([$column => 'DESC'])
                ->first();

            return $query[$column];
        }
    }

    // Get minimum value present in the specified column
    // Minimum value will be
    // 1. Length of the smallest string if the column contains some text
    // 2. Oldest date if the column contains dates
    // 3. Smallest value if the column contains integers/real numbers
    public function getMinimumValueByColumn($table, $column, $type)
    {
        if (($type == 'string') || ($type == 'text')) {
            $query = TableRegistry::getTableLocator()
                ->get($table)
                ->find()
                ->select([$column])
                ->order(["LENGTH(" . $column . ")" => 'ASC'])
                ->first();

            return strlen($query[$column]);
        } elseif ($type == 'date') {
            $query = TableRegistry::getTableLocator()
                ->get($table)
                ->find()
                ->select([$column])
                ->where($column . ' IS NOT NULL')
                ->order([$column => 'ASC'])
                ->first();

            return $query[$column]->format('Y-m-d e');
        } else {
            $query = TableRegistry::getTableLocator()
                ->get($table)
                ->find()
                ->select([$column])
                ->where($column . ' IS NOT NULL')
                ->order([$column => 'ASC'])
                ->first();

            return $query[$column];
        }
    }

    /**
     * Helper functions for resolving various identifiers
     */

    public function findNumber(Query $query, array $options)
    {
        $id = $options['id'];
        switch ($id[0]) {
            case 'P':
                return $query->where(['id' => ltrim(substr($id, 1), '0')]);
                break;
            case 'Q':
                return $this->findComposite($query, $options);
                break;
            case 'S':
                return $this->findSeal($query, $options);
                break;
        }
    }

    public function findComposite(Query $query, array $options)
    {
        $id = $options['id'];
        return $query->where(['Artifacts.composite_no =' => $id]);
    }

    public function findSeal(Query $query, array $options)
    {
        $id = $options['id'];
        return $query->where(['Artifacts.seal_no =' => $id]);
    }

    public function getLatestInscription($id)
    {
        $inscription = $this->Inscriptions->find('all', ['fields' => ['id']])
            ->matching('Artifacts', function ($q) use ($id) {
                return $q->where(['Artifacts.id' => $id]);
            })
            ->where(['Inscriptions.is_latest' => 1])
            ->first();

        if (is_null($inscription)) {
            throw new RecordNotFoundException('No latest inscription for artifact ' . $id);
        } else {
            return $inscription->id;
        }
    }

    /**
     * Helper functions for generating visualizations
     */

    // Get most occurring languages alongwith its corresponding count
    // Bar Chart and Donut Chart Helper functions
    public function getLanguagesWithCount()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT (SELECT language FROM languages WHERE id = language_id) AS language, COUNT(*) AS count FROM `artifacts_languages` WHERE 1 GROUP BY language_id ORDER BY COUNT(*) DESC');
        $results = $results->fetchAll('assoc');

        $results = $this->processData($results, 10);

        return $results;
    }

    // Helper function which returns only `limit` - 1 number of specific attribute
    // with the rest being counted within a single row named `Others`
    public function processData($results, $limit)
    {
        $length = count($results);

        if ($length <= $limit) {
            return $results;
        }

        $data = array_slice($results, 0, $limit-1);

        $count = 0;
        for ($i = $limit; $i < $length; $i++) {
            $count += $results[$i]['count'];
        }

        foreach ($results[0] as $key => $value) {
            if ($key != 'count') {
                $data[$limit-1][$key] = 'Others';
            }
        }

        $data[$limit-1]['count'] = $count;

        return $data;
    }

    // Get top occurring genres
    public function getTopGenres($limit = 6)
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT COUNT(*) AS count, (SELECT genre FROM `genres` WHERE id = genre_id) AS genre FROM `artifacts_genres` WHERE 1 GROUP BY genre_id ORDER BY COUNT(*) DESC LIMIT ' . $limit);
        $results = $results->fetchAll('assoc');

        $topGenres = array();
        foreach ($results as $key => $value) {
            array_push($topGenres, $value['genre']);
        }

        return $topGenres;
    }

    // Get top occurring languages
    public function getTopLanguages($limit = 6)
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT COUNT(*) AS count, (SELECT language FROM `languages` WHERE id = language_id) AS language FROM `artifacts_languages` WHERE 1 GROUP BY language_id ORDER BY COUNT(*) DESC LIMIT ' . $limit);
        $results = $results->fetchAll('assoc');

        $topLanguages = array();
        foreach ($results as $key => $value) {
            array_push($topLanguages, $value['language']);
        }

        return $topLanguages;
    }

    // Get top occurring materials
    public function getTopMaterials($limit = 6)
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT COUNT(*) AS count, (SELECT material FROM `materials` WHERE id = material_id) AS material FROM `artifacts_materials` WHERE 1 GROUP BY material_id ORDER BY COUNT(*) DESC LIMIT ' . $limit);
        $results = $results->fetchAll('assoc');

        $topMaterials = array();
        foreach ($results as $key => $value) {
            array_push($topMaterials, $value['material']);
        }

        return $topMaterials;
    }

    // Get top occurring regions
    public function getTopRegions($limit = 3)
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT COUNT(*) AS count, (SELECT (SELECT region FROM `regions` WHERE id = region_id) FROM `proveniences` WHERE id = provenience_id) AS regions FROM `artifacts` WHERE provenience_id IS NOT NULL GROUP BY regions ORDER BY COUNT(*) DESC');
        $results = $results->fetchAll('assoc');

        $count = 0;
        $topRegions = array();
        foreach ($results as $key => $value) {
            // Consider only those regions which are not stored as NULL
            if (!is_null($value['regions'])) {
                array_push($topRegions, $value['regions']);
                $count++;
            }

            if ($count == $limit) {
                break;
            }
        }

        return $topRegions;
    }

    // Radar Chart Helper functions
    public function getRadarChartData()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT artifact_id, (SELECT genre FROM `genres` WHERE id =  genre_id) AS genre FROM `artifacts_genres`');
        $results = $results->fetchAll('assoc');

        $topGenres = $this->getTopGenres();     // Limit can be passed for the specific number of top genres
        $topRegions = $this->getTopRegions();   // Limit can be passed for the specific number of top regions

        $regionMapping = $this->getRegionMappingByRegionId();
        $regionIdMapping = $this->getRegionIdMappingByProvenienceId($regionMapping);

        $data = array();
        foreach ($results as $key => $value) {
            $region = $this->getRegionByArtifactId($value['artifact_id'], $regionIdMapping);
            if ($region != -1) {
                if (in_array($region, $topRegions) && in_array($value['genre'], $topGenres)) {
                    if (!isset($data[$region][$value['genre']])) {
                        $data[$region][$value['genre']] = 1;
                    } else {
                        $data[$region][$value['genre']]++;
                    }
                }
            }
        }

        return $data;
    }

    // Get the legends of the Radar Chart
    public function getRadarChartLegend($data)
    {
        return array_keys($data);
    }

    public function getRegionByArtifactId($artifactId, $regionIdMapping)
    {
        $provenienceId = $this->getProvenienceByArtifactId($artifactId);
        if (($provenienceId == null) || ($regionIdMapping[$provenienceId] == -1)) {
            return -1;
        }

        $region = $regionIdMapping[$provenienceId];

        return $region;
    }

    public function getProvenienceByArtifactId($artifactId)
    {
        $provenienceId = TableRegistry::getTableLocator()
            ->get('artifacts')
            ->find()
            ->select(['provenience_id'])
            ->where(['id' => $artifactId])
            ->first()
            ->provenience_id;

        return $provenienceId;
    }

    public function getRegionIdMappingByProvenienceId($regionMapping)
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT id, region_id FROM `proveniences`');
        $results = $results->fetchAll('assoc');

        $data = array();
        foreach ($results as $key => $value) {
            $data[$value['id']] = ($value['region_id'] == null) ? -1 : $regionMapping[$value['region_id']];
        }

        return $data;
    }

    public function getRegionMappingByRegionId()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT id, region FROM `regions`');
        $results = $results->fetchAll('assoc');

        $data = array();
        foreach ($results as $key => $value) {
            $data[$value['id']] = $value['region'];
        }

        return $data;
    }

    public function formatRadarChartData($data)
    {
        $result = array();
        $i = 0;
        foreach ($data as $values) {
            $j = 0;
            foreach ($values as $key => $value) {
                $result[$i][$j]['axis'] = $key;
                $result[$i][$j]['value'] = $value;
                $j++;
            }
            $i++;
        }

        return $result;
    }

    // Line Chart Helper Functions
    public function getLineChartData($chartType = "")
    {
        $data = array();
        if ($chartType == "period-vs-language") {
            $data = $this->getPeriodLanguageLineChart();
        } elseif ($chartType == "period-vs-material") {
            $data = $this->getPeriodMaterialLineChart();
        } else {
            $data = $this->getPeriodGenreLineChart();
        }

        return $data;
    }

    public function getPeriodLanguageLineChart()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT (SELECT language FROM `languages` WHERE id = language_id) AS language, (SELECT period_id FROM `artifacts` WHERE id = artifact_id) AS period_id FROM `artifacts_languages` WHERE 1');
        $results = $results->fetchAll('assoc');

        $periodMapping = $this->getSequenceToPeriodMapping();
        $sequenceMapping = $this->getPeriodIdToSequenceMapping();
        $topLanguages = $this->getTopLanguages(3);    // LIMIT - 3

        $data = array();
        foreach ($results as $value) {
            if (($value['period_id'] != null) && ($value['language'] != null) && in_array($value['language'], $topLanguages)) {
                if (!isset($data[$value['period_id']][$value['language']])) {
                    $data[$sequenceMapping[$value['period_id']]][$value['language']] = 1;
                } else {
                    $data[$sequenceMapping[$value['period_id']]][$value['language']]++;
                }
            }
        }

        // Set any of top 3 genre field to 0 if the value doesn't exists
        foreach ($data as $key => $value) {
            foreach ($topLanguages as $language) {
                if (!isset($data[$key][$language])) {
                    $data[$key][$language] = 0;
                }
            }
        }

        // Replace Period Id as key with Period field in the array
        foreach ($data as $key => $value) {
            $data[$key]['period'] = $periodMapping[$key];
        }

        // Sort the periods with respect to sequence
        ksort($data);

        // Reset index to zero based
        $data = array_values($data);

        return $data;
    }

    public function getPeriodMaterialLineChart()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT (SELECT material FROM `materials` WHERE id = material_id) AS material, (SELECT period_id FROM `artifacts` WHERE id = artifact_id) AS period_id FROM `artifacts_materials` WHERE 1');
        $results = $results->fetchAll('assoc');

        $periodMapping = $this->getSequenceToPeriodMapping();
        $sequenceMapping = $this->getPeriodIdToSequenceMapping();
        $topMaterials = $this->getTopMaterials(3);    // LIMIT - 3

        $data = array();
        foreach ($results as $value) {
            if (($value['period_id'] != null) && ($value['material'] != null) && in_array($value['material'], $topMaterials)) {
                if (!isset($data[$value['period_id']][$value['material']])) {
                    $data[$sequenceMapping[$value['period_id']]][$value['material']] = 1;
                } else {
                    $data[$sequenceMapping[$value['period_id']]][$value['material']]++;
                }
            }
        }

        // Set any of top 3 genre field to 0 if the value doesn't exists
        foreach ($data as $key => $value) {
            foreach ($topMaterials as $material) {
                if (!isset($data[$key][$material])) {
                    $data[$key][$material] = 0;
                }
            }
        }

        // Replace Period Id as key with Period field in the array
        foreach ($data as $key => $value) {
            $data[$key]['period'] = $periodMapping[$key];
        }

        // Sort the periods with respect to sequence
        ksort($data);

        // Reset index to zero based
        $data = array_values($data);

        return $data;
    }

    public function getPeriodGenreLineChart()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT (SELECT genre FROM genres WHERE id = genre_id) AS genre, (SELECT period_id FROM artifacts WHERE id = artifact_id) AS period_id FROM `artifacts_genres` WHERE 1');
        $results = $results->fetchAll('assoc');

        $periodMapping = $this->getSequenceToPeriodMapping();
        $sequenceMapping = $this->getPeriodIdToSequenceMapping();
        $topGenres = $this->getTopGenres(3);    // LIMIT - 3

        $data = array();
        foreach ($results as $value) {
            if (($value['period_id'] != null) && ($value['genre'] != null) && in_array($value['genre'], $topGenres)) {
                if (!isset($data[$value['period_id']][$value['genre']])) {
                    $data[$sequenceMapping[$value['period_id']]][$value['genre']] = 1;
                } else {
                    $data[$sequenceMapping[$value['period_id']]][$value['genre']]++;
                }
            }
        }

        // Set any of top 3 genre field to 0 if the value doesn't exists
        foreach ($data as $key => $value) {
            foreach ($topGenres as $genre) {
                if (!isset($data[$key][$genre])) {
                    $data[$key][$genre] = 0;
                }
            }
        }

        // Replace Period Id as key with Period field in the array
        foreach ($data as $key => $value) {
            $data[$key]['period'] = $periodMapping[$key];
        }

        // Sort the periods with respect to sequence
        ksort($data);

        // Reset index to zero based
        $data = array_values($data);

        return $data;
    }

    public function getSequenceToPeriodMapping()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT sequence, period FROM `periods`');
        $results = $results->fetchAll('assoc');

        $data = array();
        foreach ($results as $key => $value) {
            $data[$value['sequence']] = $value['period'];
        }

        return $data;
    }

    public function getPeriodIdToSequenceMapping()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT id, sequence FROM `periods`');
        $results = $results->fetchAll('assoc');

        $data = array();
        foreach ($results as $key => $value) {
            $data[$value['id']] = $value['sequence'];
        }

        return $data;
    }

    // Dendrogram Chart Helper Functions
    public function getDendrogramChartData()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT COUNT(*) AS count, (SELECT material FROM `materials` WHERE id = material_id) AS material FROM `artifacts_materials` WHERE 1 GROUP BY material_id ORDER BY COUNT(*) DESC');
        $results = $results->fetchAll('assoc');

        $materialParents = $this->getMaterialParents();
        $materialMapping = $this->getMaterialIdToMaterialMapping();

        foreach ($results as $key => $value) {
            $materialParentId = $materialParents[$value['material']];
            if ($materialParentId != null) {
                $results[$key]['parent'] = $materialMapping[$materialParentId];
            }
        }

        // Set Parent of the nodes without parent to "Root Node(here Materials)"
        foreach ($results as $key => $value) {
            if (!isset($value['parent'])) {
                $results[$key]['parent'] = "Materials";
            }     //Set overall root node to a variable
        }

        // Push Root Node of the Dendrogram
        $root = array(
            'material'  => "Materials",
            'parent'    => ""
        );
        array_push($results, $root);

        return $results;
    }

    public function getMaterialParents()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT material, parent_id FROM `materials`');
        $results = $results->fetchAll('assoc');

        $data = array();
        foreach ($results as $key => $value) {
            $data[$value['material']] = $value['parent_id'];
        }

        return $data;
    }

    public function getMaterialIdToMaterialMapping()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT id, material FROM `materials`');
        $results = $results->fetchAll('assoc');

        $data = array();
        foreach ($results as $key => $value) {
            $data[$value['id']] = $value['material'];
        }

        return $data;
    }

    // Choropleth Map Helper Functions
    public function getChoroplethMapData($filter = 'regions')
    {
        if ($filter == 'regions') {
            $map = $this->getChoroplethMapForRegions();
        } else {
            $map = $this->getChoroplethMapForProveniences();
        }

        return $map;
    }

    public function getChoroplethMapForProveniences()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT provenience, region_id, geo_coordinates AS coordinates FROM `proveniences` WHERE geo_coordinates IS NOT NULL');
        $results = $results->fetchAll('assoc');

        $regionMapping = $this->getRegionMappingByRegionId();

        // Format return data
        $data = array(
            'type'      => 'FeatureCollection',
            'name'      => 'Sites',
            'features'  => array()
        );

        $i = 0;
        foreach ($results as $value) {
            $data['features'][$i]['type'] = 'Feature';
            $data['features'][$i]['geometry']['type'] = 'Point';
            $data['features'][$i]['geometry']['coordinates'] = array_map('trim', explode(',', $value['coordinates']));
            $data['features'][$i]['properties']['provenience'] = ($value['provenience']) ? $value['provenience'] : 'NULL';
            $data['features'][$i]['properties']['regionId'] = $value['region_id'];
            $data['features'][$i]['properties']['region'] = ($value['region_id']) ? $regionMapping[$value['region_id']] : 'NULL';
            $i++;
        }

        return $data;
    }

    public function getChoroplethMapForRegions()
    {
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('SELECT region, geo_coordinates AS coordinates FROM `regions` WHERE geo_coordinates IS NOT NULL');
        $results = $results->fetchAll('assoc');

        // Format return data
        $data = array(
            'type'      => 'FeatureCollection',
            'name'      => 'Regions',
            'features'  => array()
        );

        $i = 0;
        foreach ($results as $value) {
            $data['features'][$i]['type'] = 'Feature';
            $data['features'][$i]['geometry']['type'] = 'MultiPolygon';
            $data['features'][$i]['geometry']['coordinates'] = array(array($this->formatRegionCoordinates($value['coordinates'])));
            $data['features'][$i]['properties']['region'] = $value['region'];
            $i++;
        }

        return $data;
    }

    public function formatRegionCoordinates($coordinates)
    {
        $split = array_map(
            function ($value) {
                return implode(',', $value);
            },
            array_chunk(explode(',', $coordinates), 2)
        );

        $result = array();
        foreach ($split as $value) {
            $row = array_map('trim', explode(',', $value));
            $row[0] = explode('[', $row[0])[1];
            $row[1] = explode(']', $row[1])[0];
            array_push($result, array_map('floatval', $row));
        }

        return $result;
    }
}
