<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CdliTag Entity
 *
 * @property int $id
 * @property string|null $cdli_tag
 *
 * @property \App\Model\Entity\AgadeMail[] $agade_mails
 */
class CdliTag extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cdli_tag' => true,
        'agade_mails' => true
    ];
}
