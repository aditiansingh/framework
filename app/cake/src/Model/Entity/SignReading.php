<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SignReading Entity
 *
 * @property int $id
 * @property string|null $sign_name
 * @property string|null $sign_reading
 * @property bool|null $preferred_reading
 * @property int|null $period_id
 * @property int|null $provenience_id
 * @property int|null $language_id
 * @property string|null $meaning
 *
 * @property \App\Model\Entity\Period $period
 * @property \App\Model\Entity\Provenience $provenience
 * @property \App\Model\Entity\Language $language
 * @property \App\Model\Entity\SignReadingsComment[] $sign_readings_comments
 */
class SignReading extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sign_name' => true,
        'sign_reading' => true,
        'preferred_reading' => true,
        'period_id' => true,
        'provenience_id' => true,
        'language_id' => true,
        'meaning' => true,
        'period' => true,
        'provenience' => true,
        'language' => true,
        'sign_readings_comments' => true
    ];
}
