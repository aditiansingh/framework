<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Article Entity
 *
 * @property int $id
 * @property string|null $serial
 * @property string $title
 * @property int $year
 * @property string $article_no
 * @property string $content_html
 * @property string $content_latex
 * @property bool $is_pdf_uploaded
 * @property string $article_type
 * @property int $is_published
 * @property int $article_status
 * @property string $pdf_link
 * @property int $created_by
 * @property \Cake\I18n\FrozenDate|null $created
 * @property \Cake\I18n\FrozenDate|null $modified
 *
 * @property \App\Model\Entity\Author[] $authors
 * @property \App\Model\Entity\Publication[] $publications
 */
class Article extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'serial' => true,
        'title' => true,
        'year' => true,
        'article_no' => true,
        'content_html' => true,
        'content_latex' => true,
        'is_pdf_uploaded' => true,
        'article_type' => true,
        'is_published' => true,
        'article_status' => true,
        'pdf_link' => true,
        'created_by' => true,
        'created' => true,
        'modified' => true,
        'authors' => true,
        'publications' => true,
    ];
}
