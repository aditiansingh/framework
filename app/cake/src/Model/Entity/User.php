<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property int|null $author_id
 * @property \Cake\I18n\FrozenTime $last_login_at
 * @property bool $active
 * @property string $password
 * @property string|null $2fa_key
 * @property bool|null $2fa_status
 * @property \Cake\I18n\FrozenTime $created_at
 * @property \Cake\I18n\FrozenTime $modified_at
 * @property int|null $hd_images_collection_id
 * @property string|null $token_pass
 * @property \Cake\I18n\FrozenTime|null $generated_at
 *
 * @property \App\Model\Entity\Author $author
 * @property \App\Model\Entity\Collection $hd_images_collection
 * @property \App\Model\Entity\Role[] $roles
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'username' => true,
        'email' => true,
        'author_id' => true,
        'last_login_at' => true,
        'active' => true,
        'password' => true,
        '2fa_key' => true,
        '2fa_status' => true,
        'created_at' => true,
        'modified_at' => true,
        'hd_images_collection_id' => true,
        'token_pass' => true,
        'generated_at' => true,
        'author' => true,
        'hd_images_collection' => true,
        'roles' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    //hash password
    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();
            return $hasher->hash($value);
        }
    }
}
