<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArtifactsComposite Entity
 *
 * @property int $id
 * @property string|null $composite_no
 * @property int|null $artifact_id
 *
 * @property \App\Model\Entity\Artifact $composite
 * @property \App\Model\Entity\Artifact $witness
 */
class ArtifactsComposite extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'composite_no' => true,
        'artifact_id' => true,
        'composite' => true,
        'witness' => true,
    ];
}
