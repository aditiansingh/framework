<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Region Entity
 *
 * @property int $id
 * @property string $region
 * @property string|null $geo_coordinates
 * @property int $pleiades_id
 *
 * @property \App\Model\Entity\Provenience[] $proveniences
 */
class Region extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'region' => true,
        'geo_coordinates' => true,
        'pleiades_id' => true,
        'proveniences' => true
    ];

    public function getCidocCrm()
    {
        return [
            '@id' => $this->getUri(),
            '@type' => 'crm:E53_Place',
            'crm:P87_is_identified_by' => [
                '@type' => 'crm:E48_Place_Name',
                'rdfs:label' => $this->region
            ],
            'osgeo:asGeoJSON' => empty($this->geo_coordinates)
                ? null
                : '{"type": "Polygon", "coordinates": [[' . $this->geo_coordinates . ']]}'
        ];
    }
}
