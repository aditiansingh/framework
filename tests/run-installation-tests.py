import unittest
import requests
import os
import subprocess

# These are the installation tests for ngnix sever
# and phpmyadmin.
class RunInstallationTests(unittest.TestCase):

    def test_nginx_installation(self):
        
        res = subprocess.check_output(['bash','-c', 'docker ps  -qf "name=^cdlidev_nginx_1$"'])
        
        if res.decode():
            status = "Running"
        else:
            status = "Errored"
        
        self.assertEqual(status, "Running")
        
    # def test_phpMyAdmin_installation(self):
        
    #     phpMyAdmin = "http://127.0.0.1:2355"
    #     phpMyAdmin_status = requests.get(phpMyAdmin)
        
    #     self.assertEqual(phpMyAdmin_status.status_code, 200)

if __name__ == '__main__':
    unittest.main()
